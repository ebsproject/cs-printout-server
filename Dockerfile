#See httpsx://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app 




# COPY ["sources.list","/etc/apt/sources.list"]
# COPY ["cupsd.conf","/etc/cups/cupsd.conf"]



RUN apt-get update  
# RUN apt-get install -y software-properties-common  
# RUN add-apt-repository 'deb http://deb.debian.org/debian bullseye main'  
# RUN apt-get update 
RUN apt-get install -y libc6 -f -o APT::Immediate-Configure=0 
RUN apt-get install -y libgdiplus
RUN apt-get install -y libicu-dev
RUN apt-get install -y libharfbuzz0b
RUN apt-get install -y libfontconfig1
RUN apt-get install -y libfreetype6
RUN apt-get install -y libpango-1.0-0
RUN apt-get install -y libpangocairo-1.0 
# RUN apt-get install -y cups cups-pdf
#RUN apt-get install -y avahi-daemon
# RUN apt-get install -y ufw
# RUN useradd \
#     --groups lp,lpadmin \
#     ebsuser



# ups services
#  RUN systemctl enable cups
#RUN systemctl enable dbus
#RUN systemctl enable avahi-daemon
# RUN ufw allow 5353/udp

# Set the password for the printer user
# RUN echo ebsuser:12345 | chpasswd

# EXPOSE 631
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["./cs-printout-server/cs-printoutserver.csproj", "."]
RUN dotnet restore "./cs-printoutserver.csproj" -s https://nuget.devexpress.com/XYYE7lQyrYAuldZeSbFCIBfCbczdiPmhEDEg3V19I4YkhdB8BS/api -s https://api.nuget.org/v3/index.json 
COPY ./cs-printout-server .
WORKDIR "/src/."
RUN dotnet build "./cs-printoutserver.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "./cs-printoutserver.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish . 
# COPY entrypoint.sh .

# RUN chmod +x ./entrypoint.sh 

RUN mkdir -p /app/TempFiles
ENTRYPOINT ["dotnet", "cs-printoutserver.dll"]
# ENTRYPOINT ["./entrypoint.sh"]
