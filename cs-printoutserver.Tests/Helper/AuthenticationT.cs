﻿using cs_printoutserver.Tests.Models;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace cs_printoutserver.Tests.Helper
{
    public class AuthenticationT : IAuthenticationT
    {
        public AuthenticationT()
        {

        }

        public async Task<TokenResponse?> Value()
        {
            try
            {
                string sgEndPoint = "https://cognito-idp.ap-southeast-1.amazonaws.com";
                string sgClientID = "17v3raavvaq032lpbq1f520v9g";
                //string sgClientSecret = Environment.GetEnvironmentVariable("CLIENT_SECRET");
                string sgPwdUser = "f@B7fW4lyl4&__";
                string sgUser = "admin@ebsproject.org";
                // var options = new  RestClientOptions(sgEndPoint);

                // string sgEndPoint = Environment.GetEnvironmentVariable("EBS_USER_AUTH_URL");
                // string sgClientID = Environment.GetEnvironmentVariable("CLIENT_ID");
                // string sgPwdUser = Environment.GetEnvironmentVariable("USER_PWD");
                // string sgUser = Environment.GetEnvironmentVariable("USER_NAME");

                RestClient client = new RestClient(new Uri(sgEndPoint));

                RestRequest request = new RestRequest(RestSharp.Method.POST);
                request.AddHeader("Content-Type", "application/x-amz-json-1.1");
                request.AddHeader("X-Amz-Target", "AWSCognitoIdentityProviderService.InitiateAuth");
                request.Method = RestSharp.Method.POST;

                request.RequestFormat = DataFormat.Json;

                var body = new TokenBody
                {
                    AuthParameters = new AuthParameter
                    {
                        USERNAME = sgUser,
                        PASSWORD = sgPwdUser
                    },
                    ClientId = sgClientID,
                    AuthFlow = "USER_PASSWORD_AUTH"
                };
                request.AddJsonBody(body);
                //  request.AddBody()

                IRestResponse response = await client.ExecuteAsync(request);//   .Post(request);

                dynamic resultJSON = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(response.Content);
                TokenResponse tokenResponse = ((Newtonsoft.Json.Linq.JObject)resultJSON.AuthenticationResult).ToObject<TokenResponse>();
                return tokenResponse;
            }
            catch (Exception ex)
            {
                return new TokenResponse();
            }
        }
    }
}
