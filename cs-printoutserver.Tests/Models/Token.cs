﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using RestSharp;

namespace cs_printoutserver.Tests.Models
{
    public class TokenResponse
    {
        [JsonPropertyName("TokenType")]
        public string? TokenType { get; set; }
        [JsonPropertyName("AccessToken")]
        public string? AccessToken { get; set; }
        [JsonPropertyName("IdToken")]
        public string? IdToken { get; set; }
    }

    public class TokenBody
    {
        [JsonPropertyName("AuthParameters")]
        public AuthParameter AuthParameters { get; set; }

        [JsonPropertyName("AuthFlow")]
        public string AuthFlow { get; set; } = "USER_PASSWORD_AUTH";
        [JsonPropertyName("ClientId")]
        public string ClientId { get; set; }
    }

    public class AuthParameter
    {
        [JsonPropertyName("USERNAME")]
        public string USERNAME { get; set; }
        [JsonPropertyName("PASSWORD")]
        public string PASSWORD { get; set; }
    }
}
