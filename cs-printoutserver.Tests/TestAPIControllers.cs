﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using ChoETL;
using cs_printoutserver.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;


namespace cs_printoutserver.Tests
{
    public class TestAPIControllers : IClassFixture<APIWebApplicationFactory<Startup>>
    {
        private readonly APIWebApplicationFactory<Startup> _factory;
        protected HttpClient Client;
        Helper.AuthenticationT auth = null; // new Helper.AuthenticationT();
       // string _token= String.Empty;

        public TestAPIControllers(APIWebApplicationFactory<Startup> factory)
        {
            System.Environment.SetEnvironmentVariable("CLIENT_ID", "fr8hmofILgBDzMp2EQ1acYicaGga"); 
            System.Environment.SetEnvironmentVariable("CLIENT_SECRET", "0oOh9BahOVbzr528ajfvET8M1nga");
            System.Environment.SetEnvironmentVariable("ENDPOINT_CS_API", "http://localhost:18080");
            //System.Environment.GetEnvironmentVariable("CLIENT_ID");
            //System.Environment.GetEnvironmentVariable("CLIENT_SECRET");
            //System.Environment.GetEnvironmentVariable("ENDPOINT_CS_API");
           //_token = "eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJhdF9oYXNoIjoiWW95ajc5c2owbV9Jc1BaQ3V4RU03QSIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL3VzZXJuYW1lIjoiNHVuVXJhQW5jQkVMTkVIUXBOdzhsOFpnaTlFQy1VZ1ZTWFg5TEhxRlRoTSIsInN1YiI6IjR1blVyYUFuY0JFTE5FSFFwTnc4bDhaZ2k5RUMtVWdWU1hYOUxIcUZUaE1AY2FyYm9uLnN1cGVyQGNhcmJvbi5zdXBlciIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL3JvbGUiOlsiNDBkMTc2YzctOWFmZS00NjFhLWE1YjAtMDdlNGI1ZGQyMWE3IiwiNjVhMmMzMTQtOTc5Ny00ZDhlLWIzYjYtZmQ2MjY1M2I4YThkIiwiN2QxYTg3MzktYzlhOS00NjA2LTg4NzctZGM5MmVmMDZlYTY4IiwiZGJkYjU4NmUtYTM0OS00N2FlLWE3ZGQtYTM0ZmMyNDhkMGRmIiwiZTViZDdiM2UtNzRiYS00MjY3LWFiZjMtODFiODJlMzEwYmZlIiwiMDY2NjM2MWItYjI5OC00YzIzLWIwYTItNDBiMmQxNWFmMjI3IiwiM2Y2MjIyNWUtY2MxNy00YzI5LTg1ZGItZWY2MDIyNjNlNDdkIiwiZjg1NDUwMjMtMTBiNC00MmRlLTgyODUtNTMxZThlNGMzM2Y1IiwiZGE1MDU4NDktMjY2Yy00OTViLTg1YmItZDgwNDY4NGYxZDYyIiwiNmVkNzFkYTUtNzBlZS00MWIyLTlmM2UtNjY1ZmYyMTJkNGYyIiwiYzBmYTdmNTQtZTBkYy00N2NhLTkzNTgtMDcyMmMxYmU4MmI0IiwiNzYyMGIyOTktNzVjOC00MGZlLWE3YTktZjA3ZjQyYjA1MTMzIiwiZTdhNWYxOTUtMDc2NC00NTNhLWJjM2UtNzk5ZmZkZWViMDVkIiwiYjJmMjdiMTQtZTZhMS00MTJiLWIzZjUtYzY5OTk0YWYyYjYzIiwiMDRjNDA5ZGMtNzE0ZC00ZWMyLWI5MGEtM2VhYjcwNjQ0MmFmIiwiYzUzMjUzYmUtMWI2OC00NDIxLWJjMWItYzNjOWFjZWE3YTdjIiwiYjAxN2QxMWItZTQyOS00YjkzLTlmNzQtY2VkNTE3MDBhOTVkIiwiMmM2N2RkZWItYjQxMy00NGI2LTg3NzctZDdmYTRlYjZjMDE3Il0sImFtciI6WyJTQU1MU1NPQXV0aGVudGljYXRvciJdLCJpc3MiOiJodHRwczpcL1wvc2cxLmNpbW15dC5vcmc6OTQ0M1wvb2F1dGgyXC90b2tlbiIsImF1ZCI6ImZyOGhtb2ZJTGdCRHpNcDJFUTFhY1lpY2FHZ2EiLCJjX2hhc2giOiJXQk4zTUFkSHBLZTBCOVFfanZsNGdnIiwibmJmIjoxNjQ2NjkwNzMwLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9mdWxsbmFtZSI6WyJSQU1JUkVaIEFMREFNQSIsIkp1YW4gTWFudWVsIChDSU1NWVQpIl0sImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2Rpc3BsYXlOYW1lIjoiSi5SQU1JUkVaQENJTU1ZVC5PUkciLCJhenAiOiJmcjhobW9mSUxnQkR6TXAyRVExYWNZaWNhR2dhIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvZW1haWxhZGRyZXNzIjoiSi5SQU1JUkVaQENJTU1ZVC5PUkciLCJleHAiOjE2NDY2OTQzMzAsImlhdCI6MTY0NjY5MDczMH0.OsdWGfpY1HeJmFp1pX5VtXS7UOn-9Pi0aqcTa3pF3gDhaWvKbPgTWJ68Yio-mnq3r90nuOEFz3w9yhvCYTkKPnMH8iqAcWggy89bl3XYzX7zi4jeShtvPF9u5dljCteKttmcxxyeYofw0EzvSDsp1kwCBVfWo9HxqJRhIws7NCDD_WIDPFEiQHt2wzC1UrfvL9qA1D19ePL_YEhL8On3YKoDDV35yY4diz6gI1edLwyXscJeHjNEeyC-845aBPNG-dnYn6jLf_pIPun7Fbagc1uo8cnNqhND5beNGF8wh5Mk_ilxTYrJX6G9LFNBF9Dvkg9kyQm5tixNQyydxPpr0Q";
            _factory = factory;
            auth = new Helper.AuthenticationT();
            
        }
        
        // AuthenticationHeaderValue TokenGlobal;
        // public TestAPIControllers(){
        //     var _token = auth.Value();
        //     TokenGlobal = new AuthenticationHeaderValue("bearer",_token.IdToken);
        // }

         [Fact]
        public async Task Test_Get_Programs()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);

            //Act
            var response = await httpClient.GetAsync(String.Format("api/template/programs"));

            //Asert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            httpClient.Dispose();
        }  
           [Fact]
        public async Task Test_Get_Products()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);

            //Act

            var response = await httpClient.GetAsync(String.Format("api/template/products"));

            //Asert

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            httpClient.Dispose();
        }  
         [Fact]
        public async Task Test_Get_Templates_By_Product()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var productId = "1".ToInt32();

            //Act
            var response = await httpClient.GetAsync(String.Format("api/template/product/?productId={0}",productId));

            //Asert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            httpClient.Dispose();
        }  
         [Fact]
        public async Task Test_Get_Templates_By_Program()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var programId = "1".ToInt32();

            //Act
            var response = await httpClient.GetAsync(String.Format("api/template/program/?programId={0}",programId));

            //Asert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            httpClient.Dispose();
        }
         [Fact]
        public async Task Test_Get_Templates_By_Program_And_Product()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var programId = "1".ToInt32();
            var productId = "1".ToInt32();

            //Act
            var response = await httpClient.GetAsync(String.Format("api/template/product/?productId={0}/program/?programId={0}",productId,programId ));

            //Asert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            httpClient.Dispose();
        }
         [Fact]
        public async Task Test_Get_All_Connections()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);

            //Act
            var response = await httpClient.GetAsync(String.Format("api/report/getAllConnections"));

            //Asert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            httpClient.Dispose();
        }
         [Fact]
        public async Task Test_Get_Formats()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);

            //Act
            var response = await httpClient.GetAsync(String.Format("api/report/getformats"));

            //Asert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            httpClient.Dispose();
        }
//  [Fact]
//         public async Task Test_Get_Query()
//         {
//             //Arange
//             var _token = await auth.Value();
//             var httpClient = _factory.CreateClient();
//             httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
//             var programId = "1".ToInt32();
//             var productId = "1".ToInt32();

//             //Act
//             var response = await httpClient.GetAsync(String.Format("api/template/product/?productId={0}/program/?programId={0}",productId,programId ));

//             //Asert
//             Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
//             httpClient.Dispose();
//         }

   
        [Fact]
        public async Task Test_Create_Report()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
     
            var reportName = Guid.NewGuid().ToString();

            //Act

            var response = await httpClient.PostAsync(String.Format("api/report/create?name={0}",reportName),null);

            //Asert

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            await httpClient.DeleteAsync(string.Format("api/report/delete?name={0}", reportName));
            httpClient.Dispose();
        }
        [Fact]
        public async Task Test_Create_Report_When_Exist()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
     
            var reportName = "Test-1";

            //Act

            var response1 = await httpClient.PostAsync(String.Format("api/report/create?name={0}",reportName),null);

            var response = await httpClient.PostAsync(String.Format("api/report/create?name={0}",reportName),null);

            //Asert

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            await httpClient.DeleteAsync(string.Format("api/report/delete?name={0}", reportName));
            httpClient.Dispose();
        }
         [Fact]
        public async Task Test_Create_Report_When_Error()
        {
            //Arange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
     
            var reportName = "Test-1";
            //Act
            var response = await httpClient.PostAsync(String.Format("api/report/create?names={0}",reportName),null);
            //Asert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            httpClient.Dispose();
        }
        [Fact]
        public async Task Test_Export_Report()
        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var reportNameToExport = Guid.NewGuid().ToString();
            var format = "txt";
            await httpClient.PostAsync(String.Format("api/report/create?name={0}", reportNameToExport), null);

            //Act
            var response = await httpClient.GetAsync(String.Format("api/report/export?name={0}&format={1}", reportNameToExport, format));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            await httpClient.DeleteAsync(string.Format("api/report/delete?name={0}", reportNameToExport));
            httpClient.Dispose();
        }




        [Fact]
        public async Task Test_Report_Copy()
        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            var reportName = Guid.NewGuid().ToString();
            var reportName_Copy = Guid.NewGuid().ToString();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            await httpClient.PostAsync(String.Format("api/report/create?name={0}",reportName), null);

            //Act

            var response = await httpClient.PostAsync(String.Format("api/report/copy?source={0}&destination={1}",reportName,reportName_Copy), null);

            //Assert

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            await httpClient.DeleteAsync(string.Format("api/report/delete?name={0}", reportName));
            await httpClient.DeleteAsync(string.Format("api/report/delete?name={0}", reportName_Copy));
            httpClient.Dispose();
        }
        [Fact]
        public async Task Test_Print_When_Report_Exists()
        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var reportName = "ReportTest";
            var printerName = "Microsoft Print to PDF";
           // var interval = "0";

            await httpClient.PostAsync(String.Format("api/report/create?name={0}",reportName), null);

            //Act

            var response = await httpClient.GetAsync(string.Format("api/report/print?name={0}&printerName={1}&parameter[pageRange]=0&parameter[pageInterval]=0&parameter[name]={0}", reportName, printerName));
            //Assert

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            await httpClient.DeleteAsync(string.Format("api/report/delete?name={0}", reportName));
            httpClient.Dispose();
        }
        [Fact]
        public async Task Test_Print_When_Report_Doesnt_Exists()
        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var reportName = "ReportDoesNotExists";
            var printerName = "Microsoft Print to PDF";
            //Act

            var response = await httpClient.GetAsync(string.Format("api/report/print?name={0}&printerName={1}&parameter[pageRange]=0&parameter[pageInterval]=0&parameter[name]={0}", reportName, printerName));
            //Assert

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            httpClient.Dispose();
        }
        //[Fact]
        //public async Task TestPrintZPL()
        //{
        //    //Arrange

        //    var httpClient = _factory.CreateClient();
        //    var name = "ReporteX";
        //    var ipAddress = "127.0.0.1";
        //    var zplCode = "^XA^ CF0,60 ^ FO50,25 ^ GB700,3,3 ^ FS ^ FO220,50 ^ FDEBS ^ FS ^ CF0,30^ FO220,115 " +
        //        "^ FDEnterprise Breeding System^FS ^ FO220,155 ^ FDTest Label por ZPL Printers^FS " +
        //        "^ FO220,195 ^ FDPrinting the Label^FS ^ FO50,250 ^ GB700,3,3 ^ FS^ XZ";
        //    //Act

        //    var response = await httpClient.GetAsync(string.Format("api/report/printzpl?name={0}&printerName={1}&zplCode={2}", name, ipAddress, zplCode));
        //    //Assert

        //    Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        //    httpClient.Dispose();
        //}
        [Fact]
        public async Task Test_Delete_Report_When_Exists()
        {


            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var reportName = Guid.NewGuid().ToString();

            await httpClient.PostAsync(String.Format("api/report/create?name={0}",reportName), null);

            //Act

            var response = await httpClient.DeleteAsync(String.Format("api/report/delete?name={0}", reportName));
            //Assert

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            httpClient.Dispose();
        }
        #region File Management Controllers Test
        [Fact]
        public async Task Upload_File()

        {

            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            string programName = Guid.NewGuid().ToString();
            string fileName = Guid.NewGuid().ToString();
            string documentName = programName + "-" + fileName;

            MultipartFormDataContent form = new MultipartFormDataContent();
            form = CreateFormForDocumentUpload(programName,fileName);

            //Act
         
            var result = await httpClient.PostAsync("api/filemanager/uploaddocument", form);


            // Assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            await httpClient.DeleteAsync(string.Format("api/filemanager/deletedocument?file_name={0}", documentName+".pdf"));


        }
        [Fact]
        public async Task Download_File_Test_When_Document_Exists()

        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var fileName = "person_import_template.csv";

            //Act

            var response = await httpClient.GetAsync(string.Format("/api/filemanager/downloaddocument?file_name={0}", fileName));

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);


        }
        [Fact]
        public async Task Download_File_Test_When_Document_Doesnt_Exists()

        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var fileName = "ReporteUnavailabe.pdf";

            //Act

            var response = await httpClient.GetAsync(string.Format("/api/filemanager/downloaddocument?file_name={0}", fileName));

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);


        }
        [Fact]
        public async Task Delete_File_Test()

        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            string programName = Guid.NewGuid().ToString(); ;
            string fileName = Guid.NewGuid().ToString(); ;
            string documentName = programName + "-" + fileName;

            var form = CreateFormForDocumentUpload(programName, fileName);
            await httpClient.PostAsync("api/filemanager/uploaddocument", form);

            //Act

            var response = await httpClient.DeleteAsync(string.Format("/api/filemanager/deletedocument?file_name={0}", documentName+".pdf"));

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);


        }
        [Fact]
        public async Task Delete_File_Test_When_File_Doesnt_Exists()

        {
            //Arrange
            var _token = await auth.Value();
            var httpClient = _factory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _token.IdToken);
            var fileName = "ProgramTest-TstNotExists.pdf";

            //Act

            var response = await httpClient.DeleteAsync(string.Format("/api/filemanager/deletedocument?file_name={0}", fileName));

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);


        }
        #endregion
 

        private MultipartFormDataContent CreateFormForDocumentUpload(string programName,string fileName)
        {
            var bytes = Encoding.UTF8.GetBytes("Document upload for testing purpose.");
            MultipartFormDataContent form = new MultipartFormDataContent();
            var byteArrayContent = new ByteArrayContent(bytes);
            byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/pdf");
            form.Add(new StringContent(programName), "program");
            form.Add(byteArrayContent, "file", fileName + ".pdf");


            return form;

        }

    }
}
