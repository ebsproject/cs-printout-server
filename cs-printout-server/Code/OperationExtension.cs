﻿using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace cs_printoutserver.Code
{


    //HACK: Workaround, convert to a class outsite of this namespace
    public class CustomOpenIdConnectProtocolValidator : OpenIdConnectProtocolValidator
    {
        public CustomOpenIdConnectProtocolValidator(bool shouldValidateNonce)
        {
            this.ShouldValidateNonce = shouldValidateNonce;
        }

        protected override void ValidateNonce(OpenIdConnectProtocolValidationContext validationContext)
        {
            if (this.ShouldValidateNonce)
            {
                base.ValidateNonce(validationContext);
            }
        }

        protected override void ValidateState(OpenIdConnectProtocolValidationContext validationContext)
        {
            if (this.ShouldValidateNonce)
            {
                base.ValidateState(validationContext);
            }
        }


        private bool ShouldValidateNonce { get; set; }

    }

}

