﻿using System.Collections.Generic;
using Zebra.Sdk.Printer.Discovery;
namespace cs_printoutserver.Code
{
    public class ZebraUtilities
    {
    }

    public class NetworkDiscoveryHandler : DiscoveryHandler
    {

        private bool discoveryComplete = false;
        public List<DiscoveredPrinter> printers = new List<DiscoveredPrinter>();

        public void DiscoveryError(string message)
        {
          
            discoveryComplete = true;
        }

        public void DiscoveryFinished()
        {
            foreach (DiscoveredPrinter printer in printers)
            {
                string p = printer.Address;
            }
      
            discoveryComplete = true;
        }

        public void FoundPrinter(DiscoveredPrinter printer)
        {
            printers.Add(printer);
        }

        public bool DiscoveryComplete
        {
            get => discoveryComplete;
        }

    }
}
