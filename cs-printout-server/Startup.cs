using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Reporting;
using DevExpress.XtraReports.Web.Extensions;
using GraphQL.Client.Serializer.Newtonsoft;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using DevExpress.XtraReports.Web.WebDocumentViewer;
using DevExpress.DataAccess.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using DevExpress.XtraReports.Web.ReportDesigner.Services;
using AspNetCoreHero.ToastNotification;
using AspNetCoreHero.ToastNotification.Extensions;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using NSwag;
using NSwag.Generation.Processors.Security;
using System.Security.Claims;
using Quartz;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.Services;
using cs_printoutserver.Services;

namespace cs_printoutserver
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
      
        public Startup(IConfiguration configuration, 
                        IWebHostEnvironment env)
        {

            Configuration = configuration;
            _env = env;
            
        }
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment HostingEnvironment { get; set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
                // Register reporting services in an application's dependency injection container.
                 //  services.AddDevExpressControls();
               // Use the AddMvcCore (or AddMvc) method to add MVC services.
                //  services.AddMvcCore(); 

            services.AddCors(options =>
            {
                options.AddPolicy("AllowCorsPolicy", builder =>
                {
                    // Allow all ports on local host.
                   // builder.SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost");
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                    builder.AllowAnyOrigin();
                    builder.WithHeaders("Content-Type");
                });
            });

            #region Authentication and Authorization
           
            services.AddAuthentication(options =>
            {
               
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;

            })
            .AddCookie(options =>
            {
                options.LoginPath = "/auth/login";
                options.LogoutPath = "/auth/logout";
            })
            .AddOpenIdConnect("WSO2", option =>
             {
                
                 option.ClientId = Environment.GetEnvironmentVariable("CLIENT_ID");
                 option.ClientSecret = Environment.GetEnvironmentVariable("CLIENT_SECRET");
                 option.CorrelationCookie.SameSite = SameSiteMode.Unspecified;
                 option.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;                 
                 option.SaveTokens = true;
                 option.ResponseType = OpenIdConnectResponseType.Code;
                 option.CallbackPath = new PathString("/callback");
               
                 option.Scope.Clear();
                 option.Scope.Add("openid");
                 option.ProtocolValidator = new Code.CustomOpenIdConnectProtocolValidator(false);
                 option.TokenValidationParameters = CreateTokenValidationParameters();
                 option.Configuration = new OpenIdConnectConfiguration
                 {
                     AuthorizationEndpoint = $"{Environment.GetEnvironmentVariable("EBS_USER_AUTH_URL")}/authorize",
                     TokenEndpoint = $"{Environment.GetEnvironmentVariable("EBS_USER_AUTH_URL")}/token",

                 };
                 option.Events = new OpenIdConnectEvents
                 {
                    
                    
                     OnAccessDenied = context => {
                         return Task.CompletedTask;
                     },
                     OnTokenValidated = async context => {

                         // var _client = context.HttpContext?.RequestServices.GetService<IGraphQLClient>();
                         // var _env = context.HttpContext?.RequestServices.GetService<IWebHostEnvironment>();
                         // var _httpContextAccessor = context.HttpContext?.RequestServices.GetService<IHttpContextAccessor>();
                         // var _dbContext = context.HttpContext?.RequestServices.GetService<CoreSystemContext>();


                         // PrintOutTemplateService printOutTemplateService =
                         // new PrintOutTemplateService(_env, _dbContext, _client, _httpContextAccessor);

                         //await printOutTemplateService.FindUserLogged(context.Principal.Identity.Name,
                         //                                        context.TokenEndpointResponse.IdToken);


                         // Profile profile =  await _dbContext.Profile.SingleOrDefaultAsync<Profile>();
                         //var claims = new List<Claim>
                         //{
                         //    new Claim("FullName",$"{profile.Person.FamilyName.ToUpper()},{profile.Person.GivenName}({context.Principal.Identity.Name.ToLower()})"),

                         //};
                         //foreach (Role role in profile.Roles)
                         //    claims.Add(new Claim(ClaimTypes.Role, role.Name));

                         var claims = new List<Claim>
                         {
                             new Claim(ClaimTypes.Role,"Admin"),

                         };
                         var appIdentity = new ClaimsIdentity(claims);
                         context.Principal.AddIdentity(appIdentity);
                      
                     },
                    
                     OnAuthenticationFailed = context =>
                     {
                         var ctx = context;
                         return Task.CompletedTask;
                     },
                     OnRedirectToIdentityProvider = context =>
                     {
                          var builder = new UriBuilder(context.ProtocolMessage.RedirectUri);
                         builder.Scheme = !builder.Host.Equals("localhost") ? "https" : builder.Scheme;
                        // context.ProtocolMessage.RedirectUri = builder.ToString();
                         context.ProtocolMessage.RedirectUri=builder.Port == 80 ? $"{builder.Scheme}://{builder.Host}/callback" : builder.ToString();
                         return Task.FromResult(0);
                     },
                     OnRemoteFailure = context =>
                     {
                         var ctx = context;
                         return Task.CompletedTask;
                     }
                 };
             }).AddJwtBearer(option=> {
                 
                 option.TokenValidationParameters = CreateTokenValidationParameters();
                 option.SaveToken = true;
                 option.Events = new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerEvents
                 {
                     OnAuthenticationFailed = context =>
                     {
                         return Task.CompletedTask;
                     },

                     
                 };
             });
            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(new string[]
                    {
                     
                    CookieAuthenticationDefaults.AuthenticationScheme
                    })
                    .RequireAuthenticatedUser()
                    .Build();

                options.AddPolicy("API",
                        new AuthorizationPolicyBuilder(new string[] { JwtBearerDefaults.AuthenticationScheme })
                        .RequireAuthenticatedUser()
                        .Build()
                        ); 


            });

        
            #endregion
            //session and cache
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
      
            services.AddDevExpressControls();
            var reportsCacheCleanerSettings = new CacheCleanerSettings(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1));
            services.AddSession();

            //add models
            services.AddDbContext<Models.CoreSystemContext>(opt =>
               opt.UseInMemoryDatabase("CoreSystem"));

            services.AddControllers(options =>
            {
                options.ModelBinderProviders.Insert(0, new Binders.CoreSystemBinderProvider());
            });
            services.AddMvc();


          

            string s = Environment.GetEnvironmentVariable("ENDPOINT_CS_API")!;

            //graph
            services.AddScoped<IGraphQLClient>(s => new GraphQLHttpClient(new GraphQLHttpClientOptions
            {
                EndPoint = new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CS_API")!),"graphql"),

            }, new NewtonsoftJsonSerializer()));
            services.AddSingleton<CacheCleanerSettings>(reportsCacheCleanerSettings);

            //devexpress
            services.AddTransient<IJsonDataConnectionProviderFactory, CustomJsonDataConnectionProviderFactory>();          
            services.AddScoped<ReportStorageWebExtension, ReportStorage>();
            services.AddScoped<WebDocumentViewerOperationLogger, WebDocumentViewerLogger>();
            services.AddScoped<PreviewReportCustomizationService,PreviewReportCustomService>();
            services.AddScoped<IReportProviderAsync, CustomReportProviderAsync>();
          


            services.AddScoped<Services.Interfaces.IOperation, Services.OperationService>();


            services.ConfigureReportingServices(configurator =>
            {
                //configurator.UseDevelopmentMode(options =>
                //{
                //    options.Enabled = true;
                 
                //});
                configurator.ConfigureReportDesigner(designerConfigurator =>
                {
                    designerConfigurator.RegisterDataSourceWizardJsonConnectionStorage<cs_printoutserver.Services.CustomJsonConnection>(true);                  
                    // designerConfigurator.EnableCustomSql();
                });

                configurator.ConfigureWebDocumentViewer(viewerConfigurator => {
                    viewerConfigurator.UseCachedReportSourceBuilder();
                    viewerConfigurator.RegisterJsonDataConnectionProviderFactory<CustomJsonDataConnectionProviderFactory>();
                });

                configurator.UseAsyncEngine();
            });
            //Register the Swagger Services
            services.AddSwaggerDocument(doc =>
            {
                doc.Title = "PrintOut Server Services";
                doc.Description = "Documentation for Printout Services";
                doc.PostProcess = setting =>
                {
                    setting.Schemes.Add(NSwag.OpenApiSchema.Https);
                };
                doc.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name ="Authorization",
                    Description = "Type into the textbox: Bearer {your JWT token}.",
                    In = OpenApiSecurityApiKeyLocation.Header
                });
                doc.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
            });

            //register Notyf component
            services.AddNotyf(config => { config.DurationInSeconds = 10; config.IsDismissable = true; config.Position = NotyfPosition.BottomRight; });




            //services.AddRazorPages().AddRazorRuntimeCompilation();
            //var embeddedFileProvider = new EmbeddedFileProvider(typeof(FormFactory.FF).GetTypeInfo().Assembly, nameof(FormFactory));
            //services.Configure<MvcRazorRuntimeCompilationOptions>(options =>
            //{
            //    options.FileProviders.Add(embeddedFileProvider);
            //});

            #region Scheduler
            // base configuration from appsettings.json
            services.Configure<QuartzOptions>(Configuration.GetSection("Quartz"));

            // if you are using persistent job store, you might want to alter some options
            services.Configure<QuartzOptions>(options =>
            {
                options.Scheduling.IgnoreDuplicates = true; // default: false
                options.Scheduling.OverWriteExistingData = true; // default: true
            });
            services.AddQuartz(q =>
            {

                q.UseMicrosoftDependencyInjectionJobFactory();

                q.UseSimpleTypeLoader();
                q.UseInMemoryStore();
                q.UseDefaultThreadPool(tp =>
                {
                    tp.MaxConcurrency = 10;
                });

              
                //q.UsePersistentStore(s =>
                //{
                //    s.UseProperties = true;
                //    s.RetryInterval = TimeSpan.FromSeconds(10);
                //    s.UsePostgres(pg =>
                //    {
                //        pg.ConnectionString = "";
                //        pg.TablePrefix = "qrtz_";

                //    });
                //});

                //q.UseXmlSchedulingConfiguration(x =>
                //  {
                //      x.Files = new[] { "~/quartz_jobs.xml" };
                //      x.ScanInterval = TimeSpan.FromSeconds(2);
                //      // this is the default
                //      x.FailOnFileNotFound = true;
                //      // this is not the default
                //      x.FailOnSchedulingError = true;
                //  });


            });

            // ASP.NET Core hosting
            services.AddQuartzServer(options =>
            {
                // when shutting down we want jobs to complete gracefully
                options.WaitForJobsToComplete = true;
            });
            #endregion


            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                DevExpress.Printing.CrossPlatform.CustomEngineHelper.RegisterCustomDrawingEngine(
                    typeof(
                        DevExpress.CrossPlatform.Printing.DrawingEngine.PangoCrossPlatformEngine
                    ));
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            app.UseRouting();            
            app.UseStaticFiles();
            app.UseDevExpressControls();
            app.UseSession();
            app.UseCors("AllowCorsPolicy");
            app.UseAuthentication();
            app.UseAuthorization();

            // app.UseCookiePolicy();
            app.UseNotyf();
            //initialize data

            if (_env.EnvironmentName == "Development")
                          IdentityModelEventSource.ShowPII = true;
           
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
             

            //DevExpress
           //  app.UseStaticFiles();
             // Initialize reporting services.
        //     app.UseDevExpressControls();
            DevExpress.XtraReports.Web.ClientControls.LoggerService.Initialize(new Services.LoggerService());

            //Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();

        }
        private TokenValidationParameters CreateTokenValidationParameters()
        {
            var result = new TokenValidationParameters
            {
                ValidateIssuer = false,              
                RequireAudience = false,
                ValidateAudience = false,            
                ValidateIssuerSigningKey = false,
              
                //comment this and add this line to fool the validation logic
                SignatureValidator = delegate (string token, TokenValidationParameters parameters)
                {
                    var jwt = new JwtSecurityToken(token);
                    return jwt;
                },
                NameClaimType= "email",
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
            };
            result.RequireSignedTokens = false;
            return result;
        }

    }
}
