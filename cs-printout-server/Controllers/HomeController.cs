﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using cs_printoutserver.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Newtonsoft.Json;

namespace cs_printoutserver.Controllers
{
    public class HomeController : BaseController<HomeController>
    {
  
  
        public HomeController()
        {
            
            
        }

        private Task<string> token
        {
            get
            {
                return CS_OperationService.GetTokenAsync();
            }
        }
        public async Task<IActionResult> Index()
        {
            //review it to be improved
            if (User.Identity.IsAuthenticated)
                await RefreshData();

            var result = CS_DBContext.PrintOutTemplates.Select(s => s);
            return View(result);
        }

        public IActionResult Help()
        {
            return Redirect("/help/index.html");
        }
        public IActionResult Report(string name)
        {
            ViewBag.ReportName = name;

            PrintOutTemplate printOutTemplate = CS_DBContext.PrintOutTemplates.Where(w => w.ReportName.Trim().ToLower() == name.Trim().ToLower()).SingleOrDefault();

            var model = new Models.Report();
            model.GeneralInformation = printOutTemplate;
            model.DatasourceConn = CS_OperationService.GetJsonDataConnection(name);

            return PartialView("_report", model);
        }

        [Authorize]
        public IActionResult Settings()
        {
            return PartialView("_settings");
        }
        public async Task<IActionResult> UploadPrinterSetup(IFormFile file, string ipAddress)
        {

            //temporally code need to be moved to other business logic
            var result = new StringBuilder();
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                    result.AppendLine(await reader.ReadLineAsync());
            }


            return RedirectToAction("Index");
        }

        /// <summary>
        /// redirect to the swagger documentation
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult Swagger()
        {
            return Redirect("/swagger");
        }
        /// <summary>
        /// redirect to the file management
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult FileManagement()
        {
            //  var programs = getProgram().Result.result.data;
            Services.PrintOutTemplateService printOutTemplateService = new Services.PrintOutTemplateService(CS_WebHostEnv, CS_DBContext, CS_GraphQLClient, CS_HttpContext,CS_OperationService);
            var programs = printOutTemplateService.getProgram().Result.result.data;
            FileModel fileModel = new FileModel();
            var displayInfo = Path.Combine(CS_WebHostEnv.ContentRootPath, "Files");
            DirectoryInfo directoryInfo = new DirectoryInfo(displayInfo);
            if (!directoryInfo.Exists)
                directoryInfo.Create();
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            fileModel.fileInfo = fileInfo;
            List<string> programList = new List<string>();
            if (programs != null)
            {
                foreach (var item in programs)
                {
                    programList.Add(item.Code);
                }
            }
            fileModel.programList = programList;

            return PartialView("_fileManagement", fileModel);
        }
        [HttpPost]
        public async Task<IActionResult> UploadDocument(IFormFile file, [FromForm] string program)
        {
            var result = await CS_OperationService.UploadDocumentAsync(file, program);
            if (result)
                return Content("File Uploaded successfull.");
            else
                return Content("There was an error uploading the file.");
        }
        [HttpGet]
        public IActionResult DownloadDocument(string file_name)
        {
            return CS_OperationService.DownloadDocument(file_name);

        }
        [HttpDelete]
        public async Task<IActionResult> DeleteDocument(string file_name)
        {

            var path = Path.Combine(CS_WebHostEnv.ContentRootPath, "Files", file_name);
            bool result = await CS_OperationService.DeleteFileAsync(path);
            if (result)
                return Content("File Deleted successful.");
            else
                return Content("There was an error deleting the file.");

        }
        [HttpGet("[action]")]
        public IActionResult DownloadJson(string program)
        {
            brapi result = CS_OperationService.QueryResult(program);
            var jsonResult = JsonConvert.SerializeObject(result);
            byte[] jsonFile = System.Text.Encoding.ASCII.GetBytes(jsonResult);
            return File(jsonFile, "application/force-download", program + "-DocumentList.json");
        }
        [HttpGet("[action]")]
        public IActionResult EnvironmentVariables()
        {
            var result = Environment.GetEnvironmentVariables();
            var jsonResult = JsonConvert.SerializeObject(result);
            byte[] jsonFile = System.Text.Encoding.ASCII.GetBytes(jsonResult);
            return File(jsonFile, "application/force-download", "EnvironmentVariables.json");
        }
        [HttpGet]
        [Produces("application/json")]
        public IActionResult GetDocumentList(string program)
        {
            brapi result = CS_OperationService.QueryResult(program);
            return Ok(result);
        }

        /// <summary>
        /// redirect to the CUPS Environment
        /// </summary>
        /// <returns></returns>
        public IActionResult PrinterManagement()
        {
            return Redirect($"{Environment.GetEnvironmentVariable("ENDPOINT_CUPS")}");
        }


        [Authorize(Roles = "Admin,Administrator")]
        [HttpPost]
        public IActionResult DeleteFile(string name)
        {

            try
            {
                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");
                CS_OperationService.DeleteFileAsync(fileName).Wait();

                PrintOutTemplate printOutTemplate = CS_DBContext.PrintOutTemplates.Where(w => w.ReportName.Trim().ToLower() == name.Trim().ToLower()).SingleOrDefault();
                CS_DBContext.PrintOutTemplates.Remove(printOutTemplate);
                CS_DBContext.SaveChanges();
                CS_NotyfService.Success("The Report was deleted successfully");
            }
            catch (Exception ex)
            {
                CS_NotyfService.Error(ex.Message);
            }


            var result = CS_DBContext.PrintOutTemplates.Select(s => s);
            return View("Index", result);
        }
        
        [Authorize(Roles = "Admin,Administrator")]
        [HttpPost]
        public IActionResult DeleteDoc(string name)
        {

            try
            {
                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Files", name);
                CS_OperationService.DeleteFileAsync(fileName).Wait();
            }
            catch (Exception ex)
            {
                CS_NotyfService.Error(ex.Message);
            }
            return Ok("Document deleted");
        }

        /// <summary>
        /// Copy the Report API Url to the clipboard
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public async Task<JsonResult> Clipboard(string name)
        {

            string uriPath = await Task.Run(() =>
            {
                string buildText = $"{CS_HttpContext.HttpContext.Request.Scheme}://{CS_HttpContext.HttpContext.Request.Host.Value}/api/report/export?name={name}";

                return buildText;
            });


            return Json(uriPath);
        }
        /// <summary>
        /// Reload the console
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Refresh()
        {

            try
            {
                await RefreshData();
                CS_NotyfService.Success("Console was reloaded");
            }
            catch (Exception ex)
            {
                CS_NotyfService.Error(ex.Message);
            }


            var result = CS_DBContext.PrintOutTemplates.Select(s => s);
            return View("Index", result);
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task RefreshData()
        {
            CS_DBContext.PrintOutTemplates.RemoveRange(CS_DBContext.PrintOutTemplates);
            CS_DBContext.SaveChanges();
            Services.PrintOutTemplateService printOutTemplateService = new Services.PrintOutTemplateService(CS_WebHostEnv, CS_DBContext, CS_GraphQLClient, CS_HttpContext,CS_OperationService);
            await printOutTemplateService.LoadPrintOutTemplates();
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {

            var path = Path.Combine(
                           CS_WebHostEnv.ContentRootPath,"Reports","Connections",
                           "connection.json");
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                await file.CopyToAsync(stream);
            }

            CS_NotyfService.Success("Settings updated");
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Download()
        {

            var path = Path.Combine(
                           CS_WebHostEnv.ContentRootPath,"Reports","Connections",
                            "connection.json");

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, CS_OperationService.GetContentType(path), Path.GetFileName(path));
        }

    }
}
