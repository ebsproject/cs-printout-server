﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace cs_printoutserver.Controllers
{



    public class AccountController : BaseController<AccountController>
    {
      
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Logout()
        {

           await CS_HttpContext.HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");

        }

       


        [HttpGet]
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            CS_NotyfService.Error("Access Denied, Your are not an Administrator");
            return RedirectToAction("Index", "Home");
        }

    }
}
