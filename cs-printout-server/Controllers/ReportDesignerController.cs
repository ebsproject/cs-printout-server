﻿namespace cs_printoutserver.Controllers
{
    using DevExpress.XtraReports.Web.ReportDesigner;

    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
 
    using DevExpress.DataAccess.Json;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
 
    using Models;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
 
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Hosting;
 
    using System.Data;
    using cs_printoutserver.Services;
    using cs_printoutserver.Services.Interfaces;

    using GraphQL.Client.Abstractions;
using Microsoft.Extensions.DependencyInjection;
    using DevExpress.AspNetCore.Reporting.WebDocumentViewer;
    using DevExpress.AspNetCore.Reporting.WebDocumentViewer.Native.Services;
    using DevExpress.AspNetCore.Reporting.QueryBuilder;
    using DevExpress.AspNetCore.Reporting.QueryBuilder.Native.Services;
    using DevExpress.AspNetCore.Reporting.ReportDesigner;
    using DevExpress.AspNetCore.Reporting.ReportDesigner.Native.Services;

    public class CustomWebDocumentViewerController : WebDocumentViewerController
    {
        public CustomWebDocumentViewerController(IWebDocumentViewerMvcControllerService controllerService) : base(controllerService)
        {
        }
    }

    /// <summary>
    /// Defines the <see cref="ReportDesignerController" />.
    /// </summary>



    public class CustomReportDesignerController : ReportDesignerController
    {
        protected IWebHostEnvironment _hostEnvironment { get; }
        protected IHttpContextAccessor _httpContextAccessor { get; }
        protected IOperation _operation { get; }
        public CustomReportDesignerController(IReportDesignerMvcControllerService controllerService, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor, IOperation operation) : base(controllerService)
        {

            _hostEnvironment = env;
            _httpContextAccessor = httpContextAccessor;
            _operation = operation;
        }
        async Task<Dictionary<string, object>> GetAvailableDataSources(string reportUrl)
        {
            var dataSources = new Dictionary<string, object>();
            JsonDataSource jsonDataSource;

            //get default connections

            List<DataSourceConnection> dsConnections = _operation.GetDataConnections()
                                                      .Where(w => w.TemplateList.Any(f => f.Name == reportUrl)).ToList();

            var _client = _httpContextAccessor.HttpContext?.RequestServices.GetService<IGraphQLClient>();
            var _dbContext = _httpContextAccessor.HttpContext?.RequestServices.GetService<CoreSystemContext>();

            PrintOutTemplateService printOutTemplateService = new PrintOutTemplateService(_hostEnvironment, _dbContext, _client, _httpContextAccessor, _operation);
            var domains = await printOutTemplateService.getTenant();
         
                foreach (var ds in dsConnections)
                {
                    //use Rest Client
                    if(domains.result.data != null)
                        ds.Uri = _operation.GetSgContext(ds, domains.result.data);

                    QueryParameter[] arrQueryParameters = ds.QueryParameters.Select(s => new QueryParameter
                    {
                        Name = s.Name,
                        Type = typeof(String),
                        Value = s.DefaultValue
                    }).ToArray<QueryParameter>();


                    PathParameter[] arrPathParameter = ds.PathParameters.Select(s => new PathParameter
                    {
                        Name = s.Name,
                        Type = typeof(String),
                        Value = s.DefaultValue
                    }).ToArray<PathParameter>();

                    string jsonResult = String.Empty;
                    brapi brapiResult = await _operation.ResultFromApiSourcesAsync(ds);
                    jsonResult = JsonConvert.SerializeObject(brapiResult);


                if (!String.IsNullOrEmpty(jsonResult))
                    {
                        jsonDataSource = new JsonDataSource();
                        //verify if the result requires traspone rows to columns
                        if (ds.TransposeRowToColumns)
                        {
                            JObject jsonObject = JObject.Parse(jsonResult);
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonObject["result"]!["data"]!.ToString());
                            jsonResult = _operation.TransponseDatatable(dt, ds);
                        }
                        var jsonSource = new CustomJsonSource(jsonResult);
                        try
                        {
                            jsonDataSource.JsonSource = jsonSource;
                            await jsonDataSource.FillAsync();
                            jsonDataSource.ConnectionName = ds.Name;
                            dataSources.Add(ds.Name, jsonDataSource);
                        }
                        catch
                        { }


                    }
                
            }
          

            return dataSources;
        }



        /// <summary>
        /// The GetReportDesignerModel.
        /// </summary>
        /// <param name="reportUrl">The reportUrl<see cref="string"/>.</param>
        /// <param name="modelGenerator">The reportDesignerClientSideModelGenerator<see cref="IReportDesignerClientSideModelGenerator"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> GetReportDesignerModel([FromForm] string reportUrl,
                                                   [FromServices] IReportDesignerClientSideModelGenerator modelGenerator)
        {
            try
            {
                var model = await modelGenerator.GetModelAsync(reportUrl, await GetAvailableDataSources(reportUrl),
                                                        ReportDesignerController.DefaultUri,
                                                        WebDocumentViewerController.DefaultUri,
                                                        QueryBuilderController.DefaultUri);

                // string modelJsonScript =
                //modelGenerator
                //.GetJsonModelScript(
                //    // The name of a report (reportUrl)
                //    // that the Report Designer opens when the application starts.
                //    reportUrl,
                //   // Data sources for the Report Designer.                
                //   await GetAvailableDataSources(reportUrl),
                //    // The URI path of the default controller
                //    // that processes requests from the Report Designer.
                //    DevExpress.AspNetCore.Reporting.ReportDesigner.ReportDesignerController.DefaultUri,
                //    // The URI path of the default controller
                //    // that processes requests from the Web Document Viewer.
                //    DevExpress.AspNetCore.Reporting.WebDocumentViewer.WebDocumentViewerController.DefaultUri,
                //    // The URI path of the default controller
                //    // that processes requests from the Query Builder.
                //    DevExpress.AspNetCore.Reporting.QueryBuilder.QueryBuilderController.DefaultUri

                // );
                // return DesignerModel(model);
                JsonResult result = (JsonResult)DesignerModel(model);
                // return result;
                return Content(result.Value.ToString(), "application / json");
            }
            catch (Exception exception)
            {
                string errorJson = new ReportDesignerClientSideModelGenerator().CreateErrorJson(exception);
                return Content(errorJson, "application/json");
            }

        }

    }

    public class CustomQueryBuilderController : QueryBuilderController
    {
        public CustomQueryBuilderController(IQueryBuilderMvcControllerService controllerService) : base(controllerService)
        {
        }
    }
}
