﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cs_printoutserver.Models;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace cs_printoutserver.Controllers
{
    [Authorize(Policy = "API")]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TemplateController : BaseController<TemplateController>
    {

        private Task<string> token
        {
            get
            {
                return CS_OperationService.GetTokenAsync();
            }
        }
        #region WebServices

        [HttpGet]
        [Route("query")] 
        public async Task<IActionResult> GetQuery([FromQuery] string name, [FromQuery] string content, string filter, [FromQuery] int page = 1, [FromQuery] int size = 20, Uri uri = null)
        {
            try
            {
                if (uri == null)
                    uri = new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CS_API")), "graphql");

                List<GraphQLFilter> filters = new List<GraphQLFilter>();

                filters = JsonConvert.DeserializeObject<List<GraphQLFilter>>(filter);


                // var data = GetQueryDynamicsAsync(name, content, filter, page, size, isCB);
                DataSourceConnection ds = new DataSourceConnection
                {
                    Uri = uri.ToString(),
                    IsGraph = true,
                    AllData = true,
                    GraphQL = new GraphQLSetting
                    {
                        Entity = name,
                        Content = content
                    },
                    IsDefault = false,
                    QueryParameters = new List<QueryParameterConnection>()
                           {
                               new QueryParameterConnection
                               {
                                   Name="page",
                                   DefaultValue=page.ToString(),
                                   Value=page.ToString(),
                                   Type= "Int"
                               },
                               new QueryParameterConnection
                               {
                                   Name="size",
                                   DefaultValue=size.ToString(),
                                   Value=size.ToString(),
                                   Type= "Int"
                               }

                           }
                };

                QueryParameterConnection queryParameter;
                foreach(var f in filters)
                {
                    queryParameter = new QueryParameterConnection
                    {
                        Name = f.Col,
                        DefaultValue = f.Val,
                        Value = f.Val,
                        Type = "String"
                    };
                    ds.QueryParameters.Add(queryParameter);
                }

                var data =  await CS_OperationService.ResultFromApiSourcesAsync(ds);

                return Content(JsonConvert.SerializeObject(data),"application/json",System.Text.Encoding.UTF8);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// get the products list
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("programs")]
        public async Task<IActionResult> GetProgram([FromQuery] int page = 1, [FromQuery] int size = 20)
        {
            
           Uri uri = new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CS_API")), "graphql");
            
            // var data = GetQueryDynamicsAsync(name, content, filter, page, size, isCB);
            DataSourceConnection ds = new DataSourceConnection
            {
                Uri = uri.ToString(),
                IsGraph = true,
                AllData = true,
                GraphQL = new GraphQLSetting
                {
                    Entity = "Program",
                    Content = "content{id name code }"
                },
                IsDefault = false,
                QueryParameters = new List<QueryParameterConnection>()
                           {
                               new QueryParameterConnection
                               {
                                   Name="page",
                                   DefaultValue=page.ToString(),
                                   Value=page.ToString(),
                                   Type= "Int"
                               },
                               new QueryParameterConnection
                               {
                                   Name="size",
                                   DefaultValue=size.ToString(),
                                   Value=size.ToString(),
                                   Type= "Int"
                               }

                           }
            };
 

            var data = await CS_OperationService.ResultFromApiSourcesAsync( ds);

            return Content(JsonConvert.SerializeObject(data), "application/json", System.Text.Encoding.UTF8);
        }

        /// <summary>
        /// Get the Products Lists
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("products")]
        public async Task<IActionResult> GetProducts([FromQuery] int page = 1, [FromQuery] int size = 20)
        {
   


            Uri uri = new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CS_API")!), "graphql");

            // var data = GetQueryDynamicsAsync(name, content, filter, page, size, isCB);
            DataSourceConnection ds = new DataSourceConnection
            {
                Uri = uri.ToString(),
                IsGraph = true,
                AllData = true,
                GraphQL = new GraphQLSetting
                {
                    Entity = "Product",
                    Content = "content{id name domain { id name }}"
                },
                IsDefault = false,
                QueryParameters = new List<QueryParameterConnection>()
                           {
                               new QueryParameterConnection
                               {
                                   Name="page",
                                   DefaultValue=page.ToString(),
                                   Value=page.ToString(),
                                   Type= "Int"
                               },
                               new QueryParameterConnection
                               {
                                   Name="size",
                                   DefaultValue=size.ToString(),
                                   Value=size.ToString(),
                                   Type= "Int"
                               }

                           }
            };


            var data =  await CS_OperationService.ResultFromApiSourcesAsync( ds);

            return Content(JsonConvert.SerializeObject(data), "application/json", System.Text.Encoding.UTF8);
        }


        /// <summary>
        /// Get Templates by product Id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("product/{productId}")]
        public async Task<IActionResult> GetTemplatesByProduct(int productId)
        {
            var result = await RefreshData(productId: productId);
            return Ok(result);
        }

        /// <summary>
        /// Get Templates by program Id
        /// </summary>
        /// <param name="programId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("program/{programId}")]
        public async Task<IActionResult> GetTemplatesByProgram(int programId)
        {
            var result = await RefreshData(programId: programId);
            return Ok(result);
        }

        /// <summary>
        /// Get Template by Program and Product Id
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="programId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("product/{productId}/program/{programId}")]
        public async Task<IActionResult> GetTemplatesByProductAndProgram(int productId, int programId)
        {
            //brapi format
            var result = await RefreshData(productId, programId);
            return Ok(result);
        }

        #endregion

        #region Helpers
        /// <summary>
        /// Refresh data of templates
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="programId"></param>
        /// <returns></returns>
        [NonAction]
        private async Task<brapi> RefreshData(int productId = 0, int programId = 0)
        {
            CS_DBContext.PrintOutTemplates.RemoveRange(CS_DBContext.PrintOutTemplates);
            CS_DBContext.SaveChanges();
            Services.PrintOutTemplateService printOutTemplateService = new Services.PrintOutTemplateService(CS_WebHostEnv, CS_DBContext, CS_GraphQLClient, CS_HttpContext, CS_OperationService);
            await printOutTemplateService.LoadPrintOutTemplates(productId, programId);



            brapi result;

            try
            {
                var templates = CS_DBContext.PrintOutTemplates.Where(w => !String.IsNullOrEmpty(w.Product)).ToList();


                result = new brapi(new Status { message = "Success", messageType = MaterialStatus.success },
                                         new Pagination
                                         {
                                             currentPage = 1,
                                             totalCount = templates.Count,
                                             totalPages = 1,
                                             pageSize = templates.Count
                                         },
                                         new Datafiles(),
                                         new Data
                                         {
                                             data = templates.Select(s => new
                                             {
                                                 s.Id,
                                                 s.Name,
                                                 s.Tenant,
                                                 s.Program,
                                                 s.Product,
                                                 s.DefaultFormat,
                                                 s.Label,
                                                Parameters = CS_OperationService.RetrieveParametersFromReport(s.Name)
                                             }).ToArray()
                                         }
                                         );

            }
            catch (Exception ex)
            {
                result = new brapi(new Status { message = ex.Message, messageType = MaterialStatus.error },
                                   new Pagination { currentPage = 0, totalCount = 0, totalPages = 0, pageSize = 0 },
                                   new Datafiles(),
                                   new Data()
                                   );
            };

            return result;

        }


        #endregion


    }
}
