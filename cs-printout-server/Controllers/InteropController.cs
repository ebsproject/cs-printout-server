﻿using cs_printoutserver.Models;
using DevExpress.DataAccess.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace cs_printoutserver.Controllers
{
    /// <summary>
    /// Allow some tranformation from other domains
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
 
    public class InteropController : BaseController<ReportController>
    {
        
        private readonly IDataSourceWizardJsonConnectionStorage storage;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="storageContext"></param>
        public InteropController(IDataSourceWizardJsonConnectionStorage storageContext)
        {
            storage = storageContext;
           
        }

        /// <summary>
        /// Special API to print the tag a labels for printronix
        /// </summary>
        /// <param name="occurrenceDbId"></param>
        /// <param name="isTag"></param> Indicate if the data is to print labels or tags, true=tags, false=labels
        /// <param name="includeAlignment">Determinate if the result includes the dummy labels</param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPlotFieldTag(string occurrenceDbId, bool includeAlignment = false, bool isTag = false)
        {

            try
            {
                JArray finalResult = new JArray(); //json to respond in the request

        
                    DataSourceConnection dsConnection = CS_OperationService.GetDataConnections().Where(w => w.Name == "cb_connectionGraph").SingleOrDefault();
                dsConnection.QueryParameters.Where(w => w.Name == "occurrence.occurrenceDbId").FirstOrDefault().DefaultValue = occurrenceDbId;

                brapi brapiResult = await  CS_OperationService.ResultFromApiSourcesAsync(dsConnection);  //json result from the first pages
                JArray arrayPlots = JArray.Parse(brapiResult.result.data.ToString());


                //specify if the alignment json will part of the result
                finalResult = new JArray(arrayPlots);
                if (includeAlignment)
                {
                    JObject resultAligment = JObject.Parse(System.IO.File.ReadAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "alignmentGraphql.json")));
                    JArray arrayAligmentLabels = (JArray)resultAligment["result"]!["data"]!;

                    for (int i = 0; i < (isTag ? 4 : 9); i++)                                          
                        finalResult = new JArray(arrayAligmentLabels.Union(finalResult));
                    
                }
                brapiResult.result.data = finalResult;
                return Content(JsonConvert.SerializeObject(brapiResult), "application/json", System.Text.Encoding.UTF8);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpGet("[action]")]
     
        public async Task<IActionResult> GetFromDataConnections([FromQuery] string dataSourceConnection, [FromQuery] Dictionary<string, string> parameter = null)
        {

            DataSourceConnection dsConnection = CS_OperationService.GetDataConnections().Where(w => w.Name == dataSourceConnection).SingleOrDefault();
           

            if(parameter != null) { 
            
                string paramRealName = String.Empty;
                foreach (var query in dsConnection.QueryParameters)
                {
                    paramRealName = query.Name.Split(".").Length == 1 ? query.Name : query.Name.Split(".")[1];
                    if (parameter.ContainsKey(paramRealName))
                        query.DefaultValue = (string)parameter.GetValueOrDefault(paramRealName)!;
                }


            
                foreach (var path in dsConnection.PathParameters)
                {
                    paramRealName = path.Name.Split(".").Length == 1 ? path.Name : path.Name.Split(".")[1];
                    if (parameter.ContainsKey(paramRealName))
                        path.DefaultValue = (string)parameter.GetValueOrDefault(paramRealName)!;

                }
            }
            var content =  await CS_OperationService.ResultFromApiSourcesAsync(dsConnection)!;

            return Content(JsonConvert.SerializeObject(content),"application/json",System.Text.Encoding.UTF8);
        }
         
    }
}
