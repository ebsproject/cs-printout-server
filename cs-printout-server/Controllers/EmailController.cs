﻿using Microsoft.AspNetCore.Mvc;
using Limilabs.Mail;
using Limilabs.Mail.Headers;
using Limilabs.Client.SMTP;
using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Limilabs.Mail.MIME;
using cs_printoutserver.Models;
using Newtonsoft.Json;
namespace cs_printoutserver.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : BaseController<EmailController>
    {
      
        public EmailController( )
        {
           
        }

        /// <summary>
        /// API to send emails using any clients
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> Send([FromForm] Models.Email email)
        {

            try
            {
                //verify if the email use template

                if (email.TemplateId > 0)
                {
                    EmailTemplate emailTemplate = await FindTemplate(email.TemplateId);
                    email.Text = emailTemplate.Template;
                }

                if (await SendEmail(email))
                {
                    return Ok("The Email was sent to the recipients");
                }
                else
                {
                    return BadRequest("The email was not sent sucessfully to the recipients");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        public async Task<EmailTemplate> FindTemplate(int templateId)
        {

            try
            {
                //get template
                Uri uri = new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CS_API")), "graphql");

                // var data = GetQueryDynamicsAsync(name, content, filter, page, size, isCB);
                DataSourceConnection ds = new DataSourceConnection
                {
                    Uri = uri.ToString(),
                    IsGraph = true,
                    AllData = true,
                    GraphQL = new GraphQLSetting
                    {
                        Entity = "EmailTemplate",
                        Content = "content{id name subject template  }"
                    },
                    IsDefault = false,
                    QueryParameters = new List<QueryParameterConnection>()
                           {
                               new QueryParameterConnection
                               {
                                   Name="id",
                                   DefaultValue=templateId.ToString(),
                                   Value=templateId.ToString(),
                                   Type= "Int"
                               }

                           }
                };


                var result = await CS_OperationService.ResultFromApiSourcesAsync(ds);
                var convertResult = JsonConvert.SerializeObject(result.result.data);
                List<EmailTemplate> templates = JsonConvert.DeserializeObject<List<EmailTemplate>>(convertResult);
                return templates.FirstOrDefault();

            }
            catch
            {
                return null;
            }


        }
        private async Task<bool> SendEmail(Models.Email emailObject)
        {

            using (Smtp smtp = new Smtp())
            {
                //get variables
                string server = Environment.GetEnvironmentVariable("SMTP_SERVER")!;
                string user = Environment.GetEnvironmentVariable("SMTP_USER")!;
                string pwd = Environment.GetEnvironmentVariable("SMTP_PWD")!;


                smtp.Connect(server);    // or ConnectSSL for SSL
                smtp.StartTLS();
                smtp.UseBestLogin(user, pwd); // remove if authentication is not needed

                ISendMessageResult result = smtp.SendMessage(await CreateEmail(emailObject));
                if (result.Status == SendMessageStatus.Success)
                    return true;


                smtp.Close();
            }
            return false;
        }
        private async Task<IMail> CreateEmail(Models.Email email)
        {
            //create email
            MailBuilder builder = new MailBuilder();
            builder.From.Add(new MailBox(email.From));

            MailAddressParser parser = new MailAddressParser();
            var recipientList = parser.Parse(email.To);

            // builder.Subject = email.Subject;

            // It can be replace by MailGroup
            if (recipientList != null)
                foreach (var recipient in recipientList)
                    builder.To.Add(recipient);


            //attachments management
            if (email.Attachments != null)
            {
                foreach (var formFile in email.Attachments)
                {
                    if (formFile.Length > 0 && (formFile.ContentType.Contains("pdf") || formFile.ContentType.Contains("text/csv")))
                    {
                        using (var ms = new MemoryStream())
                        {
                            formFile.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            MimeData attachment = builder.AddAttachment(fileBytes);
                            attachment.ContentType = formFile.ContentType.Equals("pdf") ? ContentType.ApplicationPdf : ContentType.TextCsv;
                            attachment.FileName = formFile.FileName;
                        }
                    }
                }
            }
            //******//
            string rendered = String.Empty;
            string filePath = String.Empty;
            string sessionId = CS_HttpContext.HttpContext.Session.Id;
            string renderedSubject = String.Empty;
            if (email.File != null)
            {

                filePath = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", $"{email.File.FileName}{sessionId}.txt");


                if (email.File.Length > 0)
                {
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await email.File.CopyToAsync(fileStream);

                    }
                }
                else
                {
                    throw new Exception("Error Rendering the template, an error can be the file or data parameter is missing or wrong");
                }
            }

            //if the email file is null , create one on base text
            if (email.File == null)
            {
                filePath = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", $"{sessionId}.txt");
                if (!System.IO.File.Exists(filePath))
                {
                    using (StreamWriter sw = System.IO.File.CreateText(filePath))
                    {
                        sw.WriteLine("<html>");
                        sw.WriteLine("<body>");
                        sw.WriteLine(email.Text);
                        sw.WriteLine("</body>");
                        sw.WriteLine("</html>");
                    }
                }
            }


            if (!String.IsNullOrEmpty(email.Data))
            {
                //var data = Newtonsoft.Json.JsonConvert.DeserializeObject<ExpandoObject>(email.Data);           
                JObject jdata = JObject.Parse(email.Data);
                Dictionary<string, object> data = new Dictionary<string, object>();

                CreateDictionary(jdata, data);
                rendered = Limilabs.Mail.Templates.Template
                  .FromFile(filePath)
                  .DataFrom(data)
                  .Render();
            }

            System.IO.File.Delete(filePath);

            var filePathSubject = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", $"Subject-{sessionId}.txt");
            if (!System.IO.File.Exists(filePathSubject))
            {
                using (StreamWriter sw = System.IO.File.CreateText(filePathSubject))
                {
                    sw.WriteLine(email.Subject);
                }
            }
            if (!String.IsNullOrEmpty(email.Data))
            {
                JObject jdata = JObject.Parse(email.Data);
                Dictionary<string, object> dataSubject = new Dictionary<string, object>();

                CreateDictionary(jdata, dataSubject);
                renderedSubject = Limilabs.Mail.Templates.Template
                  .FromFile(filePathSubject)
                  .DataFrom(dataSubject)
                  .Render();
            }
            System.IO.File.Delete(filePathSubject);
            
            if (String.IsNullOrEmpty(renderedSubject))
            {
                builder.Subject = email.Subject ;
            }
            else
            {
                builder.Subject = renderedSubject;
            }

            if (String.IsNullOrEmpty(rendered))
            {
                builder.Text = email.Text;

            }
            else
            {
                builder.Html = rendered;
            }


                //create the email
                return builder.Create();


            }

            private void CreateDictionary(JObject o, IDictionary<string, object> dict)
            {
                // do stuff
                foreach (var item in o)
                {
                    JTokenType jTokenType = o[item.Key].Type;
                    //if (jTokenType == JTokenType.String)
                    //{

                    //}

                    //if (jTokenType == JTokenType.Integer)
                    //{
                    //    dict.Add(item.Key, item.Value);
                    //}


                    if (jTokenType == JTokenType.Array)
                    {
                        List<Dictionary<string, object>> listDicts = new List<Dictionary<string, object>>();
                        foreach (JObject jObject in o[item.Key].Children<JObject>())
                        {
                            Dictionary<string, object> subDict = new Dictionary<string, object>();
                            //foreach (JProperty prop in jObject.Properties())
                            //{
                            //    subDict.Add(prop.Name, prop.Value);
                            //}
                            //nested feature
                            CreateDictionary(jObject, subDict);
                            listDicts.Add(subDict);
                        }
                        dict.Add(item.Key, listDicts);

                    }
                    else
                    {
                        dict.Add(item.Key, item.Value);
                    }
                }


            }

        }
    }
