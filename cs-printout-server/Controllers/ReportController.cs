﻿using ChoETL;

using cs_printoutserver.Code;
using cs_printoutserver.Services;
using DevExpress.DataAccess.Json;
using DevExpress.DataAccess.Web;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.Native;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.WebDocumentViewer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;
using Zebra.Sdk.Printer;
using cs_printoutserver.Models;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Data;
using Newtonsoft.Json;
using System.Text.Json;

namespace cs_printoutserver.Controllers
{

    [Route("api/[controller]")]
    [ApiController]

    public class ReportController : BaseController<ReportController>
    {

        private readonly IDataSourceWizardJsonConnectionStorage storage;

        private string TempFileZpl { get; set; }
        public string TempFileCsv { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="storageContext"></param>
        public ReportController(IDataSourceWizardJsonConnectionStorage storageContext)
        {
            storage = storageContext;

        }

        /// <summary>
        /// Method to create a new report for reporting engine
        /// </summary>
        /// <param name="name">Report Name</param>
        /// <returns></returns>
        /// 
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]

        public ActionResult Create(string name)
        {
            name = name.Trim().ToLower();
            try
            {
                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");

                if (!System.IO.File.Exists(fileName))
                {
                    XtraReport xtraReport = new XtraReport {
                        Name = name,
                        DisplayName = name
                    };

                    CS_ReportStorageService.SetDataAsync(xtraReport, name);
                    return Ok();
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    return NotFound(new { message = $"The File {name} already exists" });
                }

            }
            catch
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }



        /// <summary>
        /// Print directly to the printer using the labels created with designer support the same API parameters and pageRange (eg 1-1, or 1-3)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="printerName"></param>   
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult> Print(string name = "default", string printerName = "", [FromQuery] Dictionary<string, string> parameter = null)
        {
            string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");
            int pageFrom = 1;
            int pageTo = 1;
            int pageInterval = 0;

            if (System.IO.File.Exists(fileName))
            {
                XtraReport xtraReport = XtraReport.FromFile(fileName);
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties
                {
                    Parameters = parameter.ToDictionary(pair => pair.Key, pair => (object)pair.Value)
                };

                var jArrayObj = await CS_OperationService.CreateDataSourceFromWebAsync(xtraReport, reportBuildProperties, true);

                string interval = parameter["pageInterval"];
                int.TryParse(interval, out pageInterval);

                if (pageInterval > 0)
                {
                    xtraReport.DataSource = await ExtractData(jArrayObj, pageInterval);
                }
                else
                {
                    xtraReport.DataSource = jArrayObj;
                    //check if pass the rang
                    if (parameter.ContainsKey("pageRange") && parameter["pageRange"] != "0")
                    {
                        string[] range = parameter["pageRange"].Split("-");
                        int.TryParse(range[0], out pageFrom);
                        int.TryParse(range[1], out pageTo);
                        xtraReport.PrintingSystem.StartPrint += (sender, e) => PrintingSystem_StartPrint(sender, e, fromPage: pageFrom, toPage: pageTo);
                    }
                }


                //print

                await xtraReport.CreateDocumentAsync();
                try
                {
                    PrintToolBase tool = new PrintToolBase(xtraReport.PrintingSystem);
                    tool.Print(printerName);
                    return Ok("The print order was sent sucessfully");
                }catch(Exception ex)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return BadRequest(Json(ex.Message));
                }
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }

        }
        /// <summary>
        /// Print From File previusly download
        /// </summary>
        /// <param name="reportName"></param>
        /// <param name="fileResultName"></param>
        /// <param name="printerName"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpGet("[action]"),ActionName("printfromfile")]
        public async Task<ActionResult> PrintFromFileAsync(string reportName = "default",string fileResultName="", string printerName = "", [FromQuery] Dictionary<string, string> parameter = null)
        {
            string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", reportName + ".repx");
            int pageFrom = 1;
            int pageTo = 1;
            int pageInterval = 0;

            if (System.IO.File.Exists(fileName))
            {
                XtraReport xtraReport = XtraReport.FromFile(fileName);
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties
                {
                    Parameters = parameter.ToDictionary(pair => pair.Key, pair => (object)pair.Value)
                };

                var jArrayObj = await CS_OperationService.CreateDataSourceFromFile(fileResultName);

                string interval = parameter["pageInterval"];
                int.TryParse(interval, out pageInterval);

                if (pageInterval > 0)
                {
                    xtraReport.DataSource = await ExtractData(jArrayObj, pageInterval);
                }
                else
                {
                    xtraReport.DataSource = jArrayObj;
                    //check if pass the rang
                    if (parameter.ContainsKey("pageRange") && parameter["pageRange"] != "0")
                    {
                        string[] range = parameter["pageRange"].Split("-");
                        int.TryParse(range[0], out pageFrom);
                        int.TryParse(range[1], out pageTo);
                        xtraReport.PrintingSystem.StartPrint += (sender, e) => PrintingSystem_StartPrint(sender, e, fromPage: pageFrom, toPage: pageTo);
                    }
                }


                //print

                await xtraReport.CreateDocumentAsync();
                try
                {
                    PrintToolBase tool = new PrintToolBase(xtraReport.PrintingSystem);
                    tool.Print(printerName);
                    return Ok("The print order was sent sucessfully");
                }
                catch (Exception ex)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return BadRequest(Json(ex.Message));
                }
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }
        private async Task<JsonDataSource> ExtractData(JsonDataSource jArrayObj, int pageInterval)
        {
            brapi brapiResult = new brapi(new Status(), new Pagination(), new Datafiles(), new Data());
            string result = String.Empty;
            int totalCount = 0;
            result = JsonConvert.SerializeObject(jArrayObj);
            var jsonDataSource = new JsonDataSource();
            if (result != "")
            {
                result = result.Substring(1);
                result = result.Substring(0, result.Length - 1);
                var jsonObj = JToken.Parse(result);
                brapiResult.metadata = JsonConvert.DeserializeObject<Metadata>(jsonObj["metadata"].ToString());
                JArray data = (JArray)jsonObj["result"]["data"];
                List<JToken> filteredData = new List<JToken>();
                List<JToken> dataItems = data.Children().ToList();
                totalCount = dataItems.Count();
                int firstItemIndex = 0;
                int initialInterval = 1;

                for (int i = 0; i < dataItems.Count; i++)
                {
                    var item = dataItems[i];
                    if (item["id"].ToString().Contains("X"))
                    {
                        filteredData.Add(item);
                        firstItemIndex++;
                    }
                    else
                    {
                        if (i == firstItemIndex || i == totalCount - 1 || initialInterval % pageInterval == 0)
                        {
                            filteredData.Add(item);
                        }
                        initialInterval++;
                    }
                }
                brapiResult.result.data = new JArray(filteredData);

                jsonDataSource.JsonSource = new CustomJsonSource(JsonConvert.SerializeObject(brapiResult));
                await jsonDataSource.FillAsync();

            }
            return jsonDataSource;

        }
        /// <summary>
        /// Print directly to the printer using ZPL Code
        /// </summary>
        /// <param name="name"></param>
        /// <param name="ipAddress"></param>
        /// <param name="zplCode"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ActionResult PrintZpl(string name = "default", string ipAddress = "127.0.0.1", string zplCode = "", [FromQuery] Dictionary<string, string> parameter = null)
        {
            Connection zebraPrinter=null;
            try
            {
                string sessionId =CS_HttpContext.HttpContext.Session.Id;
                if (String.IsNullOrEmpty(zplCode))
                {

                    //first case take the zplCode from database
                    zplCode = CS_DBContext.PrintOutTemplates.FirstOrDefault(w => w.Name.ToLower().Equals(name.ToLower())).Zpl;

                    //if ZplCode is empy in the datbase print a message 
                    if(String.IsNullOrEmpty(zplCode))
                        zplCode = System.IO.File.ReadAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", "zplError.zpl"));


                    //define path of the files
                    TempFileCsv = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", $"{name}{sessionId}.csv");
                    TempFileZpl = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", $"{name}{sessionId}.zpl");

                    //create the CSV file
                    ConvertRestToCsv(name,parameter);

                    //print
                    MemoryStream sourceDataStream = new MemoryStream();

                    using (var fileZpl = System.IO.File.OpenWrite(TempFileZpl))
                    using (var fileCsvStream = System.IO.File.Open(TempFileCsv, FileMode.Open))
                    {
                        Byte[] fileContent = new UTF8Encoding(true).GetBytes(zplCode);
                        fileZpl.Write(fileContent, 0, fileContent.Length);
                        fileCsvStream.CopyTo(sourceDataStream);
                        sourceDataStream.Position = 0;
                    }
                    CsvPrinter.Print(ipAddress, sourceDataStream, TempFileZpl, "1", null, true);
                }
                else
                {
                    zebraPrinter = new TcpConnection(ipAddress, TcpConnection.DEFAULT_ZPL_TCP_PORT);

                    zebraPrinter.Open();
                    zebraPrinter.Write(Encoding.UTF8.GetBytes(zplCode));
                }



                return Ok("The print order was sent sucessfully");
            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound(ex.Message);

            }
            finally
            {
                System.IO.File.Delete(TempFileZpl);
                System.IO.File.Delete(TempFileCsv);

                if(zebraPrinter != null)
                    zebraPrinter.Close();
            }



        }


        private async void ConvertRestToCsv(string name, Dictionary<string, string> parameters)
        {
            string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");


            XtraReport xtraReport = null;
            if (System.IO.File.Exists(fileName))
            {
                xtraReport = XtraReport.FromFile(fileName);
                //this code should be reuseble
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties
                {
                    Parameters = parameters.ToDictionary(pair => pair.Key, pair => (object)pair.Value)
                };


                JsonDataSource jsonDataSource = await CS_OperationService.CreateDataSourceFromWebAsync(xtraReport, reportBuildProperties,true);
                UriJsonSource uriJsonSource = jsonDataSource.JsonSource as UriJsonSource;

                RestClient client = new RestClient($"{uriJsonSource.Uri.Scheme}://{uriJsonSource.Uri.Authority}");
                client.AddDefaultHeader("Authorization", uriJsonSource.HeaderParameters["Authorization"].Value.ToString());

                foreach (var queryParam in uriJsonSource.QueryParameters)
                    client.AddDefaultQueryParameter(queryParam.Name, queryParam.Value.ToString());


                foreach (var pathParam in uriJsonSource.PathParameters)
                    client.AddDefaultUrlSegment(pathParam.Name, pathParam.Value.ToString());

                string[] dataMember = xtraReport.DataMember.Split(".");

                var request = new RestRequest(uriJsonSource.Uri.AbsolutePath, DataFormat.Json);
                var response = client.Get(request);

                //need to be dynamic if the datamember is null or if it doesnot exists
                List<ExpandoObject> result = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(response.Content)[dataMember[0]][dataMember[1]].ToObject<List<ExpandoObject>>();
                int idx = 1;
                JsonSchemaNode nodeResult = jsonDataSource.Schema.Nodes.Where(n => n.Value.Name == dataMember[0]).FirstOrDefault<JsonSchemaNode>().Nodes.FirstOrDefault<JsonSchemaNode>();
                ChoCSVRecordConfiguration config = new ChoCSVRecordConfiguration();


                //dont delete can be useful to create api to export
                //configuration of csv headers
                //      config.WithFirstLineHeader();        

                //foreach (JsonSchemaNode node in nodeResult.Nodes)
                //{
                //    config.CSVRecordFieldConfigurations.Add(new ChoCSVRecordFieldConfiguration(node.Name, idx));
                //    idx += 1;
                //}

                using var file = System.IO.File.OpenWrite(TempFileCsv);
                using var parser = new ChoCSVWriter(file);
                parser.Write(result.Take(1));

            }

        }

        private void PrintingSystem_StartPrint(object sender, PrintDocumentEventArgs e,int fromPage=1,int toPage=1)
        {
            // Set the page range.         
            e.PrintDocument.PrinterSettings.FromPage = fromPage;
            e.PrintDocument.PrinterSettings.ToPage = toPage;

        }
        /// <summary>
        /// Get the printer information, valid if the printer is enable 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ActionResult Printers(string ipAddress = "127.0.0.1")
        {
            try
            {

                NetworkDiscoveryHandler networkDiscoveryHandler = new NetworkDiscoveryHandler();
                NetworkDiscoverer.FindPrinters(networkDiscoveryHandler, new List<string> { ipAddress });
                while (!networkDiscoveryHandler.DiscoveryComplete)
                {
                    System.Threading.Thread.Sleep(10);
                }

                if (networkDiscoveryHandler.printers.Any())
                {
                    return Ok($"The printer {networkDiscoveryHandler.printers[0].DiscoveryDataMap["PRODUCT_NAME"]} was found");
                }
                else
                {
                    return NotFound("The printer was not found");

                }


            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound(ex);
            }

        }

        /// <summary>
        /// Delete a Physical Report
        /// </summary>
        /// <param name="name">Report Name</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpDelete("[action]")]

        public ActionResult Delete(string name)
        {
            try
            {
                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");

                if (System.IO.File.Exists(fileName))
                {
                    CS_OperationService.DeleteFileAsync(fileName).Wait();
                    return Ok();
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    return NotFound(new { message = $"The File {name} not exists" });
                }

            }
            catch
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }


        private void PrintConfigLabelUsingDnsName(string dnsName)
        {
            Connection connection = new TcpConnection(dnsName, 9100);
            try
            {
                connection.Open();
                ZebraPrinter p = ZebraPrinterFactory.GetInstance(connection);

                p.PrintConfigurationLabel();
            }
            catch (ConnectionException e)
            {
                Console.WriteLine(e.ToString());
            }
            catch (ZebraPrinterLanguageUnknownException e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                // Close the connection to release resources.
                connection.Close();
            }
        }

        /// <summary>
        /// Copy an existing report
        /// </summary>
        /// <param name="source">Source physical report</param>
        /// <param name="destination">Destination physical report</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public ActionResult Copy(string source, string destination)
        {
            try
            {

                string fileSourceName = Path.Combine(CS_WebHostEnv.ContentRootPath, source.Contains("sys_") ? "SystemTemplates" : "Reports", source + ".repx");
                string fileDestinationName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", destination.ToLower() + ".repx");
                //string fileName = Path.Combine(_env.ContentRootPath, "Reports", orignalName + ".repx");

                if (System.IO.File.Exists(fileSourceName) && !System.IO.File.Exists(fileDestinationName))
                {
                    
                    XtraReport destinationReport = new XtraReport();
                    destinationReport.LoadLayout(fileSourceName);
                    destinationReport.Name = destination.ToLower() + ".repx";
                    destinationReport.DisplayName = destination.ToLower() + ".repx";
                    destinationReport.SaveLayoutToXml(fileDestinationName);
                    if (source.Contains("sys_"))
                    {
                        InsertReportMetadataSysConnections(source, destination);
                    }

                    return Ok(new { message = $"The File was copied" });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    return NotFound(new { message = $"The Report already exists" });
                }

            }
            catch
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }


        /// <summary>
        /// Export a report to different formats
        /// </summary>
        /// <param name="name">Report Name</param>
        /// <param name="format">formats availables (pdf,csv,png)</param>
        /// <param name="parameter">parameter supported by the report eg. parameter[name]</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public  async Task<ActionResult> Export(string name = "template", string format = "pdf", [FromQuery] Dictionary<string, string> parameter = null)
        {
            format = format.ToLower();

            string fileName = String.Empty;
            Boolean isSystem = false;

            fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");
            if(name.Contains("sys_")){
                fileName  = Path.Combine(CS_WebHostEnv.ContentRootPath, "SystemTemplates", name + ".repx");
                isSystem = true;
            }
            if (System.IO.File.Exists(fileName))
            {
                XtraReport xtraReport = XtraReport.FromFile(fileName);
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties
                {
                    Parameters = parameter.ToDictionary(pair => pair.Key, pair => (object)pair.Value)
                };

                xtraReport.DataSource = await CS_OperationService.CreateDataSourceFromWebAsync(xtraReport, reportBuildProperties, true, isSystem);
                return ExportReport(format, xtraReport);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }


        [Authorize(Policy = "API")]
        [HttpGet("[action]"), ActionName("exportjson")]
        public async Task<ActionResult> ExportFromJsonFileAsync(string name = "template", string fileResultName ="", string format = "pdf", [FromQuery] Dictionary<string, string> parameter = null)
        {
            format = format.ToLower();

            string fileName = String.Empty;
            Boolean isSystem = false;

            fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");

            if (name.Contains("sys_"))
            {
                fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "SystemTemplates", name + ".repx");
                isSystem = true;
            }
            if (System.IO.File.Exists(fileName))
            {
                XtraReport xtraReport = XtraReport.FromFile(fileName);
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties
                {
                    Parameters = parameter.ToDictionary(pair => pair.Key, pair => (object)pair.Value)
                };


                xtraReport.DataSource = await CS_OperationService.CreateDataSourceFromFile(fileResultName);
                return ExportReport(format, xtraReport);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }

        /// <summary>
        /// Export data of connection, for know the method only support queryparameters
        /// </summary>
        /// <param name="connectionName"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public async Task<ActionResult> ExportPivotTable(string connectionName, [FromQuery] Dictionary<string, string> parameter = null)
        {
            DataSourceConnection connection = CS_OperationService.GetDataConnections().Where(w => w.Name.Trim().ToLower() == connectionName.Trim().ToLower()).FirstOrDefault();

            if (connection != null)
            {
                string sessionId = CS_HttpContext.HttpContext.Session.Id;
                List<QueryParameterConnection> queryParameters = parameter.Select(s => new QueryParameterConnection
                {
                    Name = s.Key,
                    Type = "String",
                    DefaultValue = s.Value,
                    Value = s.Value
                }).ToList();

                connection.QueryParameters = queryParameters;
                brapi  brapiResult =  await CS_OperationService.ResultFromApiSourcesAsync(connection);
                string result = brapiResult.result.data.ToStringEx();
                List<ExpandoObject> outPutResult = null;
                if (connection.TransposeRowToColumns)
                {
                    JObject jsonObject = JObject.Parse(result);
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonObject["result"]["data"].ToString());
                    result = CS_OperationService.TransponseDatatable(dt, connection);
                    outPutResult = JsonConvert.DeserializeObject<List<ExpandoObject>>(result);
                }

                //prepare file to export
                string fileName = $"{connection.Name}{sessionId}.csv";
                TempFileCsv = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", fileName);
                ChoCSVRecordConfiguration config = new ChoCSVRecordConfiguration();

                using (var file = System.IO.File.OpenWrite(TempFileCsv))
                using (var parser = new ChoCSVWriter(file))
                {
                    parser.Write(outPutResult);
                }
                var resultFile = CS_OperationService.DownloadDocument(fileName,"TempFiles");
                return resultFile;
            }
            else
            {
                return BadRequest("The connection doesn't exist");
            }


        }

        /// <summary>
        /// Get the datasource connections
        /// </summary>
        /// <param name="name">Report Name</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public ActionResult Datasource(string name)
        {
            try
            {
                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");

                JsonDataConnection jsonDataConnection;
                UriJsonSource Uricnn;
                Dictionary<string, string> dicConnections = new Dictionary<string, string>();

                if (System.IO.File.Exists(fileName))
                {
                    XtraReport xtraReport = XtraReport.FromFile(fileName);

                    //get datsoruce from report definitionb
                    var dataSources = new DataContainerEnumerator().EnumerateDataContainers(xtraReport).Select(c => c.DataSource).Distinct();
                    foreach (JsonDataSource ds in dataSources)
                    {
                        if(ds != null)
                        {
                            jsonDataConnection = storage.GetJsonDataConnection(ds.ConnectionName);
                            Uricnn = jsonDataConnection.GetJsonSource() as UriJsonSource;
                            dicConnections.Add(ds.ConnectionName, Uricnn.Uri.ToString());
                        }

                    }

                    var options = new JsonSerializerOptions { WriteIndented = true };
                    string jsonString = System.Text.Json.JsonSerializer.Serialize(dicConnections, options);

                    return new JsonResult(dicConnections);

                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    return NotFound(new { message = $"The File not exists" });
                }

            }
            catch
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }
        /// <summary>
        /// Get all connections available in Printout Server
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public IActionResult GetAllConnections(){
            var result  = CS_OperationService.GetDataConnections();
            return Ok(result);
        }
        /// <summary>
        /// Get all the formats available to convert
        /// </summary>
        /// <returns>Array</returns>
        // [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public IActionResult GetFormats(){
            List<string> formats = new List<string>
            {
                "pdf",
                "docx",
                "xls",
                "xlsx",
                "rtf",
                "mht",
                "html",
                "txt",
                "csv",
                "png"
            };
            return Ok(formats);
        }

        /// <summary>
        /// Get the parameter of the report
        /// </summary>
        /// <param name="nameReport"></param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public IActionResult Parameters(string nameReport)
        {
            try
            {


                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", nameReport + ".repx");

                //  JsonCustomizationService customizationService = new JsonCustomizationService();


                if (System.IO.File.Exists(fileName))
                {
                    XtraReport xtraReport = XtraReport.FromFile(fileName);

                    var result = from s in xtraReport.Parameters.AsQueryable<DevExpress.XtraReports.Parameters.Parameter>()
                                 select new  Models.QueryParameterConnection
                                 {
                                     Name = s.Name,
                                     DefaultValue = s.Value.ToString(),
                                     Value = s.Value.ToString(),
                                     Type= s.Type.Name.ToString()
                                 } ;

                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Ok(result);
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    return NotFound(new { message = $"The Report physical File not exists" });
                }

            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound(ex.Message);
            }
        }
        /// <summary>
        /// Update the Datasource URI of a report
        /// </summary>
        /// <param name="name">Report Name</param>
        /// <param name="uri">New Uri</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPut("[action]")]
        public ActionResult Datasource(string name, string uri)
        {
            try
            {


                string fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", name + ".repx");
                Uri uriInfo = new Uri(uri);
                var source = new UriJsonSource(uriInfo);
                JsonDataConnection jsonDataConnection;
                //  JsonCustomizationService customizationService = new JsonCustomizationService();


                if (System.IO.File.Exists(fileName))
                {
                    XtraReport xtraReport = XtraReport.FromFile(fileName);

                    //get datsoruce from report definitionb
                    var dataSources = new DataContainerEnumerator().EnumerateDataContainers(xtraReport).Select(c => c.DataSource).Distinct();

                    foreach (JsonDataSource ds in dataSources)
                    {

                        jsonDataConnection = new JsonDataConnection(source)
                        {
                            StoreConnectionNameOnly = false,
                            Name = ds.ConnectionName,

                        };
                        storage.SaveConnection(ds.ConnectionName, jsonDataConnection, false);

                    }


                    return Ok();
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.NotFound;
                    return NotFound(new { message = $"The File not exists" });
                }

            }
            catch
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }
        }

        /// <summary>
        /// Delete a Datasource URI of a report
        /// </summary>Method
        /// <param name="connectionName">Connection Name</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public ActionResult DeleteDataSource(string connectionName)
        {
            DeleteJsonDataConnection(connectionName);
            return Ok();
        }

        /// <summary>
        /// Add a new connection
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public ActionResult AddConnection([FromBody]dynamic jsonData)
        {
            DataSourceConnection dataSource = JsonConvert.DeserializeObject<DataSourceConnection>(jsonData.ToString());

            DataSourceConnection dsConnection;
            List<DataSourceConnection> dsConnections = CS_OperationService.GetDataConnections();
            dsConnection = dsConnections.Where(x => x.Name.Trim().ToLower() == dataSource.Name.Trim().ToLower()).FirstOrDefault();

            if (dsConnection != null)
                dsConnections.Remove(dsConnection);

            dsConnection = dataSource;

            dsConnections.Add(dsConnection);
            dynamic jsonConnection = new ExpandoObject();
            jsonConnection.JsonConnections = dsConnections;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonConnection, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", "Connections", "connection.json"), output);

            return Ok();
         }

        /// <summary>
        /// Export with body
        /// </summary>
        /// <param name="name">Report Name</param>
        /// <param name="format">formats availables (pdf,csv,png)</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public async Task<ActionResult> ExportWithBody([FromBody]ExportBody param)
        
        {
           
            string fileName = String.Empty;
            Boolean isSystem = false;
            
             param.Format = param.Format.ToLower();

            fileName = Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", param.Name + ".repx");
            if(param.Name.Contains("sys_")){
                fileName  = Path.Combine(CS_WebHostEnv.ContentRootPath, "SystemTemplates", param.Name + ".repx");
                isSystem = true;
            }
            if (System.IO.File.Exists(fileName))
            {
                XtraReport xtraReport = XtraReport.FromFile(fileName);
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties
                {
                    Parameters = param.ReportParameters.ToDictionary(pair => pair.name, pair => (object)pair.value)
                };

                xtraReport.DataSource = await CS_OperationService.CreateDataSourceFromWebAsync(xtraReport, reportBuildProperties, true, isSystem);
                return ExportReport(param.Format, xtraReport);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return NotFound();
            }

        }
        private  void  DeleteJsonDataConnection(string connectionName)

        {
            DataSourceConnection dsConnection;
            List<DataSourceConnection> dsConnections = CS_OperationService.GetDataConnections();
            dsConnection = dsConnections.Where(x => x.Name.Trim().ToLower() == connectionName.Trim().ToLower()).FirstOrDefault();

            //exist the connection to modify it
            if (dsConnection!=null)
            {
                dsConnections.Remove(dsConnection);
            }
            dynamic jsonConnection = new ExpandoObject();
            jsonConnection.JsonConnections = dsConnections;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonConnection, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(Path.Combine(CS_WebHostEnv.ContentRootPath,"Reports","Connections", "connection.json"), output);


        }




        /// <summary>
        /// Format supported to export
        /// </summary>
        /// <param name="format">format requested</param>
        /// <param name="report">report object</param>
        /// <returns></returns>
        private FileContentResult ExportReport(string format, XtraReport report)
        {

            string contentType = string.Format("application/{0}", format);
            using MemoryStream ms = new MemoryStream();
            switch (format)
            {
                case "pdf":
                    contentType = "application/pdf";
                    report.ExportToPdf(ms);
                    break;
                case "docx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    report.ExportToDocx(ms);
                    break;
                case "xls":
                    contentType = "application/vnd.ms-excel";
                    report.ExportToXls(ms);
                    break;
                case "xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    report.ExportToXlsx(ms);
                    break;
                case "rtf":
                    report.ExportToRtf(ms);
                    break;
                case "mht":
                    contentType = "message/rfc822";
                    report.ExportToMht(ms);
                    break;
                case "html":
                    contentType = "text/html";
                    report.ExportToHtml(ms);
                    break;
                case "txt":
                    contentType = "text/plain";
                    report.ExportToText(ms);
                    break;
                case "csv":
                    contentType = "text/plain";
                    report.ExportToCsv(ms);
                    break;
                case "png":
                    contentType = "image/png";
                    report.ExportToImage(ms, new ImageExportOptions() { Format = System.Drawing.Imaging.ImageFormat.Png });
                    break;
            }
            return File(ms.ToArray(), contentType);



        }

        /// <summary>
        /// Export Templates as ZIP file
        /// </summary>
        /// <param name="templates"></param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public IActionResult DownloadReportsZip([FromBody] dynamic templates)
        {
            var zipFileName = "Download_PrintOut_Reports.zip";
            var tempFile = Path.Combine(CS_WebHostEnv.ContentRootPath, "TempFiles", zipFileName);
            List<PrintOutTemplate> printoutTemplate = JsonConvert.DeserializeObject<List<PrintOutTemplate>>(templates.ToString());

            if (System.IO.File.Exists(tempFile))
                System.IO.File.Delete(tempFile);

            var zipFile = CS_OperationService.DownloadZipFile(CS_WebHostEnv.ContentRootPath, zipFileName, printoutTemplate);

            if (zipFile == null)
            {
                CS_NotyfService.Error("There was an error");
                return NotFound();
            }
            else
            {
                return zipFile;
            }

        }
        /// <summary>
        /// Export Templates as ZIP file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public async Task<IActionResult> UploadReportsZip(IFormFile file)

        {
            //Delete  temp files if exist
            CS_OperationService.DeleteTempFiles(CS_WebHostEnv.ContentRootPath, file.FileName);

            if (file == null || file.Length == 0 &&( file.ContentType != "application/zip" ||  file.ContentType != "application/x-zip-compressed" ) )
            {

                return NotFound();

            }
            else
            {
                try
                {
                    if (await CS_OperationService.UploadZipFileAsync(file, CS_WebHostEnv.ContentRootPath))
                    {
                        await InsertReportMetadata();
                        InsertReportMetadataConnections();
                    }
                    CS_OperationService.DeleteTempFiles(CS_WebHostEnv.ContentRootPath, file.FileName);
                }
                catch (Exception ex)
                {
                    CS_OperationService.DeleteTempFiles(CS_WebHostEnv.ContentRootPath, file.FileName);
                    return NotFound(ex);

                }
            }

            return Ok();
        }

        private async Task InsertReportMetadata()
        {
            Services.PrintOutTemplateService pts = new Services.PrintOutTemplateService(CS_WebHostEnv, CS_DBContext, CS_GraphQLClient, CS_HttpContext, CS_OperationService);
            var jsonString = System.IO.File.ReadAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", "Report_Metadata.json"));
            var listMetadata = JsonConvert.DeserializeObject<List<PrintOutTemplate>>(jsonString);

            foreach (PrintOutTemplate row in listMetadata)
            {
                await pts.InsertReportMetadata(row);
            }

        }
        private void InsertReportMetadataConnections()
        {
            var jsonStringConnection = System.IO.File.ReadAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", "Report_Metadata_Connection.json"));
            var listMetadataConnection = JsonConvert.DeserializeObject<List<DataSourceConnection>>(jsonStringConnection);

            List<DataSourceConnection> targetConnections = CS_OperationService.GetDataConnections();


            foreach (DataSourceConnection row in listMetadataConnection)
            {
                var conn = targetConnections.FirstOrDefault(f => f.Name.Equals(row.Name));
                if (conn == null)
                {
                    targetConnections.Add(row);

                }
                else
                {
                    DateTime dtToday = DateTime.Now;
                    var newName = conn.Name + " / " + dtToday;
                    List<TemplateListConnection> template = new List<TemplateListConnection>(row.TemplateList);
                    CS_OperationService.ChangeConnection(template, CS_WebHostEnv.ContentRootPath, newName);
                    row.Name = newName;
                    targetConnections.Add(row);
                }

            }
            dynamic jsonConnection = new ExpandoObject();
            jsonConnection.JsonConnections = targetConnections;
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonConnection, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", "Connections", "connection.json"), output);

        }
        private void InsertReportMetadataSysConnections(string source = null, string destination = null)
        {
            List<DataSourceConnection> targetSysConnections = CS_OperationService.GetDataConnections(true);
            List<DataSourceConnection> targetConnections = CS_OperationService.GetDataConnections();

            foreach (DataSourceConnection row in targetSysConnections)
            {
                var conn = row.TemplateList.FirstOrDefault(f => f.Name.Equals(source));
                if (conn != null)
                {
                    var connx = targetConnections.FirstOrDefault(f => f.Name.Equals(row.Name));
                    if (connx == null)
                    {
                        var newName = row.Name;
                        row.IsDefault = true;
                        List<TemplateListConnection> templates = new List<TemplateListConnection>();
                        row.TemplateList.Clear();
                        TemplateListConnection _template = new TemplateListConnection();
                        _template.Name = destination;
                        templates.Add(_template);
                        CS_OperationService.ChangeConnection(templates, CS_WebHostEnv.ContentRootPath, newName);
                        row.TemplateList = templates;
                        targetConnections.Add(row);
                    }else{
                        DateTime dtToday = DateTime.Now;
                        var newName = connx.Name + " / " + dtToday;
                        List<TemplateListConnection> templates = new List<TemplateListConnection>();
                        row.TemplateList.Clear();
                        TemplateListConnection _template = new TemplateListConnection();
                        _template.Name = destination;
                        templates.Add(_template);
                        row.Name = newName;
                        CS_OperationService.ChangeConnection(templates, CS_WebHostEnv.ContentRootPath, newName);
                        row.IsDefault = true;
                        row.TemplateList = templates;
                        targetConnections.Add(row);
                    }
                }
            }
            dynamic jsonConnection = new ExpandoObject();
            jsonConnection.JsonConnections = targetConnections;
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonConnection, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(Path.Combine(CS_WebHostEnv.ContentRootPath, "Reports", "Connections", "connection.json"), output);
        }
    }
}
