﻿using AspNetCoreHero.ToastNotification.Abstractions;
using cs_printoutserver.Models;
using cs_printoutserver.Services.Interfaces;
using DevExpress.XtraReports.Web.Extensions;
using GraphQL.Client.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


namespace cs_printoutserver.Controllers
{
    public abstract  class BaseController<T> : Controller where T: BaseController<T>
    {
    
        private  IWebHostEnvironment _env;
        private  CoreSystemContext _context;
        private  IGraphQLClient _client;
        private  INotyfService _notyf;
        private  IHttpContextAccessor _httpContextAccessor;
        private  ILogger<T> _logger;
        private  IOperation _operationService;
        private ReportStorageWebExtension _reportStorage;

        protected ILogger<T> CS_Logger => _logger ?? (_logger = HttpContext?.RequestServices.GetService<ILogger<T>>());
        protected IGraphQLClient CS_GraphQLClient => _client ?? (_client = HttpContext?.RequestServices.GetService<IGraphQLClient>());
        protected CoreSystemContext CS_DBContext => _context ?? (_context = HttpContext?.RequestServices.GetService<CoreSystemContext>());
        protected IWebHostEnvironment CS_WebHostEnv => _env ?? (_env = HttpContext?.RequestServices.GetService<IWebHostEnvironment>());
        protected INotyfService CS_NotyfService => _notyf ?? (_notyf = HttpContext?.RequestServices.GetService<INotyfService>());
        protected IHttpContextAccessor CS_HttpContext => _httpContextAccessor ?? (_httpContextAccessor = HttpContext?.RequestServices.GetService<IHttpContextAccessor>());
        protected IOperation CS_OperationService => _operationService ?? (_operationService = HttpContext?.RequestServices.GetService<IOperation>());
        protected ReportStorageWebExtension CS_ReportStorageService => _reportStorage ?? (_reportStorage = HttpContext?.RequestServices.GetService<ReportStorageWebExtension>());
    }
}
