using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using cs_printoutserver.Models;
using Microsoft.AspNetCore.Authorization;
using cs_printoutserver.Services.Interfaces;

namespace cs_printoutserver.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class FileManagerController : BaseController<FileManagerController>
    {
        private readonly IOperation _operationService;
        public FileManagerController(IWebHostEnvironment env, IOperation operation)
        {

            _env = env;
            _operationService = operation;
        }

        private Task<string> token
        {
            get
            {
                return _operationService.GetTokenAsync();
            }
        }
        private readonly IWebHostEnvironment _env;
        /// <summary>
        /// Method to upload  a new document into Files folder
        /// </summary>
        /// <param name="program">string</param>
        /// <param name="file">file</param>
        /// <returns></returns>
        /// 
        [Authorize(Policy = "API")]
        [HttpPost("[action]")]
        public async Task<IActionResult> UploadDocument(IFormFile file,[FromForm] string program)

        {
            var result = await _operationService.UploadDocumentAsync(file,program);
            if (result)
                return Ok("Document uploaded correctly.");
            else
                return NotFound("There was an error uploading the file.");

        }

        /// <summary>
        /// Method to download a list with all the existing documents
        /// </summary>
        /// <returns></returns>
        // [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        [Produces("application/json")]
        public IActionResult GetSystemTemplateList()

        {
            brapi result = _operationService.QueryResultSystem();
            return Ok(result);
        }
        /// <summary>
        /// Template list
        /// </summary>
        /// <returns></returns>
        // [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        [Produces("application/json")]
        public IActionResult TemplateList()

        {
            brapi result = _operationService.QueryResultTemplates();
            return Ok(result);
        }
        /// <summary>
        /// Method to download a list with all the existing documents
        /// </summary>
        /// <param name="program">Document</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        [Produces("application/json")]
        public IActionResult GetDocumentList(string program)

        {
            brapi result = _operationService.QueryResult(program);
            return Ok(result);
        }
        /// <summary>
        /// Method to download a document from Files Folder
        /// </summary>
        /// <param name="file_name">File name</param>
        /// <returns></returns>
        /// 
        [Authorize(Policy = "API")]
        [HttpGet("[action]")]
        public IActionResult DownloadDocument(string file_name)
        {
            var dir = _env.ContentRootPath;
            var path = Path.Combine(dir, "Files", file_name);
            if (!System.IO.File.Exists(path))
            {
                return NotFound("Not found");
            }

            var result = _operationService.DownloadDocument( file_name);
            return result;
        }

        /// <summary>
        /// Delete a Physical Document in PrintOut server
        /// </summary>
        /// <param name="file_name">Report Name</param>
        /// <returns></returns>
        [Authorize(Policy = "API")]
        [HttpDelete("[action]")]

        public async Task<IActionResult>  DeleteDocument(string file_name)
        {
            var path = Path.Combine(_env.ContentRootPath, "Files", file_name);
            if (!System.IO.File.Exists(path))
                return NotFound();
            var result = await _operationService.DeleteFileAsync(path);
            if (result)
                return Ok("Document Deleted correctly.");
            else
                return NotFound("There was an error deleting the file.");

        }

    }

}