using System.Collections.Generic;

namespace cs_printoutserver.Models
{
    public class Instance
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<DomainInstance> DomainInstances { get; set; }

    }
}
