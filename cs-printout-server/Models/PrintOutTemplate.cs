﻿using cs_printoutserver.Binders;
using Microsoft.AspNetCore.Mvc;
namespace cs_printoutserver.Models
{

    [ModelBinder(BinderType =typeof(CoreSystemBinder))]
    public class PrintOutTemplate
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string  Zpl { get; set; }
        public string  Tenant { get; set; }
        public string Product { get; set; }
        public string Program { get; set; }
        public string ProductIds { get; set; }
        public string ProgramIds { get; set; }
        public string ReportName { get; set; }
        public string ReportPath { get; set; }
        public long PhysicalSize { get; set; }
        public bool IsHealth { get; set; }
        public string DefaultFormat { get; set; }
        public string Label { get; set; }
    }
}
