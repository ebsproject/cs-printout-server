﻿namespace cs_printoutserver.Models
{
    using Microsoft.AspNetCore.Http;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class Email
    {
        [Required, Display(Name ="From(*)", Order =1,Description = "Email address of who sent the email")]        
        public string From { get; set; }
        [Required, Display(Name = "To(*)", Order = 2, Description = "Email address of the email recipients eg. email@domain.org or Recipient Name <email@domain.org>")]
        public string To { get; set; }
        [Required, Display(Name = "Subject(*)", Order = 3, Description = "Subject of the email")]
        public string Subject { get; set; }
        
        [Display(Name = "Text", Order = 4, Description = "Email Body in text format")]
        public string? Text { get; set; }

        
        [Display(Name = "Template File", Order = 5, Description = "Html Template Email where contain the field tags of data")]
        public IFormFile? File { get; set; }
        [Display(Name = "Json Data", Order = 6, Description = "Json payload used in the Html Template")]
        public string? Data { get; set; }
        [Display(Name = "Attachements", Order = 7, Description = "Email Attachment, only support pdf files")]
        public IEnumerable<IFormFile>? Attachments { get; set; } 


        [Display(Name = "TemplateId", Order = 8, Description = "Find Template Id")]
        public int TemplateId { get; set; }

    }
}
