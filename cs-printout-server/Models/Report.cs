﻿namespace cs_printoutserver.Models
{
    public class Report
    {
        public PrintOutTemplate GeneralInformation { get; set; }
        public DataSourceConnection DatasourceConn { get; set; }
        public ReportPrinter PrinterInformation { get; set; }
    }
}
