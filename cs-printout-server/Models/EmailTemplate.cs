﻿namespace cs_printoutserver.Models
{
    public class EmailTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
    }
}
