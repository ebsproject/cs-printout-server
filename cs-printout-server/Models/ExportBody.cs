using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace cs_printoutserver.Models
{

    /// <summary>
    /// 
    /// </summary>
    public class ExportBody
    {
        [Required]
        public string Name { get; set; }
        public string Format { get; set; } = "pdf";
        public List<Parameter> ReportParameters { get; set; }

    }
    public class Parameter
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}