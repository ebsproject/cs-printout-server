﻿using System.Collections.Generic;



namespace cs_printoutserver.Models
{
    public enum MaterialStatus
    {
        info = 1,
        warning = 2,
        success = 3,
        error = 4
    }
    public class brapi
    {
        public brapi()
        {

        }
        public brapi(Status status,Pagination pagination,Datafiles datafiles,Data data)
        {
            metadata = new Metadata
            {
                status = new List<Status> { status},
                pagination = pagination,
                datafiles = new List<Datafiles> { datafiles }
            };
            result = data;

        }
        public Metadata metadata { get; set; }
        public Data result { get; set; }

    }
    public class Metadata
    {
        public Pagination pagination { get; set; }
        public List<Status> status { get; set; }
        public List<Datafiles> datafiles { get; set; }
    }
    public class Pagination
    {
        public int totalCount { get; set; }
        public int pageSize { get; set; }
        public int totalPages { get; set; }
        public int currentPage { get; set; }
    }
    public class Status
    {
        public MaterialStatus messageType { get; set; }
        public string message { get; set; }
    }
    public class Datafiles { }
    public class Data
    {
        public dynamic data { get; set; }
    }

    
}
