﻿using System.Collections.Generic;

namespace cs_printoutserver.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public string UserName { get; set; }
    
        public IEnumerable<Role> Roles { get; set; }
        public Person Person { get; set; }

    }

    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class Person
    {
        public int Id { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string JobTitle { get; set; }

    }
}
