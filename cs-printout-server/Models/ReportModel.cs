﻿namespace cs_printoutserver.Models
{
    public class ReportModel
    {
        public string name { get; set; }
        public string fullName { get; set; }
        public long size { get; set; }
    }
}
