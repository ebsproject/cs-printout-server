﻿using System.Collections.Generic;

namespace cs_printoutserver.Models
{
    public class Tenant
    {
        public int Id { get; set; }
        public string Name { get; set; }

          public IEnumerable<Instance> Instances { get; set; }
    }
}
