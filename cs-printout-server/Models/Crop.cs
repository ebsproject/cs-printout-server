﻿namespace cs_printoutserver.Models
{
    public class Crop
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
