﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace cs_printoutserver.Models
{

    /// <summary>
    /// 
    /// </summary>
    public class DataSourceConnection
    {

       

        [Required]
        [StringLength(150, MinimumLength = 5)]
        public string Name { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 20)]
        public string Uri { get; set; }
        public string Type { get; set; }
        public bool IsDefault { get; set; }
        public bool IsGraph { get; set; } = false;     
        public bool ConvertQueryToBody { get; set; } = false;
        public bool TransposeRowToColumns { get; set; } = false;
        public bool AllData { get; set; } = true;

        public TrasposeSetting TransposeSetting { get; set; } = null;

        [JsonConverter(typeof(StringEnumConverter))]
        public RestSharp.Method HttpMethod { get; set; }
        public ICollection<QueryParameterConnection> QueryParameters { get; set; }
        public ICollection<PathParameterConnection> PathParameters { get; set; }
        public ICollection<TemplateListConnection> TemplateList { get; set; }
        public GraphQLSetting GraphQL { get; set; }

        [StringLength(150, MinimumLength = 5)]
        public string CreatedBy { get; set; }

        [StringLength(150, MinimumLength = 5)]
        public string CreatedOn { get; set; }

        [StringLength(150, MinimumLength = 5)]
        public string UpdateBy { get; set; }

        [StringLength(150, MinimumLength = 5)]
        public string UpdateOn { get; set; }
    }

    public class TrasposeSetting {
        public string rowHeader { get; set; }
        public string column { get; set; }
        public string rowValue { get; set; }
    }

    public class QueryParameterConnection
    {
        [Required]
        public string Name { get; set; }
        public string ParameterAPIName { get; set; } = "";
        [Required]
        public string Type { get; set; }
        [Required]
        public string Value { get; set; }
        //has the objetive to get the schema from api
        public string DefaultValue { get; set; }
    }
    public class PathParameterConnection
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Value { get; set; }
        //has the objetive to get the schema from api
        public string DefaultValue { get; set; }
    }
    public class TemplateListConnection
    {
        [Required]
        public string Name { get; set; }
    }

    public class GraphQLSetting
    {
        public string Content { get; set; }
        public string Entity { get; set; }
    }

    public class GraphQLFilter
    {
        public enum Mode
        {
            EQ,
            LK
        }
        [JsonProperty("col")]
        public string Col { get; set; }

        [JsonProperty("mod")]
        public string Mod { get; set; }
        [JsonProperty("val")]
        public string Val { get; set; }

    }
}
