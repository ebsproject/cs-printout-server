﻿using Microsoft.EntityFrameworkCore;


namespace cs_printoutserver.Models
{
    public class CoreSystemContext: DbContext
    {
        public CoreSystemContext(DbContextOptions<CoreSystemContext> options) : base(options) { }
       
        public DbSet<PrintOutTemplate> PrintOutTemplates { get; set; }
        public DbSet<Profile> Profile { get; set; }

      
      
    }
}
