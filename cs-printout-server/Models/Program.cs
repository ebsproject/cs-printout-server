﻿namespace cs_printoutserver.Models
{
    public class Program
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Tenant Tenant { get; set; }
        public CropProgram CropProgram { get; set; }
    }
}
