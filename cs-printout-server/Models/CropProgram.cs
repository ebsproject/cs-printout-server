﻿namespace cs_printoutserver.Models
{
    public class CropProgram
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Crop Crop { get; set; }
    }
}
