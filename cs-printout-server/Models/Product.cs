﻿namespace cs_printoutserver.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Domain Domain { get; set; }
    }
}
