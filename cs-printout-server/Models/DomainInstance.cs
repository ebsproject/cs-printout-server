namespace cs_printoutserver.Models
{
    public class DomainInstance
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string SgContext { get; set; }
        public string Context { get; set; }

    }
}
