﻿using System.Text.Json.Serialization;

namespace cs_printoutserver.Models
{
    public class TokenResponse
    {
        [JsonPropertyName("TokenType")]
        public string? TokenType { get; set; }
        [JsonPropertyName("AccessToken")]
        public string? AccessToken { get; set; }
        [JsonPropertyName("IdToken")]
        public string? IdToken { get; set; }
    }

    public class TokenBody
    {
        [JsonPropertyName("AuthParameters")]
        public AuthParameter AuthParameter { get; set; }

        [JsonPropertyName("AuthFlow")]
        public string AuthFlow { get; set; } = "USER_PASSWORD_AUTH";
        [JsonPropertyName("ClientId")]
        public string ClientId { get; set; }
    }

    public class AuthParameter
    {
        [JsonPropertyName("USERNAME")]
        public string UserName { get; set; }
        [JsonPropertyName("PASSWORD")]
        public string Password { get; set; }
    }
}
