using System;

namespace cs_printoutserver.Models
{
    public class DocumentsModel
    {

        public string folder { get; set; }
        public string documentName {get; set;}
        public string extension {get;set;}
        public DateTime created{get; set;}
        
    }
}