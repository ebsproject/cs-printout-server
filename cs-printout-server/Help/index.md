
The EBS users will use different kinds of printouts such as stickers, tags, and lists in their operations. The printout manager is the product that is used to define the available printout templates and to set the usage of the templates.

# User Documentation

The user documentation is divided into four target platforms: 
 
* [End-user Documentation](articles/index.md)
* [Technical Documentation](api/index.md)
 
