# Printout Server Design

The Printout Server is developed under **model-view-controller** design patter.However, the function of the component
is provide the services to design, export, and print reports trought backend capabilities.


## Design

![](../images/EBS_PrintoutDesign.png)


![](../images/EBS_Step1.png) ASP.net core component to expose the API to print, export, and design reports
with [DevExpress Webreporting Component](https://docs.devexpress.com/XtraReports/400249/web-reporting/asp-net-core-reporting/end-user-report-designer-in-asp-net-core-applications). The services
that the component expose can be consumed by any Webapp, or backend process.

![](../images/EBS_Step2.png) To create a new report for EBS is necesarry login EBS Tenant Portal and use the 
[designer](../articles/report-designer.md) to create or maintain a report (tabular, dashboard, or labels).

![](../images/EBS_Step3.png) The services exposed are available for any client Web or Backend. see the API Swagger Documentations.

![](../images/EBS_Step4.png) To print report using Zebra Printer for labels created with ZPL Code, the administrator
can management the printer available on the institution using the menu option ![](../images/EBS_Menu_PrinterManagement.png)
