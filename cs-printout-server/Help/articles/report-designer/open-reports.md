---
title: Add New Reports
author: Anna Vekhina
---
# Open a Report

This topic explains how to create a new report in the Report Designer.
 

**Step 1**

In the Portal go to the Settings and Select the Product **PrintOut Manager**, click **New**.


![](../../images/EBS_SettingMenu.png)

**Step 2**


click **Design** Button.![](../../images/EBS_OpenAReport.png)  

![](../../images/eurd-web-report-bands.png)


The created report contains three [bands](introduction-to-banded-reports.md) - **Top Margin**, **Detail**, and **Bottom Margin**. Refer to the [Use Report Elements](use-report-elements.md) section for information on how to add controls to the report.
