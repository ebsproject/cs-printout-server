# File Management

The **File Management** allows you to upload and store documents related to programs in the PrintOut Server, also provides APIs to management. 





![](../images/file-management-home.png)
There are 4 main actions:

* [Upload a document](file-management/upload-document.md)
* [Download a document](file-management/download-document.md)
* [Get document list](file-management/get-document-list.md)
* [Delete a document](file-management/delete-document.md)