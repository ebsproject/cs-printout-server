# Upload a document
To upload a document:
1. Select the program related.
2. Select a file from your computer (pdf, excel,word, jpg, png).
3. Click in the button ![image-home](../../images/file-management-upload-btn.png)


![image-home](../../images/file-management-upload.png)

The document will appear in the list 

![image-home](../../images/file-management-new-document.png)