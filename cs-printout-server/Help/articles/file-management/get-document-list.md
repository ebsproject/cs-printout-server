# Get a document list
You can get a list of all the documents related to a program

To get the list

1. Select the program related to the documents
2. Click in the button  ![get-list-image](../../images/file-management-getlist-btn.png)

You will get a document list in your browser

> [!NOTE]
> You can get a JSON format file if you click in the button  ![get-list-image](../../images/file-management-getlistJSON-btn.png), the file will download to your computer.

