# Delete a document
To delete a document:
1. Click in the delete button  ![image-delete-btn](../../images/file-management-delete-btn.png)  in the desired document.

