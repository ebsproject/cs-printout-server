# Download a document
To download a document:
1. Click in the download button ![image-home](../../images/file-management-download-btn.png) in the desired document

The document will be downloaded to your computer

 ![image-home](../../images/file-management-download-win.png)
