﻿using cs_printoutserver.Models;
using cs_printoutserver.Services.Interfaces;
using DevExpress.DataAccess.Json;
using DevExpress.XtraReports.Native;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.WebDocumentViewer;
using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using cs_printoutserver.Code;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using Newtonsoft.Json.Converters;
using System.Runtime.CompilerServices;
using System.Threading;

namespace cs_printoutserver.Services
{
    public class OperationService : IOperation
    {
        private readonly ILogger<OperationService> _logger;
        private readonly IWebHostEnvironment _env;
        private readonly CoreSystemContext _context;
        private readonly IGraphQLClient _client;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OperationService(IWebHostEnvironment env,
                                        CoreSystemContext context,
                                        IGraphQLClient client,
                                        IHttpContextAccessor httpContextAccessor,
                                        ILogger<OperationService> logger
                                        )
        {
            _logger = logger;
            _env = env;
            _context = context;
            _client = client;
            _httpContextAccessor = httpContextAccessor;
        }
        public string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        public string TransponseDatatable(DataTable inputDT, DataSourceConnection ds)
        {
            var rows = inputDT.AsEnumerable().Select(s => new
            {
                cols = s.Field<string>(ds.TransposeSetting.column),
                rowHeader = s.Field<string>(ds.TransposeSetting.rowHeader),
                values = s.Field<object>(ds.TransposeSetting.rowValue)
            });


            var result = rows.ToPivotTable(
                   item => item.cols,
                       item => item.rowHeader,
                       items => items.Any() ? items.FirstOrDefault().values : ""
                );

            return JsonConvert.SerializeObject(result, new KeyValuePairConverter());
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".json", "application/json"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
              //{".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
        public void ChangeConnection(List<TemplateListConnection> template, string contentRootPath, string newName)
        {
            foreach (var item in template)
            {
                string fileName = String.Empty;
                fileName = Path.Combine(contentRootPath, "Reports", item.Name + ".repx");
                if (System.IO.File.Exists(fileName))
                {
                    XtraReport xtraReport = XtraReport.FromFile(fileName);
                    var rptDatasources = new DataContainerEnumerator().EnumerateDataContainers(xtraReport).Select(c => c.DataSource).Distinct();
                    foreach (JsonDataSource ds in rptDatasources)
                    {
                        if (ds != null)
                        {
                            ds.Name = ds.Name.Replace(ds.Name, newName);
                            ds.ConnectionName = newName;
                            break;
                        }
                    }
                    xtraReport.SaveLayoutToXml(fileName);
                }
            }
        }

        public async Task<JsonDataSource> CreateDataSourceFromWebAsync(XtraReport report, ReportBuildProperties reportBuildProperties, bool isRequestFromReportSubmit = false, bool isSystem = false)
        {
            var jsonDataSource = new JsonDataSource();
            if (report.Name.Contains("sys_"))
                isSystem = true;
            //  get datasource from the xml report
            var rptDatasources = new DataContainerEnumerator().EnumerateDataContainers(report).Select(c => c.DataSource).Distinct();


            foreach (JsonDataSource ds in rptDatasources)
            {
                if (ds != null)
                {

                    DataSourceConnection dsConnection = GetDataConnections(isSystem).Where(w => w.Name ==ds.ConnectionName).SingleOrDefault();

                    //review parameters

                    
                    #region DataConnectionPreparation
                    if (_httpContextAccessor.HttpContext != null)
                    {
                        var _client = _httpContextAccessor.HttpContext?.RequestServices.GetService<IGraphQLClient>();
                        var _dbContext = _httpContextAccessor.HttpContext?.RequestServices.GetService<CoreSystemContext>();

                        PrintOutTemplateService printOutTemplateService = new PrintOutTemplateService(_env, _dbContext, _client, _httpContextAccessor, this);
                        var domains = printOutTemplateService.getTenant().Result;

                        dsConnection.Uri = GetSgContext(dsConnection, domains.result.data);
                    }
                    {
                        //it is probably a subreport request
                        if (reportBuildProperties.Parameters.ContainsKey("token"))                        
                            dsConnection.QueryParameters.Add(new QueryParameterConnection { Name = "token", DefaultValue = (string)reportBuildProperties.Parameters["token"]!});
                        
                    }

                    //TODO: Improve with Linq
                    string paramRealName = String.Empty;
                    foreach (var query in dsConnection.QueryParameters)
                    {
                        paramRealName = query.Name.Split(".").Length == 1 ? query.Name : query.Name.Split(".")[1];
                        if (reportBuildProperties.Parameters.ContainsKey(paramRealName))
                            query.DefaultValue = (string)reportBuildProperties.Parameters.GetValueOrDefault(paramRealName)!;
                    }


                    //TODO: Improve with Linq
                    foreach (var path in dsConnection.PathParameters)
                    {
                        paramRealName = path.Name.Split(".").Length == 1 ? path.Name : path.Name.Split(".")[1];
                        if (reportBuildProperties.Parameters.ContainsKey(paramRealName))
                            path.DefaultValue = (string)reportBuildProperties.Parameters.GetValueOrDefault(paramRealName)!;

                    }
                    #endregion


                    var arrayResult = await ResultFromApiSourcesAsync(dsConnection, isRequestFromReportSubmit, isSystem);

                    
          
                    jsonDataSource.JsonSource = new CustomJsonSource(JsonConvert.SerializeObject(arrayResult));

                    await jsonDataSource.FillAsync();
                    break;
                }
            }
            return jsonDataSource;
        }

        public async Task<bool> DeleteFileAsync(string path)
        {
            bool delete = await Task.Run(() =>
            {
                try
                {
                    System.IO.File.Delete(path);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            });

            return delete;
        }

        public void DeleteTempFiles(string _contentRootPath = "", string fileName = "")
        {
            var jsonPathTemp = Path.Combine(_contentRootPath, "TempFiles", "Report_Metadata.json");
            if (System.IO.File.Exists(jsonPathTemp))
                System.IO.File.Delete(jsonPathTemp);
            var jsonPath = Path.Combine(_contentRootPath, "Reports", "Report_Metadata.json");
            if (System.IO.File.Exists(jsonPath))
                System.IO.File.Delete(jsonPath);
            var fileNamePath = Path.Combine(_contentRootPath, "TempFiles", fileName);
            if (System.IO.File.Exists(fileNamePath))
                System.IO.File.Delete(fileNamePath);
            var jsonPathTempConn = Path.Combine(_contentRootPath, "TempFiles", "Report_Metadata_Connection.json");
            if (System.IO.File.Exists(jsonPathTempConn))
                System.IO.File.Delete(jsonPathTempConn);
            var jsonPathConn = Path.Combine(_contentRootPath, "Reports", "Report_Metadata_Connection.json");
            if (System.IO.File.Exists(jsonPathConn))
                System.IO.File.Delete(jsonPathConn);
        }

        public FileStreamResult DownloadDocument(string file_name, string directory = "Files")
        {
            var dir = _env.ContentRootPath;
            var path = Path.Combine(dir, directory, file_name);
            var stream = File.OpenRead(path);
            var contentType = GetContentType(path);
            stream.Position = 0;
            return new FileStreamResult(stream, contentType)
            { FileDownloadName = file_name };
        }

        public FileStreamResult DownloadZipFile(string dir, string zipFileName, List<PrintOutTemplate> printoutTemplate)
        {
            var temZipFile = Path.Combine(dir, "TempFiles", zipFileName);
            var path = Path.Combine(dir, "Reports");

            List<DataSourceConnection> dsConnections = GetDataConnections();
            List<DataSourceConnection> dsConnectionsNew = new List<DataSourceConnection>();

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            var countValidfiles = fileInfo.Count(w => w.Extension != ".cs");
            if (countValidfiles > 0)
            {
                using (var archive = ZipFile.Open(temZipFile, ZipArchiveMode.Create))
                {
                    List<PrintOutTemplate> templatesMetadata = new List<PrintOutTemplate>();
                    foreach (var template in printoutTemplate)
                    {
                        FileInfo file = fileInfo.FirstOrDefault(f => f.Name.Equals(template.ReportPath));
                        if (file != null)
                        {

                            archive.CreateEntryFromFile(file.FullName, file.Name, CompressionLevel.Optimal);
                            templatesMetadata.Add(template);
                            List<TemplateListConnection> templates = new List<TemplateListConnection>();
                            TemplateListConnection _template = new TemplateListConnection();
                            foreach (var item in dsConnections)
                            {
                                var conn = item.TemplateList.FirstOrDefault(f => f.Name.Equals(template.Name));
                                if (conn != null)

                                {      //Improve in future
                                    foreach (var list in templates)
                                    {
                                        _template.Name = template.Name;
                                        templates.Add(_template);
                                        item.TemplateList = templates;
                                    }
                                    if (!dsConnectionsNew.Contains(item))
                                        dsConnectionsNew.Add(item);

                                }
                            }

                        }

                    }
                    string jsonFilePath = Path.Combine(dir, "TempFiles", "Report_Metadata.json");
                    string jsonFile = System.Text.Json.JsonSerializer.Serialize(templatesMetadata);
                    File.WriteAllText(jsonFilePath, jsonFile);
                    archive.CreateEntryFromFile(jsonFilePath, "Report_Metadata.json", CompressionLevel.Optimal);

                    string jsonFilePathConn = Path.Combine(dir, "TempFiles", "Report_Metadata_Connection.json");
                    string jsonFileConn = System.Text.Json.JsonSerializer.Serialize(dsConnectionsNew);
                    File.WriteAllText(jsonFilePathConn, jsonFileConn);
                    archive.CreateEntryFromFile(jsonFilePathConn, "Report_Metadata_Connection.json", CompressionLevel.Optimal);

                }

                var stream = File.OpenRead(temZipFile);
                var fileStreamResult =
                new FileStreamResult(stream, "application/zip")
                {
                    FileDownloadName = zipFileName
                };
                DeleteTempFiles();
                return fileStreamResult;

            }
            else return null;
        }

      

        public List<DataSourceConnection> GetDataConnections(bool isSystem = false)
        {
            string pathWebRoot = _env != null ? _env.ContentRootPath : AppContext.BaseDirectory;

            if (!System.IO.File.Exists(Path.Combine(pathWebRoot, "Reports", "Connections", "connection.json")))
            {

                var pathFolder = Path.Combine(_env.ContentRootPath, "Reports", "Connections");
                System.IO.Directory.CreateDirectory(pathFolder);
                System.IO.File.Copy(Path.Combine(_env.ContentRootPath, "connection.json"), Path.Combine(_env.ContentRootPath, "Reports", "Connections", "connection.json"));

            }
            String path = isSystem ? Path.Combine(pathWebRoot, "SystemTemplates", "sys_connection.json")
            : Path.Combine(pathWebRoot, "Reports", "Connections", "connection.json");

            JObject objSettings = JObject.Parse(File.ReadAllText(path));
            IList<JToken> conns = objSettings["JsonConnections"].Children().ToList();

            DataSourceConnection connection;
            List<DataSourceConnection> listConnection = new List<DataSourceConnection>();

            //TODO: perform this code with LINQ
            foreach (JToken cnn in conns)
            {
                connection = cnn.ToObject<DataSourceConnection>();
                listConnection.Add(connection);
            }


            return listConnection;
        }

        public DataSourceConnection GetJsonDataConnection(string reportName, bool isSystem = false)
        {
            DataSourceConnection dsConnection;
            List<DataSourceConnection> dsConnections = GetDataConnections(isSystem);


            XtraReport xtraReport = XtraReportInfo(reportName);
            JsonDataSource jsonDataSource = (new DataContainerEnumerator().EnumerateDataContainers(xtraReport).Select(c => c.DataSource).Distinct()).FirstOrDefault() as JsonDataSource;


            if (jsonDataSource != null)
            {
                dsConnection = dsConnections.Where(x => x.Name.Trim().ToLower() == jsonDataSource.ConnectionName.Trim().ToLower()).FirstOrDefault();
                return dsConnection;
            }
            else return null;
        }

        public string GetSgContext(DataSourceConnection ds, dynamic domains)
        {
            var Uri = String.Empty;
            var sg_context = String.Empty;
            List<Host> host = new List<Host>();
            var hostName = Dns.GetHostName();

            foreach (var domain in domains)
            {
                host.Add(new Host { DomainId = domain.Id, SgContext = domain.DomainInstance[0].SgContext });

            }

            switch (ds.Type)
            {
                case "cbapi":
                    Uri = ds.IsGraph ? Environment.GetEnvironmentVariable("ENDPOINT_CB_GRAPHQL") + "/graphql" : string.Format(ds.Uri, host.Where(h => h.DomainId == 1).ToList()[0].SgContext);
                    break;
                case "csapi":
                    sg_context = host.Where(h => h.DomainId == 2).ToList()[0].SgContext;
                    sg_context = sg_context.EndsWith("/") ? sg_context : sg_context + "/";
                    Uri = string.Format(ds.Uri, sg_context);
                    break;
                case "baapi":
                    Uri = string.Format(ds.Uri, host.Where(h => h.DomainId == 3).ToList()[0].SgContext);
                    break;
                case "smapi":
                    sg_context = host.Where(h => h.DomainId == 4).ToList()[0].SgContext;
                    sg_context = sg_context.EndsWith("/") ? sg_context : sg_context + "/";
                    Uri = string.Format(ds.Uri, sg_context);
                    break;
                case "csps":
                    Uri = string.Format(ds.Uri, $"http://{hostName}/");
                    break;
                default:
                    Uri = ds.Uri;
                    break;
            }

            return Uri;
        }

        public async Task<string> GetTokenAsync()
        {
            string token = String.Empty;

            await _httpContextAccessor.HttpContext.GetTokenAsync("id_token");
            StringValues svToken;
            if (String.IsNullOrEmpty(token))
            {
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("Authorization", out svToken);
                token = svToken.ToString().Split(' ')[1];
            }

            return token;
        }

        public brapi QueryResult(string program)
        {
            brapi result;
            var path = Path.Combine(_env.ContentRootPath, "Files");
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            List<DocumentsModel> documents = new List<DocumentsModel>();
            foreach (var doc in fileInfo)
            {
                documents.Add(new DocumentsModel { documentName = doc.Name, extension = doc.Extension, folder = program, created = doc.CreationTime });
            }
            result = new brapi(new Status { message = "Success", messageType = MaterialStatus.success },
                               new Pagination { currentPage = 0, totalCount = documents.Select(a => new { name = a.documentName }).Where(a => a.name.Contains(program)).Count(), totalPages = 1, pageSize = 20 },
                               new Datafiles(),
                               new Data
                               {
                                   data = documents.Select(s => new
                                   {
                                       DocumentName = s.documentName,
                                       Folder = s.folder,
                                       Program = s.folder,
                                       CreatedDate = s.created,
                                       Url = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}/api/filemanager/downloadDocument?file_name={s.documentName}"

                                   }).Where(a => a.DocumentName.Contains(program)).ToArray()
                               }
                               );

            return result;
        }

        public brapi QueryResultSystem()
        {
            brapi result;
            var path = Path.Combine(_env.ContentRootPath, "SystemTemplates");
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            List<DocumentsModel> documents = new List<DocumentsModel>();
            foreach (var doc in fileInfo)
            {
                documents.Add(new DocumentsModel { documentName = doc.Name, extension = doc.Extension, created = doc.CreationTime });
            }
            result = new brapi(new Status { message = "Success", messageType = MaterialStatus.success },
                               new Pagination { currentPage = 0, totalCount = documents.Select(a => new { name = a.documentName }).Count(), totalPages = 1, pageSize = 20 },
                               new Datafiles(),
                               new Data
                               {
                                   data = documents.Select(s => new
                                   {
                                       DocumentName = s.documentName.Split(".")[0],
                                       CreatedDate = s.created,
                                       Url = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}/api/filemanager/downloadDocument?file_name={s.documentName}"

                                   }).ToArray()
                               }
                               );

            return result;
        }

        public brapi QueryResultTemplates()
        {
            brapi result;
            var path = Path.Combine(_env.ContentRootPath, "Reports");
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            List<DocumentsModel> documents = new List<DocumentsModel>();
            foreach (var doc in fileInfo)
            {
                documents.Add(new DocumentsModel { documentName = doc.Name, extension = doc.Extension, created = doc.CreationTime });
            }
            result = new brapi(new Status { message = "Success", messageType = MaterialStatus.success },
                               new Pagination { currentPage = 0, totalCount = documents.Select(a => new { name = a.documentName }).Count(), totalPages = 1, pageSize = 20 },
                               new Datafiles(),
                               new Data
                               {
                                   data = documents.Select(s => new
                                   {
                                       DocumentName = s.documentName.Split(".")[0],
                                       CreatedDate = s.created,
                                       Url = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}/api/filemanager/downloadDocument?file_name={s.documentName}"

                                   }).ToArray()
                               }
                               );

            return result;
        }
        private static async IAsyncEnumerable<JToken> RunPagedRestQueryAsync(RestClient client,string token,
                                                                        DataSourceConnection dataSourceConnection,
                                                                        [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
 
            bool hasMorePages = true;
            int currentPage = 1;
         

           


            while (hasMorePages)
            {
                RestRequest request = new RestRequest(dataSourceConnection.HttpMethod);
                request.AddHeader("Content-Type", "application/json"); 
                
                request.AddHeader("Authorization", $"Bearer {token}");
                                

                if (!dataSourceConnection.ConvertQueryToBody)
                {
                    //if (dataSourceConnection.QueryParameters.Where(w => w.Name == "page").FirstOrDefault() ==null)
                    //    request.AddQueryParameter("page", currentPage.ToString());
                    
                    foreach (QueryParameterConnection queryParam in dataSourceConnection.QueryParameters)
                    {
                        if (queryParam.Name.Equals("page"))
                        {
                            request.AddQueryParameter(queryParam.Name, currentPage.ToString());
                        }
                        else
                        {
                            request.AddQueryParameter(queryParam.Name, queryParam.DefaultValue);
                        }
                    }

                    if (request.Parameters.Where(w => w.Name == "page").FirstOrDefault() == null)
                        request.AddQueryParameter("page", currentPage.ToString());

                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);

                  

                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        writer.Formatting = Newtonsoft.Json.Formatting.Indented;
                        writer.WriteStartObject();
                        foreach (QueryParameterConnection queryParam in dataSourceConnection.QueryParameters)
                        {                           
                            if (queryParam.Name != "page" && queryParam.Name != "sort" && queryParam.Name != "limit")
                            {
                                writer.WritePropertyName(queryParam.Name);
                                writer.WriteValue(queryParam.DefaultValue);
                            }
                            else
                            {
                                request.AddQueryParameter(queryParam.Name, queryParam.DefaultValue);
                            }
                                
                        }
                        if(request.Parameters.Where(w=>w.Name == "page").FirstOrDefault() == null)
                            request.AddQueryParameter("page", currentPage.ToString());

                        writer.WriteEndObject();
                    }

                    //add body
                    request.AddJsonBody(sb.ToString());
                }


                foreach (PathParameterConnection pathParam in dataSourceConnection.PathParameters)
                    request.AddUrlSegment(pathParam.Name, pathParam.DefaultValue);

                IRestResponse t = await client.ExecuteAsync(request);
                JObject results = JObject.Parse(t.Content.ToString()!);
                int totalAccount = (int)results["metadata"]!["pagination"]!["totalCount"]!;
                int totalPages = (int)results["metadata"]!["pagination"]!["totalPages"]!;
                hasMorePages = totalPages > currentPage;
                ++currentPage;
                foreach (JObject content in data(results)["result"]!["data"]!)
                    yield return content;
            }

            JObject data(JObject result) => (JObject)result;


        }

        private static async IAsyncEnumerable<JToken> RunPagedQueryAsync(GraphQLHttpClient client,
                                                                         DataSourceConnection dataSourceConnection,
                                                                         [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            int currentPage = 1;
            int currentSize = 300;
            bool disjunctionFilters = false;

            JArray sort = new JArray();
            List<Models.GraphQLFilter> filtersList = new List<Models.GraphQLFilter>();
            Models.GraphQLFilter filter = null;
            string sortString = String.Empty;


            foreach (QueryParameterConnection queryParam in dataSourceConnection.QueryParameters.Where(w=>!w.Name.Equals("token")))
            {
                if (queryParam.Name != "page" && queryParam.Name != "size" && queryParam.Name != "sort")
                {
                    string[] subFilter = queryParam.DefaultValue.Split(',');
                    disjunctionFilters = (subFilter.Length > 1 && !disjunctionFilters) || disjunctionFilters ? true : false;
                    for (int i = 0; i < subFilter.Length; i++)
                    {
                        filter = new GraphQLFilter
                        {
                            Col = queryParam.Name,
                            Mod = "EQ",
                            Val = subFilter[i]
                        };
                        filtersList.Add(filter);
                    }
                }
                else
                {
                    if (queryParam.Name == "page")
                        currentPage = int.Parse(queryParam.DefaultValue);
                    if (queryParam.Name == "size")
                        currentSize = int.Parse(queryParam.DefaultValue);

                    if (queryParam.Name == "sort")
                    {
                        //sort = JArray.Parse(queryParam.DefaultValue);
                        sortString = queryParam.DefaultValue.ToString().Replace("\"col\"", "col").Replace("\"mod\"", "mod");
                    }
                }
            }

            // filters:{filtersList.ToArray()}

            var filters = JsonConvert.SerializeObject(filtersList, Newtonsoft.Json.Formatting.Indented);
            var filtersWithOutQuue = filters.Replace("\"col\"", "col").Replace("\"mod\"", "mod").Replace("\"val\"", "val").Replace("\"EQ\"", "EQ");





            bool hasMorePages = true;
           
            while (hasMorePages)
            {

                var query = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{find{dataSourceConnection.GraphQL.Entity}List(
                   filters:{filtersWithOutQuue}
                    {(String.IsNullOrEmpty(sortString) ? String.Empty : String.Concat("sort:", sortString))}
                    disjunctionFilters:{disjunctionFilters.ToString().ToLower()}
                           page: {{
                                        number: {currentPage}
                                        size:{currentSize}
                                    }}    
                    ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                {dataSourceConnection.GraphQL.Content}
                                }}
                            }}"
                };
                var request = new GraphQLRequest(query);
                var response = await client.SendQueryAsync<dynamic>(request);


                var results = response.Data[$"find{dataSourceConnection.GraphQL.Entity}List"];

                int pagesReturned = (int)results["totalPages"]!;
                
                hasMorePages = pagesReturned > currentPage;
                ++currentPage;
                foreach (JObject content in data(results)["content"]!)
                    yield return content;
            }

            JObject data(JObject result) => (JObject)result;
         

        }

        public async Task<brapi> ResultFromApiSourcesAsync(DataSourceConnection dsConnection, bool isRequestFromReportSubmit = true, bool isSystem = false)
        {

            string subReportToken = String.Empty;
            JArray finalResults = new JArray();
            brapi brapiResult = new brapi(new Status(), new Pagination(), new Datafiles(), new Data());
            var uri = new Uri(dsConnection.Uri);
            var cancellation = new CancellationTokenSource();

            QueryParameterConnection queryToken = dsConnection.QueryParameters.Where(w => w.Name.Equals("token")).SingleOrDefault();
            if (queryToken != null)
                subReportToken = queryToken.DefaultValue;

            string token = String.IsNullOrEmpty(subReportToken) ? await GetTokenAsync() : subReportToken;

                 if (dsConnection.IsGraph)
                {                    

                    var graphQLOptions = new GraphQLHttpClientOptions
                    {
                        EndPoint = uri,
                    };

                    GraphQLHttpClient graphClient = new GraphQLHttpClient(graphQLOptions, new NewtonsoftJsonSerializer());

                
                    graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                  


                    await foreach (var data in RunPagedQueryAsync(graphClient, dsConnection, cancellation.Token))                 
                        finalResults.Add(data);
                 

                }

                if (!dsConnection.IsGraph)
                {
                    RestClient restClient = new RestClient(uri);
                   
                    await foreach (var data in RunPagedRestQueryAsync(restClient, token,dsConnection, cancellation.Token))                    
                        finalResults.Add(data);
                    
                }

                brapiResult.metadata.pagination.pageSize = finalResults.Count;
                brapiResult.metadata.pagination.currentPage = 1;
                brapiResult.metadata.pagination.totalPages = 1;
                brapiResult.metadata.pagination.totalCount = finalResults.Count;
                brapiResult.metadata.status = new List<Status>
                {
                    {new Status { message = "return data", messageType= MaterialStatus.success} }
                };


            brapiResult.result.data = finalResults;
            return brapiResult;
        }

        public List<QueryParameterConnection> RetrieveParametersFromReport(string nameReport)
        {
            string fileName = Path.Combine(_env.ContentRootPath, "Reports", nameReport + ".repx");

            if (System.IO.File.Exists(fileName))
            {
                XtraReport xtraReport = XtraReport.FromFile(fileName);

                var result = from s in xtraReport.Parameters.AsQueryable<DevExpress.XtraReports.Parameters.Parameter>()
                             select new Models.QueryParameterConnection
                             {
                                 Name = s.Name,
                                 DefaultValue = s.Value.ToString(),
                                 Value = s.Value.ToString(),
                                 Type = s.Type.Name.ToString()
                             };
                return result.ToList();
            }
            else
            {
                return new List<QueryParameterConnection>();
            }
        }

        public bool UnpackFilesFromZIP(string dir, string path)
        {
            var stream = new FileStream(path, FileMode.Open);

            try
            {
                using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Read, false))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        string destinationPath = Path.GetFullPath(Path.Combine(dir, "Reports", entry.FullName));
                        if (System.IO.File.Exists(destinationPath))
                            System.IO.File.Delete(destinationPath);
                        entry.ExtractToFile(destinationPath);
                    }
                }
                stream.Close();
                return true;
            }
            catch (Exception e)
            {
                stream.Close();
                return false;
            }
        }

        public async Task<bool> UploadDocumentAsync(IFormFile file, string program)
        {
            string programCode = program;
            var path = "";
            if (file == null || file.Length < 0)
            {
                return false;

            }
            var ext = Path.GetExtension(file.FileName);
            var dir = _env.ContentRootPath;
            if (programCode != "false")
            {
                path = Path.Combine(dir, "Files", String.Format("{0}-{1}", programCode, file.FileName));
            }
            else
            {
                path = Path.Combine(dir, "Files", file.FileName);
            }

            if (System.IO.File.Exists(path))
            {
                return false;

            }
            if (file == null || file.Length == 0)
                return false;
            var stream = new FileStream(path, FileMode.Create);
            await file.CopyToAsync(stream);
            stream.Close();
            return true;
        }

        public async Task<bool> UploadZipFileAsync(IFormFile file, string dir)
        {
            var path = Path.Combine(dir, "TempFiles", file.FileName);

            var stream = new FileStream(path, FileMode.Create);
            await file.CopyToAsync(stream);
            stream.Close();

            //unpack report .repx files from zip file
            return UnpackFilesFromZIP(dir, path);
        }

        public XtraReport XtraReportInfo(string name)
        {

            string fileName = String.Empty;
            XtraReport xtraReport = null;
            fileName = Path.Combine(_env.ContentRootPath, "Reports", name + ".repx");
            if (name.Contains("sys_"))
            {
                fileName = Path.Combine(_env.ContentRootPath, "SystemTemplates", name + ".repx");
                 
            }
           
            if (System.IO.File.Exists(fileName))
            {
                xtraReport = XtraReport.FromFile(fileName);
            }
            return xtraReport;
        }

        Task<JsonDataSource> IOperation.CreateDataSourceFromFile(string fileName)
        {
            throw new NotImplementedException();
        }
    }
}
