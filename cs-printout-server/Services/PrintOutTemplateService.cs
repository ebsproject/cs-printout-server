﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using cs_printoutserver.Models;
using GraphQL.Client.Http;
using Microsoft.Extensions.Logging;
using GraphQL.Client.Abstractions;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using cs_printoutserver.Services.Interfaces;

namespace cs_printoutserver.Services
{
    public class PrintOutTemplateService
    {
        private readonly ILogger<PrintOutTemplateService> _logger;
        private readonly IWebHostEnvironment _env;
        private readonly CoreSystemContext _context;
        private readonly IGraphQLClient _client;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOperation _operation;
        //ILogger<PrintOutTemplateService> logger


        private string _token
        {
            get
            {

                return _operation.GetTokenAsync().GetAwaiter().GetResult();
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        /// <param name="context"></param>
        /// <param name="client"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="operation"></param>
        public PrintOutTemplateService(IWebHostEnvironment env,
                                        CoreSystemContext context,
                                        IGraphQLClient client,
                                        IHttpContextAccessor httpContextAccessor, IOperation operation)
        {

            _env = env;
            _context = context;
            _client = client;
            _httpContextAccessor = httpContextAccessor;
            _operation = operation;
        }

public async Task<brapi> getTenant()
        {
            brapi result;
            var query = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findDomainList(page: {{
                                        number: {1}
                                        size:{20}
                                    }}){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                            id
                                            name
                                            domaininstances{{
                                                    id
                                                    sgContext
                                                    context
                                                }}
                                       }}
                                }}
                            }}"
            };

            using (GraphQLHttpClient graphClient = _client as GraphQLHttpClient)
            {
                try
                {
                    graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                    var response = await graphClient.SendQueryAsync<dynamic>(query);
                    List<Models.Domain> tenantResult = response.Data[$"findDomainList"]["content"].ToObject<List<Models.Domain>>();
                    result = new brapi(new Status { message = "Success", messageType = MaterialStatus.success },
                                       new Pagination
                                       {
                                           currentPage = response.Data[$"findDomainList"]["number"],
                                           totalCount = response.Data[$"findDomainList"]["totalElements"],
                                           totalPages = response.Data[$"findDomainList"]["totalPages"],
                                           pageSize = response.Data[$"findDomainList"]["size"]
                                       },
                                       new Datafiles(),
                                       new Data
                                       {
                                           data = tenantResult.Select(s => new
                                           {
                                               Id = s.Id,
                                               Name = s.Name,
                                               DomainInstance = s.DomainInstances

                                           }).ToArray()
                                       }
                                       );
                }
                catch (Exception ex)
                {
                    result = new brapi(new Status { message = ex.Message, messageType = MaterialStatus.error },
                                       new Pagination { currentPage = 0, totalCount = 0, totalPages = 0, pageSize = 0 },
                                       new Datafiles(),
                                       new Data()
                                       );
                }
            }
            return result;
        }
   public async Task<brapi> getProgram()
        {
            brapi result;
            var query = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findProgramList(page: {{
                                        number: {1}
                                        size:{20}
                                    }}){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                            id
                                            name
                                            code
                                       }}
                                }}
                            }}"
            };

            using (GraphQLHttpClient graphClient = _client as GraphQLHttpClient)
            {
                try
                {
                    graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                    var response = await graphClient.SendQueryAsync<dynamic>(query);
                    List<Models.ProgramModel> programResult = response.Data[$"findProgramList"]["content"].ToObject<List<Models.ProgramModel>>();
                    result = new brapi(new Status { message = "Success", messageType = MaterialStatus.success },
                                       new Pagination
                                       {
                                           currentPage = response.Data[$"findProgramList"]["number"],
                                           totalCount = response.Data[$"findProgramList"]["totalElements"],
                                           totalPages = response.Data[$"findProgramList"]["totalPages"],
                                           pageSize = response.Data[$"findProgramList"]["size"]
                                       },
                                       new Datafiles(),
                                       new Data
                                       {
                                           data = programResult.Select(s => new
                                           {
                                               Id = s.Id,
                                               Name = s.Name,
                                               Code = s.Code

                                           }).ToArray()
                                       }
                                       );
                }
                catch (Exception ex)
                {
                    result = new brapi(new Status { message = ex.Message, messageType = MaterialStatus.error },
                                       new Pagination { currentPage = 0, totalCount = 0, totalPages = 0, pageSize = 0 },
                                       new Datafiles(),
                                       new Data()
                                       );
                }
            }
            return result;
        }

        /// <summary>
        /// Find the user logged to determinate what it is the role, because only the Administrator can use the console
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task FindUserLogged(string userEmail, string token)
        {

            //   await Task.Run(() => { 
            var query = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findUserList(
                                filters:{{
                                  col: ""userName"",
                                  mod: EQ,
                                  val: ""{userEmail.ToLower()}""
                                }}

                                ){{
                                 totalElements
                                 content {{
                                            id
                                            userName
                                            person {{
                                                id
                                                givenName
                                                familyName
                                                officialEmail
                                                jobTitle
                                            }}
                                           roles {{
                                                id
                                                name
                                            }}
                                       }}
                                }}
                            }}"
            };

            using (GraphQLHttpClient graphClient = _client as GraphQLHttpClient)
            {
                graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await graphClient.SendQueryAsync<dynamic>(query);
                IList<Profile> result = response.Data[$"findUserList"]["content"].ToObject<List<Profile>>();
                _context.Profile.AddRange(result);
                _context.SaveChanges();
            }

            //   });
        }

        /// <summary>
        /// Load the query to merge files and templates metadata
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="programId"></param>
        /// <returns></returns>
        public async Task LoadPrintOutTemplates(int productId = 0, int programId = 0)
        {
            List<PrintOutTemplate> printOutTemplates;
            List<string> filters = new List<string>();
        

            try
            {
                if (_httpContextAccessor.HttpContext != null)
                {

                    if (productId >0)
                    {
                        filters.Add($@"{{
                                        mod: EQ
                                        val: ""{productId}""
                                        col: ""products.id""
                                        }}");
                    };

                    if (programId>0)
                    {
                        filters.Add($@"{{
                                        mod: EQ
                                        val: ""{programId}""
                                        col: ""programs.id""
                                        }}");
                    };



                    var query = new GraphQL.GraphQLRequest
                    {


                        Query = @$"{{findPrintoutTemplateList(
                            filters:[{String.Join("",filters)}]
                             page: {{number:{1}
                                     size:{300}
                                    }}
                            ){{
                                 totalPages
                                 content {{
                                            id
                                            name
                                            description
                                            zpl
                                            defaultFormat
                                            label
                                            programs {{
                                                id
                                                name                                                
                                            }}
                                            products {{
                                                id
                                                name  
                                                domain {{
                                                    name
                                                }}
                                            }}
                                       }}
                                }}
                            }}"
                    };



                    IEnumerable<ReportModel> reportModels = Directory.EnumerateFiles(Path.Combine(_env.ContentRootPath, "Reports"), "*.repx")
                            .Select(s => new FileInfo(s))
                            .Select(file => new ReportModel
                            {
                                fullName = file.Name,
                                name = Path.GetFileNameWithoutExtension(file.FullName),
                                size = file.Length
                            });



                    GraphQLHttpClient clientTenant = _client as GraphQLHttpClient;



                    clientTenant.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                    var response = await clientTenant.SendQueryAsync<dynamic>(query);
                    IList<dynamic> result = response.Data[$"findPrintoutTemplateList"]["content"].ToObject<List<dynamic>>();

                    IEnumerable<PrintOutTemplate> leftOuterJoin = (from item in result.ToList<dynamic>()
                                                                   join file in reportModels on ((string)item.name).ToLower() equals file.name.ToLower() into files
                                                                   from f in files.DefaultIfEmpty()
                                                                   select new PrintOutTemplate
                                                                   {
                                                                       Id = item.id,
                                                                       Name = item.name,
                                                                       Description = item.description,
                                                                       Zpl = item.zpl,
                                                                       Program = String.Join(",", ((IList<dynamic>)item.programs.ToObject<List<dynamic>>()).Select(s => s.name)),
                                                                       Product = String.Join(",", ((IList<dynamic>)item.products.ToObject<List<dynamic>>()).Select(s => String.Format("{0}-{1}", s.domain.name, s.name))),
                                                                       ProgramIds = String.Join(",", ((IList<dynamic>)item.programs.ToObject<List<dynamic>>()).Select(s => s.id)),
                                                                       ProductIds = String.Join(",", ((IList<dynamic>)item.products.ToObject<List<dynamic>>()).Select(s => s.id)),
                                                                       ReportName = f == null ? String.Empty : f.name,
                                                                       ReportPath = f == null ? String.Empty : f.fullName,
                                                                       PhysicalSize = f == null ? 0 : f.size,
                                                                       IsHealth = f != null ? true : false,
                                                                       DefaultFormat = item.defaultFormat,
                                                                       Label = item.label,


                                                                   });

                    IEnumerable<PrintOutTemplate> rightOuterJoin = (from file in reportModels
                                                                    join item in result.ToList<dynamic>() on file.name.ToLower() equals ((string)item.name).ToLower() into templates
                                                                    from t in templates.DefaultIfEmpty()
                                                                    select new PrintOutTemplate
                                                                    {
                                                                        Id = t == null ? (new Random()).Next(10000, 90000000) : t.id,
                                                                        Name = t == null ? String.Empty : t.name,
                                                                        Description = t == null ? String.Empty : t.description,
                                                                        Zpl = t == null ? String.Empty : t.zpl,
                                                                        Program = t == null ? String.Empty : String.Join(",", ((IList<dynamic>)t.programs.ToObject<List<dynamic>>()).Select(s => s.name)),
                                                                        Product = t == null ? String.Empty : String.Join(",", ((IList<dynamic>)t.products.ToObject<List<dynamic>>()).Select(s => String.Format("{0}-{1}", s.domain.name, s.name))),
                                                                        ProgramIds = t == null ? String.Empty : String.Join(",", ((IList<dynamic>)t.programs.ToObject<List<dynamic>>()).Select(s => s.id)),
                                                                        ProductIds = t == null ? String.Empty : String.Join(",", ((IList<dynamic>)t.products.ToObject<List<dynamic>>()).Select(s => s.id)),
                                                                        ReportName = file.name,
                                                                        ReportPath = file.fullName,
                                                                        PhysicalSize = file.size,
                                                                        IsHealth = t != null ? true : false,
                                                                        DefaultFormat = t == null ? String.Empty : t.defaultFormat,
                                                                        Label = t == null ? String.Empty : t.label,
                                                                    
                                                                    });

                    //  leftOuterJoin = leftOuterJoin.Union(rightOuterJoin);

                    printOutTemplates = rightOuterJoin.ToList();
                }
                else
                {
                    printOutTemplates = new List<PrintOutTemplate>();
                }
            }
            catch (Exception ex)
            {
                printOutTemplates = new List<PrintOutTemplate>();

            }

            _context.AddRange(printOutTemplates);
            _context.SaveChanges();


        }


        public async Task InsertReportMetadata(PrintOutTemplate row)
        {
            GraphQLHttpClient graphClient = _client as GraphQLHttpClient;    
            var query = new GraphQL.GraphQLRequest
            {
                                    Query = @"
                                            query FIND_PRINTOUT_TEMPLATE_LIST(
                                            $page: PageInput
                                            $filters: [FilterInput]
                                            $disjunctionFilters: Boolean = false
                                        ) {
                                            findPrintoutTemplateList(
                                            page: $page
                                            filters: $filters
                                            disjunctionFilters: $disjunctionFilters
                                            ) {
                                            content {
                                                id
                                                name
                                            }
                                            }
                                        } ",
                OperationName = "FIND_PRINTOUT_TEMPLATE_LIST",
                Variables = new { page = new { number = 1, size = 10 }, disjunctionFilters = false, filters = new { col = "name", mod = "EQ", val = row.Name } }

            };
            
            try
            {
                graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                var response = await graphClient.SendQueryAsync<dynamic>(query);
                List<Models.PrintOutTemplate> result = response.Data[$"findPrintoutTemplateList"]["content"].ToObject<List<Models.PrintOutTemplate>>();
                
                if (result != null){
                foreach (var item in result)
                
            {
                
                var mutationDelete = new GraphQL.GraphQLRequest
                            {
                                Query = @"
                                        mutation DELETE_PRINTOUT_TEMPLATE(
                                        $printoutTemplateId: Int!
                                        
                                    ) {
                                        deletePrintoutTemplate(
                                        printoutTemplateId: $printoutTemplateId                                        
                                        ) 
                                    }",
                                OperationName = "DELETE_PRINTOUT_TEMPLATE",
                                Variables = new { printoutTemplateId = item.Id }
                            };

                            graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                            var responseDelete = await graphClient.SendQueryAsync<dynamic>(mutationDelete);
                            ;
            }  
            }
            }
            catch
            {

            }
                      
                var query1 = new GraphQL.GraphQLRequest
                {
                    Query = @"
                       mutation CREATE_PRINTOUT_TEMPLATE($printoutTemplate: PrintoutTemplateInput!) {
                       createPrintoutTemplate(printoutTemplate: $printoutTemplate) {
                       id
                       name
                       description
                       zpl
                     }
               }",
                    OperationName = "CREATE_PRINTOUT_TEMPLATE",
                    Variables = new { printoutTemplate = new { id = 0, name = row.ReportName, description = row.Description, zpl = row.Zpl } }
                };


                try
                {
                    graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                    var response = await graphClient.SendQueryAsync<dynamic>(query1);
                    //Get the ID created from executed query
                    var resultId = Int32.Parse(response.Data[$"createPrintoutTemplate"].id.Value);
                    if (resultId > 0)
                    {
                        string[] extract_programIds = row.ProgramIds.Split(",");
                        string[] extract_productIds = row.ProductIds.Split(",");
                        int[] programIds = Array.ConvertAll(extract_programIds, s => int.Parse(s));
                        int[] productIds = Array.ConvertAll(extract_productIds, s => int.Parse(s));

                        if (productIds.Length > 0)
                        {
                            var queryProducts = new GraphQL.GraphQLRequest
                            {
                                Query = @"
                                        mutation ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS(
                                        $printoutTemplateId: Int!
                                        $productIds: [Int!]!
                                    ) {
                                        addPrintoutTemplateToProducts(
                                        printoutTemplateId: $printoutTemplateId
                                        productIds: $productIds
                                        ) {
                                        id
                                        }
                                    }",
                                OperationName = "ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS",
                                Variables = new { printoutTemplateId = resultId, productIds = productIds }
                            };

                            graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                            var responseProducts = await graphClient.SendQueryAsync<dynamic>(queryProducts);
                            ;
                        }

                        if (programIds.Length > 0)
                        {
                            var queryPrograms = new GraphQL.GraphQLRequest
                            {
                                Query = @"
                                    mutation ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS(
                                    $printoutTemplateId: Int!
                                    $programIds: [Int!]!
                                ) {
                                    addPrintoutTemplateToPrograms(
                                    printoutTemplateId: $printoutTemplateId
                                    programIds: $programIds
                                    ) {
                                    id
                                    }
                                }",
                                OperationName = "ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS",
                                Variables = new { printoutTemplateId = resultId, programIds = programIds }
                            };
                            graphClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                            var responsePrograms = await graphClient.SendQueryAsync<dynamic>(queryPrograms);
                            ;
                        }
                    }
                }
                catch (Exception ex)
                {

                } 
        }        
    }
}
