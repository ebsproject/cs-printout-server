﻿using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.Web.WebDocumentViewer;

namespace cs_printoutserver.Services
{
    public class CachedReportSourceWebResolver : ICachedReportSourceWebResolver
    {
        ICSReportSource ReportSource { get; }
        public CachedReportSourceWebResolver(ICSReportSource reportSource)
        {
            ReportSource = reportSource;
        }
        public bool TryGetCachedReportSourceWeb(string reportEntry, out CachedReportSourceWeb cachedReportSourceWeb)
        {
            var report = ReportSource.GetReport(reportEntry);
            if (report == null)
            {
                cachedReportSourceWeb = null;
                return false;
            }

            cachedReportSourceWeb = new CachedReportSourceWeb(report);
            return true;
        }
    }

    public interface ICSReportSource
    {
        XtraReport GetReport(string reportName);
        Dictionary<string, string> GetReportList();
    }
}
