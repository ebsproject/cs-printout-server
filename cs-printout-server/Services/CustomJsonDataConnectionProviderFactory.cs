﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using cs_printoutserver.Models;
using cs_printoutserver.Services.Interfaces;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Json;
using DevExpress.DataAccess.Web;
using DevExpress.XtraReports.Services;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.Web.Extensions;
using DevExpress.XtraReports.Web.ReportDesigner.Services;
using DevExpress.XtraReports.Web.WebDocumentViewer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace cs_printoutserver.Services
{


    public class CustomJsonDataConnectionProviderFactory: IJsonDataConnectionProviderFactory
    {

        Dictionary<string, string> connectionStrings;

        protected IWebHostEnvironment _hostEnvironment { get; }
        protected IHttpContextAccessor _httpContextAccessor { get; }
        protected IOperation _operation { get; }
        public CustomJsonDataConnectionProviderFactory(IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor, IOperation operation)


        {
            connectionStrings = (new Services.CustomJsonConnection(env, httpContextAccessor,operation)).GetConnections();
            _hostEnvironment = env;
            _httpContextAccessor = httpContextAccessor;
            _operation = operation;
        }
       
        public IJsonDataConnectionProviderService Create()
        {
            return new WebDocumentViewerJsonDataConnectionProvider(connectionStrings,_operation);
           
        }

       
    }

    //public class JsonCustomizationService : IJsonSourceCustomizationService
    //{
    //    public JsonSourceBase CustomizeJsonSource(JsonDataSource jsonDataSource)
    //    {

    //        return new DevExpress.DataAccess.Json.UriJsonSource(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_BSO_API")));
    //    }



    //}

  
    public class PreviewReportCustomService : PreviewReportCustomizationService
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly IDataSourceWizardJsonConnectionStorage _storage;
        private readonly IWebHostEnvironment _env;
        private readonly IOperation _operation;

        public PreviewReportCustomService(IWebHostEnvironment env,                                         
                                          IHttpContextAccessor httpContext, 
                                          IDataSourceWizardJsonConnectionStorage storageContext,IOperation operation )
        {
            _httpContext = httpContext;
            _storage = storageContext;
            _env = env;
            _operation = operation;
        }

        public override Task CustomizeReportAsync(XtraReport report)
        {
            var subReports = report.AllControls<XRSubreport>();

            foreach (var subReport in subReports)
            {
                var sToken = _operation.GetTokenAsync().Result;
                report.Parameters.Add(new DevExpress.XtraReports.Parameters.Parameter { Name = "token", Value = sToken, Visible = false });
                //   subReport.ParameterBindings.Add(new ParameterBinding("prmCategory", null, "Categories.CategoryID"));
                subReport.BeforePrint += (sender, e) => xrSubreport1_BeforePrint(sender, e);

                //subReport.BeforePrint += (s, e) =>
                //{
                //    ((XRSubreport)s).ReportSource.DataSource = ((XRSubreport)s).Report.DataSource;
                //};
            }
            return base.CustomizeReportAsync(report);
        }
        public override void CustomizeReport(XtraReport report)
        {

        
            
            base.CustomizeReport(report);
        }
        private void xrSubreport1_BeforePrint(object sender, object e)
        {
            XRSubreport subreport = sender as XRSubreport;
            XtraReport detailReport = subreport.ReportSource as XtraReport;
            XtraReport masterReport = detailReport.MasterReport as XtraReport;
            
            if (detailReport != null)
            {
     
                //pass data  
                ReportBuildProperties reportBuildProperties = new ReportBuildProperties();
                reportBuildProperties.Parameters = new Dictionary<string, object>();

                foreach(var subParam in subreport.ParameterBindings)
                {
                   if(subParam.Parameter !=null)
                        reportBuildProperties.Parameters.Add(subParam.Parameter.Name, (object)subParam.Parameter.Value);
                }               
                    

                if (masterReport.Parameters["token"].Name != null)
                {
                    DevExpress.XtraReports.Parameters.Parameter param = masterReport.Parameters["token"];
                    reportBuildProperties.Parameters.Add(param.Name, param.Value);
                }

                //check group filters
                foreach(var keyValuePairs in RetrieveGroupParameter(masterReport.Bands))
                {
                    reportBuildProperties.Parameters.Add(keyValuePairs.Key, keyValuePairs.Value);
                }

                //var arrayResult =  OperationExtension.ResultFromApiSources(_env, _httpContext, "", reportBuildProperties.Parameters, false, false).Result;
                //TODO: Improve this workarount, after first render of subreport change the connection name
                XtraReport originalSubReport;

                string fileName = Path.Combine(_env.ContentRootPath, "Reports", detailReport.Name + ".repx");
                originalSubReport = detailReport;
                if (File.Exists(fileName))
                {
                    originalSubReport = XtraReport.FromFile(fileName);
                }
               

                detailReport.DataSource = _operation.CreateDataSourceFromWebAsync(originalSubReport, reportBuildProperties).Result;
                subreport.ReportSource.FillDataSource();
                

            }
        }

        public Dictionary<string, object> RetrieveGroupParameter(BandCollection bands)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            foreach (Band band in bands)
            {
                if (band.Name.ToUpper() == "SUBREPORTPARAM")
                {
                    XRControl param = band.FindControl("fieldparamsubreport", true);
                    if (param != null)
                        result.Add((string)param.Tag, param.Value);
                }
            }

            
            return result;
        }
        public override Task<CachedReportSourceWeb> CreateCachedReportSourceAsync(XtraReport report)
        {
            return base.CreateCachedReportSourceAsync(report);
        }
    }

    public class WebDocumentViewerLogger : WebDocumentViewerOperationLogger
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly IDataSourceWizardJsonConnectionStorage _storage;
        private readonly IWebHostEnvironment _env;
        private readonly IOperation _operation;
        public WebDocumentViewerLogger(IWebHostEnvironment env, IHttpContextAccessor httpContext, IDataSourceWizardJsonConnectionStorage storageContext, IOperation operation)
        {
            _storage = storageContext;
            _env = env;
            _httpContext = httpContext;
            _operation = operation;
        }
        
        public override async Task<Action> BuildStartingAsync(string reportId, string reportUrl, XtraReport report, ReportBuildProperties buildProperties)
        {

            report.DataSource = await _operation.CreateDataSourceFromWebAsync(report,buildProperties,true);
            return await base.BuildStartingAsync(reportId, reportUrl, report, buildProperties);
        }



    }
    public class WebDocumentViewerJsonDataConnectionProvider : IJsonDataConnectionProviderService
    {
        readonly Dictionary<string, string> jsonDataConnections;
        private readonly IOperation _operation;
        public WebDocumentViewerJsonDataConnectionProvider(Dictionary<string, string> jsonDataConnections, IOperation operation)
        {
            this.jsonDataConnections = jsonDataConnections;
            _operation = operation;
        }


        public JsonDataConnection GetJsonDataConnection(string name)
        {
            if (jsonDataConnections == null)
                return null;

            if (!jsonDataConnections.ContainsKey(name))
            {
                Dictionary<string, string> dicConnection = new Dictionary<string, string>();


                List<DataSourceConnection> dsConnections = _operation.GetDataConnections();

                dicConnection = dsConnections.Where(w => w.Name.ToLower()==name.ToLower())
                                             .ToDictionary(x => x.Name, d => (new JsonDataConnection(new UriJsonSource
                                             {
                                                 Uri = new Uri(d.Uri)
                                             })
                                             {
                                                 Name = d.Name,
                                                 StoreConnectionNameOnly = true

                                             }).CreateConnectionString());
                return Services.CustomJsonConnection.CreateJsonDataConnectionFromString(name, dicConnection[name]);
            }
            return Services.CustomJsonConnection.CreateJsonDataConnectionFromString(name, jsonDataConnections[name]);
        }

   
    }

    public class CustomReportProviderAsync : IReportProviderAsync
    {
        readonly ReportStorageWebExtension reportStorageWebExtension;

        public CustomReportProviderAsync(ReportStorageWebExtension reportStorageWebExtension)
        {
            this.reportStorageWebExtension = reportStorageWebExtension;
        }
        public async Task<XtraReport> GetReportAsync(string id, ReportProviderContext context)
        {
            var reportLayout = await reportStorageWebExtension.GetDataAsync(id);
            if (reportLayout == null)
                return null;
            using (var ms = new MemoryStream(reportLayout))
            {
                var report = XtraReport.FromXmlStream(ms);
                return report;
            }
        }
    }

 
}


