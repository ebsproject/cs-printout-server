﻿using cs_printoutserver.Models;
using DevExpress.DataAccess.Json;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.WebDocumentViewer;
using GraphQL.Client.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace cs_printoutserver.Services.Interfaces
{
    public interface IOperation
    {
        
        public string TransponseDatatable(DataTable inputDT, DataSourceConnection ds);
        public string GetContentType(string path);
        /// <summary>
        /// Upload an internal document into PS
        /// </summary>
        /// <param name="file"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        public Task<bool> UploadDocumentAsync(IFormFile file, string program);
        /// <summary>
        /// Download an internal document from PS
        /// </summary>
        /// <param name="file_name"></param>
        /// <param name="directory"></param>
        /// <returns></returns>
        public FileStreamResult DownloadDocument(string file_name, string directory = "Files");

        /// <summary>
        /// Delete a file from internat file storage
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public Task<bool> DeleteFileAsync(string path);
        /// <summary>
        /// Retrieve the Report Metadata
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XtraReport XtraReportInfo(string name);

        /// <summary>
        /// Get the Json Connection from connections list json file
        /// </summary>
        /// <param name="reportName"></param>
        /// <param name="isSystem"></param>
        /// <returns></returns>
        public DataSourceConnection GetJsonDataConnection(string reportName, bool isSystem = false);
      
        /// <summary>
        /// Serialize json file to Data Source Class
        /// </summary>
        /// <param name="isSystem"></param>
        /// <returns></returns>
        public List<DataSourceConnection> GetDataConnections(Boolean isSystem = false);


        /// <summary>
        /// Retrieve information from file
        /// </summary>
        /// <returns></returns>
        public Task<JsonDataSource> CreateDataSourceFromFile(string fileName);
        
        /// <summary>
        /// Prepare the data to be displayed by UI or APIs
        /// </summary>
        /// <param name="report"></param>
        /// <param name="reportBuildProperties"></param>
        /// <param name="isRequestFromReportSubmit"></param>
        /// <param name="isSystem"></param>
        /// <returns></returns>
        public Task<JsonDataSource> CreateDataSourceFromWebAsync(XtraReport report,
                                                            ReportBuildProperties reportBuildProperties,
                                                            bool isRequestFromReportSubmit = false, Boolean isSystem = false);



        /// <summary>
        /// Retrieve token from context or generate one
        /// </summary>
        /// <returns></returns>
        public Task<string> GetTokenAsync();
        /// <summary>
        /// Retrieve information from Tenant
        /// </summary>
        /// <returns></returns>
        public brapi QueryResultSystem();
       /// <summary>
       /// Retrieve Result from Template
       /// </summary>
       /// <returns></returns>
        public brapi QueryResultTemplates();
        
        /// <summary>
        /// Get parameter list from report
        /// </summary>
        /// <param name="nameReport"></param>
        /// <returns></returns>
        public List<QueryParameterConnection> RetrieveParametersFromReport(string nameReport);
        
        /// <summary>
        /// Retrieve data base on program
        /// </summary>
        /// <param name="program"></param>
        /// <returns></returns>
        public brapi QueryResult(string program);
       
        /// <summary>
        /// Download the template list as zipfile
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="zipFileName"></param>
        /// <param name="printoutTemplate"></param>
        /// <returns></returns>
        public FileStreamResult DownloadZipFile(string dir, string zipFileName, List<PrintOutTemplate> printoutTemplate);

        /// <summary>
        /// Invoke the Upload Zip file to import templates
        /// </summary>
        /// <param name="file"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        public Task<bool> UploadZipFileAsync(IFormFile file, string dir);
        /// <summary>
        /// Unpack template zip file imported
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool UnpackFilesFromZIP(string dir, string path);
     
        /// <summary>
        /// Delete Temporary files
        /// </summary>
        /// <param name="_contentRootPath"></param>
        /// <param name="fileName"></param>
        public void DeleteTempFiles(string _contentRootPath = "", string fileName = "");
        
        /// <summary>
        /// Retrieve Context Information
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="domains"></param>
        /// <returns></returns>
        public string GetSgContext(DataSourceConnection ds, dynamic domains);

        /// <summary>
        /// Initial point to process an api request
        /// </summary>
        /// <param name="dsConnection"></param>
 
        /// <param name="isRequestFromReportSubmit"></param>
        /// <param name="isSystem"></param>
        /// <returns></returns>
        public  Task<brapi> ResultFromApiSourcesAsync(DataSourceConnection dsConnection,
                                                  bool isRequestFromReportSubmit = true, Boolean isSystem = false);

       /// <summary>
       /// Allow change the api
       /// </summary>
       /// <param name="template"></param>
       /// <param name="contentRootPath"></param>
       /// <param name="newName"></param>
        public void ChangeConnection(List<TemplateListConnection> template, string contentRootPath, string newName);


    }
}
