﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevExpress.DataAccess.Json;
using DevExpress.DataAccess.Web;
using DevExpress.DataAccess.Wizard.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using cs_printoutserver.Models;
using cs_printoutserver.Services.Interfaces;
using System.Dynamic;
using DevExpress.DataAccess.ConnectionParameters;

namespace cs_printoutserver.Services
{

 
    public class CustomJsonConnection : IDataSourceWizardJsonConnectionStorage
    {

        public const string JsonDataConnectionsKey = "dxJsonDataConnections";
        protected IWebHostEnvironment _environment { get; }
        protected IHttpContextAccessor _httpContextAccessor { get; }
        protected IOperation _operation { get; }
        public CustomJsonConnection(IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor, IOperation operation)
        {
            _environment = env;
            _httpContextAccessor = httpContextAccessor;
            _operation = operation;
        }

        public Dictionary<string, string> GetConnections()
        {

            Dictionary<string, string> dicConnection = new Dictionary<string, string>();

         
            List<DataSourceConnection> dsConnections = _operation.GetDataConnections();

            dicConnection = dsConnections.Where(w => !w.IsDefault)
                                         .ToDictionary(x => x.Name, d => (new JsonDataConnection(new UriJsonSource
                                         {
                                             Uri = new Uri(d.Uri)
                                         })
                                         {
                                             Name = d.Name,
                                             StoreConnectionNameOnly = true

                                         }).CreateConnectionString());
      
            return dicConnection;
        }

        bool IJsonConnectionStorageService.CanSaveConnection { get { return _httpContextAccessor.HttpContext != null; } }
        bool IJsonConnectionStorageService.ContainsConnection(string connectionName)
        {
            var connections = GetConnections();
            return connections == null ? false : connections.ContainsKey(connectionName);
        }

        IEnumerable<JsonDataConnection> IJsonConnectionStorageService.GetConnections()
        {
            Dictionary<string, string> connections = GetConnections();
            if (connections == null)
            {
                return new List<JsonDataConnection>();
            }
            return connections.Select(x => CreateJsonDataConnectionFromString(x.Key, x.Value));

        }

        JsonDataConnection IJsonDataConnectionProviderService.GetJsonDataConnection(string name)
        {
            var connections = GetConnections();
            if (connections == null || !connections.ContainsKey(name))
                throw new InvalidOperationException();
            return CreateJsonDataConnectionFromString(name, connections[name]);

      
        
        }

        void IJsonConnectionStorageService.SaveConnection(string connectionName, JsonDataConnection dataConnection, bool saveCredentials)
        {
            var connections = GetConnections();
            if (connections == null)           
                return;
           
            var connectionString = dataConnection.CreateConnectionString();
            if (connections.ContainsKey(connectionName))
            {
                connections[connectionName] = connectionString;
                ManageJsonDataConnection(connectionName, dataConnection);
            }
            else
            {
                connections.Add(connectionName, connectionString);
                ManageJsonDataConnection(connectionName,dataConnection);
            }              
         
        }

        
        
      
        public  void  ManageJsonDataConnection(string connectionName, JsonDataConnection dataConnection)
        {
            UriJsonSource cnn = dataConnection.GetJsonSource() as UriJsonSource;


            DataSourceConnection dsConnection;
            List<DataSourceConnection> dsConnections = _operation.GetDataConnections();
            dsConnection = dsConnections.Where(x => x.Name.Trim().ToLower() == connectionName.Trim().ToLower()).FirstOrDefault();



            //exist the connection to modify it
            if (dsConnection!=null)
            {
               dsConnection.Uri = cnn!.Uri.ToString();
            }
            else
            {
                dsConnection = new DataSourceConnection();
                //create a new object connection
              
                dsConnection.Name = connectionName;
                dsConnection.Uri = cnn.Uri.ToString();
                dsConnection.HttpMethod = RestSharp.Method.GET;
                dsConnection.IsDefault = false;
                dsConnection.TemplateList = new List<TemplateListConnection>();
                //array pathParamethers (think to move as a method

                dsConnection.PathParameters = cnn.PathParameters.Select(s => new PathParameterConnection { 
                    Name= s.Name,
                    Type = s.Type.Name,
                    Value = s.Value.ToString()!,
                    DefaultValue = s.Value.ToString()!
                }).ToList<PathParameterConnection>();

                dsConnection.QueryParameters = cnn.QueryParameters.Select(s => new QueryParameterConnection
                {
                    Name = s.Name,
                    Type = s.Type.Name,
                    Value = s.Value.ToString()!,
                    DefaultValue = s.Value.ToString()!
                }).ToList<QueryParameterConnection>();               
                dsConnections.Add(dsConnection);
            }

            dynamic jsonConnection = new ExpandoObject();
            jsonConnection.JsonConnections = dsConnections;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonConnection, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(Path.Combine(_environment.ContentRootPath, "Reports","Connections","connection.json"), output);

 
        }

        public static JsonDataConnection CreateJsonDataConnectionFromString(string connectionName, string connectionString)
        {
            return new JsonDataConnection(connectionString) { StoreConnectionNameOnly = true, Name = connectionName };
        }
    }
}
