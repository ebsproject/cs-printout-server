﻿using System;

namespace cs_printoutserver.Services
{
    public class LoggerService : DevExpress.XtraReports.Web.ClientControls.LoggerService
    {
        public override void Info(string message)
        {
            System.Diagnostics.Debug.WriteLine("[{0}]: Info: '{1}'.", DateTime.Now, message);
        }
    
        public override void Error(Exception exception, string message)
        {
            System.Diagnostics.Debug.WriteLine("[{0}]: Exception occured. Message: '{1}'. Exception Details:\r\n{2}",
           DateTime.Now, message, exception);
        }
    }
}
