﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.ClientControls;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace cs_printoutserver.Services
{

    public class ReportStorage : DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension
    {
        readonly string ReportDirectory;
        readonly string ReportDirectorySystemTemplates;
        const string FileExtension = ".repx";
        public ReportStorage(IWebHostEnvironment _env, IHttpContextAccessor _httpContext)
        {
            ReportDirectory = Path.Combine(_env.ContentRootPath, "Reports");
            if (!Directory.Exists(ReportDirectory))
            {
                Directory.CreateDirectory(ReportDirectory);
            }
            ReportDirectorySystemTemplates = Path.Combine(_env.ContentRootPath,"SystemTemplates");
            
        }

      

        public override async Task<byte[]> GetDataAsync(string url)
        {
            // Returns report layout data stored in a Report Storage using the specified URL. 
            // This method is called only for valid URLs after the IsValidUrl method is called.
            string[] urlSplitted = url.Split('?');
            url = urlSplitted[0];
            string reportDirectory = url.Contains("sys_") ? ReportDirectorySystemTemplates : ReportDirectory;
            try
            {
                if (Directory.EnumerateFiles(reportDirectory).Select(Path.GetFileNameWithoutExtension).Contains(url))
                {
                    string fileName = String.Empty;
                    fileName = Path.Combine(reportDirectory, url + FileExtension);
                    XtraReport xtraReport = XtraReport.FromFile(fileName);
                    if (urlSplitted.Length > 1)
                    {
                        string paramName = urlSplitted[1];
                        string paramValue = urlSplitted[2];
                        foreach (var item in xtraReport.Parameters)
                        {
                            if (item.Name == paramName)
                            {
                                item.Value = paramValue;
                            }
                        }
                        xtraReport.RequestParameters = false;
                        xtraReport.SaveLayoutToXml(fileName);

                    }
                    else
                    {
                        xtraReport.RequestParameters = true;
                        xtraReport.SaveLayoutToXml(fileName);
                    }
                    return await File.ReadAllBytesAsync(Path.Combine(reportDirectory, url + FileExtension));
                }

                throw new DevExpress.XtraReports.Web.ClientControls.FaultException(string.Format("Could not find report '{0}'.", url));
            }
            catch (Exception)
            {
                throw new DevExpress.XtraReports.Web.ClientControls.FaultException(string.Format("Could not find report '{0}'.", url));
            }
        }



        private bool IsWithinReportsFolder(string url, string folder)
        {
            var rootDirectory = new DirectoryInfo(folder);
            var fileInfo = new FileInfo(Path.Combine(folder, url));
            return fileInfo.Directory.FullName.ToLower().StartsWith(rootDirectory.FullName.ToLower());
        }
        public override Task SetDataAsync(XtraReport report, string url)
        {
            // Stores the specified report to a Report Storage using the specified URL. 
            // This method is called only after the IsValidUrl and CanSetData methods are called.
            string reportDirectory = url.Contains("sys_") ? ReportDirectorySystemTemplates : ReportDirectory;

            if (!IsWithinReportsFolder(url, reportDirectory))
                throw new DevExpress.XtraReports.Web.ClientControls.FaultException("Invalid report name.");

            report.SaveLayoutToXml(Path.Combine(reportDirectory, url + FileExtension));
            return Task.CompletedTask;
        }

        public override async Task<string> SetNewDataAsync(XtraReport report, string defaultUrl)
        {
            // Stores the specified report using a new URL. 
            // The IsValidUrl and CanSetData methods are never called before this method. 
            // You can validate and correct the specified URL directly in the SetNewData method implementation 
            // and return the resulting URL used to save a report in your storage.
            await SetDataAsync(report, defaultUrl);
            return defaultUrl;
        }

        public override bool CanSetData(string url)
        {
            // Determines whether or not it is possible to store a report by a given URL. 
            // For instance, make the CanSetData method return false for reports that should be read-only in your storage. 
            // This method is called only for valid URLs (i.e., if the IsValidUrl method returned true) before the SetData method is called.
            return true;
        }
        public override bool IsValidUrl(string url)
        {
            // Determines whether or not the URL passed to the current Report Storage is valid. 
            // For instance, implement your own logic to prohibit URLs that contain white spaces or some other special characters. 
            // This method is called before the CanSetData and GetData methods.

            return Path.GetFileName(url) == url;
        }
        public override Task<Dictionary<string, string>> GetUrlsAsync()
        {

            // Returns a dictionary of the existing report URLs and display names. 
            // This method is called when running the Report Designer, 
            // before the Open Report and Save Report dialogs are shown and after a new report is saved to a storage.
            Dictionary<string, string> urls = new Dictionary<string, string>();
            var files = Directory.EnumerateFiles(ReportDirectory).Select(Path.GetFileNameWithoutExtension);
            foreach (var file in files)            
                urls.Add(file!, file!);
            
            return Task.FromResult(urls);

        }


        //public override void SetData(XtraReport report, string url)
        //{
        //    throw new FaultException("SetData was called");
        //}

        //public override string SetNewData(XtraReport report, string defaultUrl)
        //{
        //    throw new FaultException("SetNewData was called");
        //}
        //public override byte[] GetData(string url)
        //{
        //    throw new FaultException("GetData was called");

        //}
        //public override Dictionary<string, string> GetUrls()
        //{
        //    throw new FaultException("GetUrls was called");
        //}




    }
}
