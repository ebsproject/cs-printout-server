﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using cs_printoutserver.Models;

namespace cs_printoutserver.Binders
{
    public class CoreSystemBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(PrintOutTemplate))
            {


                return new BinderTypeModelBinder(typeof(CoreSystemBinder));
            }

            return null;
        }
    }
}
