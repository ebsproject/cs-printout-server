﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using cs_printoutserver.Models;

namespace cs_printoutserver.Binders
{
    public class CoreSystemBinder : IModelBinder
    {

        private readonly CoreSystemContext _context;
        public CoreSystemBinder(CoreSystemContext context)
        {
            _context = context;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {

            if(bindingContext == null)
            {
                throw new NotImplementedException();
            }

      
            List<PrintOutTemplate> printOutTemplates = new List<PrintOutTemplate>
            {
                new PrintOutTemplate
                {
                    Id=1,
                    Description="Dummy"
                }
            };
            bindingContext.Result = ModelBindingResult.Success(printOutTemplates);


            return Task.CompletedTask;
            
        }
    }
}
