﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Introduction to Banded Reports </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Introduction to Banded Reports ">
    <meta name="generator" content="docfx 2.58.4.0">
    
    <link rel="shortcut icon" href="../../favicon.ico">
    <link rel="stylesheet" href="../../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../../styles/docfx.css">
    <link rel="stylesheet" href="../../styles/main.css">
    <meta property="docfx:navrel" content="../../toc.html">
    <meta property="docfx:tocrel" content="../toc.html">
    
    
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../../index.html">
                <img id="logo" class="svg" src="../../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="introduction-to-banded-reports">Introduction to Banded Reports</h1>

<p>Banded reports provide a generalized report layout notion. When you preview a banded report, a report document is generated based on the report layout and data source.</p>
<h2 id="report-bands">Report Bands</h2>
<p>A report layout consists of bands that contain report controls and define their location on document pages. A blank report contains the following bands:</p>
<ul>
<li><p>The <strong>Detail Band</strong> displays recurring contents from the report's data source. This band is printed as many times as there are records available in a data source unless you filtered the data.</p>
<p>Every report must have a detail band, and you cannot delete it.</p>
</li>
<li><p>The top and bottom page <strong>Margin bands</strong>. These bands are repeated once on every document page.</p>
</li>
</ul>
<p><img src="../../images/eurd-web-report-bands.png" alt=""></p>
<p>You can also add the following bands:</p>
<ul>
<li><p><strong>Report Header</strong> and <strong>Report Footer</strong></p>
<p>The <strong>Report Header</strong> is the report's first band (margins are &quot;out-of-page&quot; zones). Use this band to display the report's name, company logo, <a href="add-extra-information.html">date of creation, username</a>, etc.</p>
<p>The <strong>Report Footer</strong> is placed before the Page Footer and Bottom Margin on the report's last page. You can use the Report Footer band for report <a href="shape-report-data%5Ccalculate-summaries%5Ccalculate-a-summary.md">summaries</a> or conclusions.</p>
</li>
<li><p><strong>Page Header</strong> and <strong>Page Footer</strong></p>
<p>These bands are at the top and bottom of every page in a report. They display information that should be printed on every page.</p>
</li>
<li><p><strong>Group Header</strong> and <strong>Group Footer</strong></p>
<p>These bands are above and below each <a href="shape-report-data%5Cgroup-and-sort-data.md">group</a>.</p>
</li>
</ul>
<div class="TIP">
<h5>Tip</h5>
<p>Only the detail and group bands can be used to display dynamic data source contents. Other bands display titles, summaries, and <a href="add-extra-information.html">extra information</a>.</p>
</div>
<p>The following image illustrates a sample report layout and the <a href="report-designer-tools%5Cui-panels%5Creport-explorer.md">Report Explorer</a> panel that reflects the report's structure:</p>
<p><img src="../../images/eurd-web-report-bands-basic-layout.png" alt=""></p>
<h2 id="add-a-detail-band-to-a-master-detail-band">Add a Detail Band to a Master-Detail Band</h2>
<p>Use the <strong>detail report band</strong> to create hierarchical <a href="create-reports/master-detail-reports-with-detail-report-bands.html">master-detail reports</a>. Detail report bands provide detailed information about each record in the master report's detail band. You can create such reports when master-detail relationships are defined between data source tables:</p>
<p><img src="../../images/eurd-web-report-bands-master-detail-relation.png" alt=""></p>
<p>A detail report band is a separate report (subreport) with its own data source and different bands. A report can have any number of detail reports that can also be nested.</p>
<p>The following image illustrates a master-detail report and the <a href="report-designer-tools%5Cui-panels%5Creport-explorer.md">Report Explorer</a> panel that reflects the report's structure:</p>
<p><img src="../../images/eurd-web-report-bands-subreports.png" alt=""></p>
<h2 id="vertical-bands">Vertical Bands</h2>
<p>You can replace the Detail band with the <strong>Vertical Header</strong>, <strong>Vertical Detail</strong> and <strong>Vertical Total</strong> bands to display record fields vertically and print data records horizontally - from left to right.</p>
<p><img src="../../images/eurd-web-report-vertical-bands-result.png" alt=""></p>
<div class="NOTE">
<h5>Note</h5>
<p>If your report's Detail band contains report controls, this band and all these controls are lost when you add a vertical band (the same behavior takes place in the opposite situation).</p>
</div>
<p>The following vertical bands are available:</p>
<ul>
<li><p><strong>Vertical Header</strong></p>
<p>Contains headers of the report's data fields. These headers are arranged one after another in a vertical direction.</p>
</li>
<li><p><strong>Vertical Details</strong></p>
<p>Displays recurring contents from the report's data source. This band is printed as many times as there are available records in a data source, unless you filtered the data. The records are displayed one after another in a horizontal direction.</p>
</li>
<li><p><strong>Vertical Total</strong></p>
<p>This band is placed at the rightmost position (leftmost when RTL is enabled). You can use the Vertical Total band for report summaries or conclusions.</p>
</li>
</ul>
<p><img src="../../images/eurd-web-report-vertical-bands-design-surface.png" alt=""></p>
<div class="TIP">
<h5>Tip</h5>
<p>See <a href="create-reports%5Cvertical-reports.md">Vertical Reports</a> for details on how to use vertical bands.</p>
</div>
<h2 id="create-band-copies">Create Band Copies</h2>
<p>You can create functional copies of a band, for example, to display different contents based on a specific condition. To do this, add <strong>sub-bands</strong> to bands.</p>
<p><img src="../../images/eurd-web-report-bands-basic-layout-sub-bands.png" alt=""></p>
<div class="TIP">
<h5>Tip</h5>
<p>See <a href="shape-report-data%5Clay-out-dynamic-report-content.md">Laying out Dynamic Report Contents</a> for details on how to specify the location of bands' content on document pages.</p>
</div>
<h2 id="managingreportbands">Manage Report Bands</h2>
<h3 id="hide-bands-in-the-report-designer">Hide Bands in the Report Designer</h3>
<p>Select a band and click on the band's tab title to collapse or expand the band.</p>
<p><img src="../../images/eurd-web-report-bands-collapsing.png" alt=""></p>
<h3 id="hide-bands-in-the-report-document">Hide Bands in the Report Document</h3>
<p>You can avoid printing a band's content in a document. To do this, set the band's <strong>Visible</strong> property to <strong>false</strong>. Select the band and set this property in the <a href="report-designer-tools/ui-panels/properties-panel.html">Properties Panel</a>.</p>
<p><img src="../../images/eurd-web-report-bands-hiding.png" alt=""></p>
<h3 id="remove-bands">Remove Bands</h3>
<p>Select a band on the report design surface and press DELETE. This removes the band and all its content.</p>
<h3 id="add-bands">Add Bands</h3>
<p>To add a band, select the report or any of its bands in the <strong>Properties</strong> panel and click an appropriate item in the <strong>Actions</strong> category.</p>
<p><img src="../../images/eurd-web-report-bands-add-bands.png" alt=""></p>
<p>You can insert a detail report band if the report's data source has <a href="create-reports/master-detail-reports-with-detail-report-bands.html">master-detail relations</a>. Click <strong>Insert Detail Report</strong> in the <strong>Actions</strong> category.</p>
<p><img src="../../images/eurd-web-report-bands-add-detail-band.png" alt=""></p>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                  <li>
                    <a href="https://bitbucket.org/ebsproject/cs-printout-server/src/develop/Help/articles/report-designer/introduction-to-banded-reports.md#lines-1" class="contribution-link">Improve this Doc</a>
                  </li>
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            
            <span>EBS Copyright 2021</span>
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../../styles/docfx.js"></script>
    <script type="text/javascript" src="../../styles/main.js"></script>
  </body>
</html>
