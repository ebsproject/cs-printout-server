﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Appearance Properties </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Appearance Properties ">
    <meta name="generator" content="docfx 2.58.4.0">
    
    <link rel="shortcut icon" href="../../../favicon.ico">
    <link rel="stylesheet" href="../../../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../../../styles/docfx.css">
    <link rel="stylesheet" href="../../../styles/main.css">
    <meta property="docfx:navrel" content="../../../toc.html">
    <meta property="docfx:tocrel" content="../../toc.html">
    
    
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../../../index.html">
                <img id="logo" class="svg" src="../../../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="appearance-properties">Appearance Properties</h1>

<p>This document describes the purpose and implementation of the appearance properties - a special set of properties that allow you to customize the appearance of a report or any of its elements.</p>
<h2 id="properties-overview">Properties Overview</h2>
<p>Every report element (<a href="..%5Cuse-report-elements.md">control</a> or <a href="..%5Cintroduction-to-banded-reports.md">band</a>), and a report itself, has a set of properties that specify its appearance. They are listed in the following table.</p>
<table>
<thead>
<tr>
<th>Property name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>BackgroundColor</strong></td>
<td>Gets or sets a background color to a report element and its child controls.</td>
</tr>
<tr>
<td><strong>BorderColor</strong></td>
<td>Gets or sets a border color to a report element and its child controls.</td>
</tr>
<tr>
<td><strong>BorderDashStyle</strong></td>
<td>Gets or sets a border dash style to a report element and its child controls.</td>
</tr>
<tr>
<td><strong>Borders</strong></td>
<td>Gets or sets borders (top, right, bottom,left), which should be visible for a report element and its child controls.</td>
</tr>
<tr>
<td><strong>BorderWidth</strong></td>
<td>Gets or sets a border width to a report element and its child controls.</td>
</tr>
<tr>
<td><strong>Font</strong></td>
<td>Gets or sets the font options (its name, size, etc.) to a report element and its child controls.</td>
</tr>
<tr>
<td><strong>ForegroundColor</strong></td>
<td>Gets or sets the foreground color to a report element and its child controls.</td>
</tr>
<tr>
<td><strong>Padding</strong></td>
<td>Gets or sets the indent values which are used to render the contents of a report element and its child controls.</td>
</tr>
<tr>
<td><strong>TextAlignment</strong></td>
<td>Gets or sets the text alignment to a report element and its child controls.</td>
</tr>
</tbody>
</table>
<h2 id="access-appearance-properties">Access Appearance Properties</h2>
<p>Use the Report Designer's <a href="..%5Creport-designer-tools%5Cui-panels%5Cproperties-panel.md">Properties</a> panel to access the appearance properties.</p>
<p><img src="../../../images/eurd-web-appearance-properties-in-properties-panel.png" alt=""></p>
<h2 id="property-value-inheritance">Property Value Inheritance</h2>
<p>By default, appearance properties for every control or a band are set to empty values, which means that their real values are obtained from a control's parent, or a parent of its parent and so on.</p>
<p><img src="../../../images/eurd-web-appearance-properties.png" alt=""></p>
<div class="NOTE">
<h5>Note</h5>
<p>The appearance properties may not be used by all descendants of the current report element for which they are defined. For example, the <strong>PageBreak</strong> element ignores the <strong>BackColor</strong> property.</p>
</div>
<p>To reset values of these properties, click the <img src="../../../images/eurd-web-reset-property-value.png" alt=""> button to the right of the editor, and in the invoked popup menu, select <strong>Reset</strong>. Then, the control's actual appearance will be determined by the appropriate properties settings of its parent.</p>
<p><img src="../../../images/eurd-web-appearance-properties-reset.png" alt=""></p>
<p>If a report element has a <a href="report-visual-styles.html">style</a> assigned to it, the priority of the properties defined by this style is determined by the <strong>StylePriority</strong> property. Note that when a <a href="..%5Cshape-report-data%5Cspecify-conditions-for-report-elements%5Cconditionally-change-a-control-appearance.md">conditional formatting</a> is involved, the appearance it defines is of greater priority than the properties described above.</p>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                  <li>
                    <a href="https://bitbucket.org/ebsproject/cs-printout-server/src/develop/Help/articles/report-designer/customize-appearance/appearance-properties.md#lines-1" class="contribution-link">Improve this Doc</a>
                  </li>
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            
            <span>EBS Copyright 2021</span>
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../../../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../../../styles/docfx.js"></script>
    <script type="text/javascript" src="../../../styles/main.js"></script>
  </body>
</html>
