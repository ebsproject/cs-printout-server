﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Expression Editor </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Expression Editor ">
    <meta name="generator" content="docfx 2.58.4.0">
    
    <link rel="shortcut icon" href="../../../favicon.ico">
    <link rel="stylesheet" href="../../../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../../../styles/docfx.css">
    <link rel="stylesheet" href="../../../styles/main.css">
    <meta property="docfx:navrel" content="../../../toc.html">
    <meta property="docfx:tocrel" content="../../toc.html">
    
    
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../../../index.html">
                <img id="logo" class="svg" src="../../../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="expression-editor">Expression Editor</h1>

<p>This document describes how to use the <strong>Expression Editor</strong> to specify expressions in the <a href="../../report-designer.html">Report Designer</a>.</p>
<p>Invoke the <strong>Expression Editor</strong> from a property's popup menu in the <a href="ui-panels/properties-panel.html">Properties Panel</a>. Click the property's marker and select the <strong><em>PropertyName</em> Expression</strong>.</p>
<p><img src="../../../images/eurd-web-report-designer-property-popup-menu.png" alt=""></p>
<p>If a property's editor displays an ellipsis button, you can click this button to invoke the <strong>Expression Editor</strong> and specify an expression that evaluates to the property's value.</p>
<p><img src="../../../images/eurd-web-report-designer-expressions-tab.png" alt=""></p>
<p>The <strong>Expression Editor</strong> offers a choice of functions, operators, data source fields, report elements, constants, and variables to create an expression.</p>
<p><img src="../../../images/eurd-web-expression-editor-construct-expression.png" alt=""></p>
<p>An expression can span multiple lines.</p>
<p><img src="../../../images/eurd-web-report-designer-expression-multiple-lines.png" alt=""></p>
<p>You can add single-line or multi-line comments in the following format: <code>/* comment text */</code>.</p>
<p><img src="../../../images/eurd-web-expression-editor-comments.png" alt=""></p>
<p>The <strong>Expression Editor</strong> highlights an expression's syntax and supports intelligent code completion (it suggests functions and available data elements as you type).</p>
<p><img src="../../../images/eurd-web-report-designer-expression-editor-code-completion.png" alt=""></p>
<p>An error icon appears if an expression contains errors. Hover the mouse pointer over this icon to invoke a pop-up notification that shows the location of the error.</p>
<p><img src="../../../images/eurd-web-expression-editor-error.png" alt=""></p>
<p>See the <a href="../use-expressions/expression-syntax.html">Expression Syntax</a> topic for the expression syntax description.</p>
<h2 id="expression-syntax">Expression Syntax</h2>
<p>Take into account the following syntax conventions when using the Expression Editor:</p>
<ul>
<li><p>Reference a data field in the expression by enclosing its name in the square brackets (for example, <strong>[ProductName]</strong>).</p>
</li>
<li><p>Insert <a href="../shape-report-data/use-report-parameters.html">report parameters</a> and <a href="../bind-to-data/specify-query-parameters.html">query parameters</a> by typing a question mark before their names (for instance, <strong>?parameter1</strong>).</p>
</li>
<li><p>Denote string values with apostrophes. Type a double apostrophe to embed an apostrophe into an expression's text (for example, <strong>'It''s sample text'</strong>).</p>
</li>
<li><p>Enclose date-time constants with hashtags (<strong>[OrderDate] &gt;= #1/1/2016#</strong>).</p>
</li>
<li><p>Use a question mark to specify a null reference (one that does not refer to any object) (<strong>[Region] != ?</strong>).</p>
</li>
<li><p>If an expression involves the use of different types, you can convert them to the same type using dedicated functions (for instance, <strong>Max(ToDecimal([Quantity]),[UnitPrice])</strong>).</p>
</li>
</ul>
<h2 id="using-the-expression-editor">Using the Expression Editor</h2>
<p>When <a href="../use-report-elements/bind-controls-to-data.html">expression bindings</a> are enabled in your reports, the Report Designer contains the <a href="ui-panels/expressions-panel.html">Expressions</a> tab allowing you to assign values to various element properties. Clicking any property's ellipsis button invokes the Expression Editor, in which you can specify custom expressions with the available data fields.</p>
<p><img src="../../../images/eurd-web-expression-editor-expressions-tab.png" alt=""></p>
<p>In the <a href="../use-report-elements/bind-controls-to-data.html">data binding</a> mode, you can use the Expression Editor in the following cases:</p>
<ul>
<li><p><strong>Edit a Calculated Field's Expression</strong></p>
<p>Access a <a href="../shape-report-data/use-calculated-fields/calculated-fields-overview.html">calculated field</a>'s settings in the Field List and click <strong>Expression</strong> property's ellipsis button.</p>
<p><img src="../../../images/eurd-web-expression-editor-calculated-field.png" alt=""></p>
</li>
<li><p><strong>Specify a Query Parameter's Value</strong></p>
<p>In the <a href="data-source-wizard%5Cspecify-data-source-settings-database.md">Configure Query Parameters</a> wizard page, set the parameter type to <strong>Expression</strong> and click the <strong>Value</strong> property's the ellipsis button.</p>
<p><img src="../../../images/eurd-web-sql-ds-wizard-configure-query-parameters-expression-editor.png" alt=""></p>
</li>
<li><p><strong>Construct a Formatting Rule's Condition</strong></p>
<p>Access the <a href="../shape-report-data/specify-conditions-for-report-elements/conditionally-change-a-control-appearance.html">formatting rule</a>'s settings in the <a href="ui-panels/properties-panel.html">Properties</a> panel and click the <strong>Condition</strong> property's ellipsis button.</p>
<p><img src="../../../images/eurd-web-shaping-formattin-rule-appearance-condition.png" alt=""></p>
</li>
</ul>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                  <li>
                    <a href="https://bitbucket.org/ebsproject/cs-printout-server/src/develop/Help/articles/report-designer/report-designer-tools/expression-editor.md#lines-1" class="contribution-link">Improve this Doc</a>
                  </li>
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            
            <span>EBS Copyright 2021</span>
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../../../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../../../styles/docfx.js"></script>
    <script type="text/javascript" src="../../../styles/main.js"></script>
  </body>
</html>
