﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Query Builder </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Query Builder ">
    <meta name="generator" content="docfx 2.58.4.0">
    
    <link rel="shortcut icon" href="../../../favicon.ico">
    <link rel="stylesheet" href="../../../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../../../styles/docfx.css">
    <link rel="stylesheet" href="../../../styles/main.css">
    <meta property="docfx:navrel" content="../../../toc.html">
    <meta property="docfx:tocrel" content="../../toc.html">
    
    
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../../../index.html">
                <img id="logo" class="svg" src="../../../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="query-builder">Query Builder</h1>

<p>The <strong>Query Builder</strong> is a visual queries editor. You can invoke it from the <a href="data-source-wizard.html">Data Source Wizard</a>.</p>
<h2 id="select">Select Tables</h2>
<p>Drag and drop a specific table or view onto the Query Builder design surface to include it into a query result set.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-select-table-drag-and-drop.png" alt=""></p>
<p>The Query Builder provides a toolbar with the following commands:</p>
<table>
<thead>
<tr>
<th>Icon</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><img src="../../../images/eurd-web-designer-query-builder-button-delete.png" alt="web-designer-query-builder-button-delete"></td>
<td>Removes the selected table or view from the query.</td>
</tr>
<tr>
<td><img src="../../../images/eurd-web-designer-query-builder-button-undo.png" alt="web-designer-query-builder-button-undo"></td>
<td>Reverses the most recent action.</td>
</tr>
<tr>
<td><img src="../../../images/eurd-web-designer-query-builder-button-redo.png" alt="web-designer-query-builder-button-redo"></td>
<td>Performs the previously undone action.</td>
</tr>
</tbody>
</table>
<p>Enable check boxes for the table columns you want to include into the query result set.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-select-fields.png" alt=""></p>
<p>Use the dedicated search box to find a specific table or view.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-search-tables.png" alt=""></p>
<h2 id="join">Join Tables</h2>
<p>The Query Builder allows you to join tables and/or views. Use drag and drop to connect corresponding columns (key fields). The connected columns should have identical data types.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-join-tables.png" alt=""></p>
<p>Click the data relation to display the <strong>Relation Properties</strong> section. Properties in this section define the join type (<strong>Inner</strong> or <strong>Left Outer</strong>) and applied logical operator.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-join-tables-selection-properties.png" alt=""></p>
<p>A left outer join returns all the values from an inner join along with all values in the &quot;left&quot; table that do not match to the &quot;right&quot; table and includes rows with NULL (empty) values in the key field.</p>
<p>If you select the left outer join, the relationship line displays an arrow which points at the &quot;right&quot; table of the join clause.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-join-tables-left-outer.png" alt=""></p>
<p>The executed query returns a &quot;flat&quot; table which joins different tables within a single query. The specified join options define which data records compose the query result set.</p>
<div class="NOTE">
<h5>Note</h5>
<p>We recommend you to use <a href="master-detail-relation-editor.html">hierarchical data sources</a> because the reporting engine generates master-detail reports faster than similar-looking reports which obtains data from &quot;flat&quot; data sources.</p>
</div>
<h2 id="filter">Filter Data</h2>
<p>Expand the <strong>Query Properties</strong> section to display the query options.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-selection-properties.png" alt=""></p>
<p>The query provides the following options:</p>
<ul>
<li><p><strong>Name</strong></p>
<p>Specifies a custom query name (alias).</p>
</li>
<li><p><strong>Filter</strong></p>
<p>Runs the <a href="filter-editor.html">Filter Editor</a> where you can specify filter conditions for the resulting data. Filter criteria may contain <a href="../bind-to-data/specify-query-parameters.html">query parameters</a>.</p>
</li>
<li><p><strong>Group Filter</strong></p>
<p>Runs the Filter Editor where you can specify filter conditions for grouped and aggregated data. This option is enabled only for grouped data.</p>
</li>
<li><p><strong>Select All (*)</strong></p>
<p>Specifies whether to include all columns from the selected tables and/or views to the query result set, regardless of their individual settings.</p>
<p>The default value is <strong>No</strong>.</p>
</li>
<li><p><strong>Select Top</strong></p>
<p>Specifies the number of first records to include to the query result set. The default value is <strong>0</strong> and indicates that the query result set contains all records that meet all other filter conditions.</p>
</li>
<li><p><strong>Offset</strong></p>
<p>Specifies the number of records to skip before the report engine retrieves data. This option is available only for sorted data.</p>
</li>
<li><p><strong>Select distinct</strong></p>
<p>Specifies whether to include only distinct values to the result set.</p>
<p>The default value is <strong>No</strong>.</p>
</li>
</ul>
<h2 id="shape">Shape Data</h2>
<p>Select a table or view and click a data column to display the data column options.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-shape-data.png" alt=""></p>
<p>The <strong>Column Properties</strong> section contains the following options:</p>
<ul>
<li><p><strong>Name</strong></p>
<p>Indicates the column name which the Query Builder obtains from the database.</p>
</li>
<li><p><strong>Type</strong></p>
<p>Indicates the column's data type.</p>
<p>The Query Builder provides information about the maximum string length for string columns.</p>
</li>
<li><p><strong>Alias</strong></p>
<p>Specifies a custom column name (alias).</p>
<p>Include a column into a query to enable this option.</p>
</li>
<li><p><strong>Output</strong></p>
<p>Specifies whether to include the column into the query result set.</p>
</li>
<li><p><strong>Sort Type</strong></p>
<p>Specifies whether to preserve the original data records' order within the column, or sort them (in an ascending or descending order).</p>
</li>
<li><p><strong>Sort Order</strong></p>
<p>Apply sorting to the data column's records to enable this option.</p>
<p>It defines the sorting priority for multiple columns (the less this number is, the higher the priority).</p>
<p>For example, set the sort order to <strong>1</strong> for the column <strong>A</strong> and set it to <strong>2</strong> for the column <strong>B</strong>. The Query Builder first sorts the query by column <strong>A</strong> and then by the column <strong>B</strong>.</p>
<p>All columns' sort order automatically updates when you change this setting for one column. It allows you to avoid conflict of priorities.</p>
</li>
<li><p><strong>Group By</strong></p>
<p>Specifies whether to group the query result set by this column.</p>
<div class="NOTE">
<h5>Note</h5>
<p>You should apply grouping and/or aggregation to each selected column.</p>
</div>
</li>
<li><p><strong>Aggregate</strong></p>
<p>Specifies whether to aggregate the column's data records.</p>
<p>You can use the following aggregate functions: <strong>Count</strong>, <strong>Max</strong>, <strong>Min</strong>, <strong>Avg</strong>, <strong>Sum</strong>, <strong>CountDistinct</strong>, <strong>AvgDistinct</strong>, <strong>SumDistinct</strong>.</p>
<p>The Query Builder discards individual data records from the query result set and keep only the aggregate function result when you apply any of these functions.</p>
<div class="NOTE">
<h5>Note</h5>
<p>Use aggregation/grouping either for all selected columns or for none of them. The Query Builder applies grouping to all selected columns automatically if you apply the aggregation to one column. The Query Builder resets grouping against other columns when you remove all aggregation functions.</p>
</div>
</li>
</ul>
<h2 id="parameters">Use Query Parameters</h2>
<p>Use the <strong>Parameters</strong> section to add, remove and edit <a href="../bind-to-data/specify-query-parameters.html">query parameters</a>.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-parameters.png" alt=""></p>
<p>Each query parameter provides the following properties:</p>
<ul>
<li><p><strong>Name</strong></p>
<p>Specifies the query parameter's name.</p>
</li>
<li><p><strong>Type</strong></p>
<p>Specifies the parameter value's data type.</p>
<p>Set this property to <strong>Expression</strong> to generate parameter values dynamically.</p>
</li>
<li><p><strong>Result Type</strong></p>
<p>Specifies the data type of the expression's result value.</p>
<p>This property is enabled if the query parameter's type is <strong>Expression</strong>.</p>
</li>
<li><p><strong>Value</strong></p>
<p>Determines the query parameter's actual value.</p>
<p>You can specify a static actual value according to the selected value's data type.</p>
<p>Alternatively, construct an expression to generate actual parameter values dynamically. Click this property's ellipsis button to invoke the <a href="expression-editor.html">Expression Editor</a> and create an expression. This ellipsis button is enabled if you set the query parameter's type to <strong>Expression</strong>.</p>
</li>
</ul>
<h2 id="preview">Preview Results</h2>
<p>Click the <strong>Preview Results</strong> button to test a query on the actual data's limited subset at any time.</p>
<p>The opened <strong>Data Preview</strong> screen displays the first <strong>100</strong> data records of the query result set.</p>
<p><img src="../../../images/eurd-web-designer-query-builder-data-preview.png" alt="web-designer-query-builder-data-preview"></p>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                  <li>
                    <a href="https://bitbucket.org/ebsproject/cs-printout-server/src/develop/Help/articles/report-designer/report-designer-tools/query-builder.md#lines-1" class="contribution-link">Improve this Doc</a>
                  </li>
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            
            <span>EBS Copyright 2021</span>
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../../../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../../../styles/docfx.js"></script>
    <script type="text/javascript" src="../../../styles/main.js"></script>
  </body>
</html>
