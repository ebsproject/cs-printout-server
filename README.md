# **PrintOut Server**

This project is the back end of the PrintOut Server that allows to user administrator the management of reports created in the Report Designer.
It also have the capability of:
 - Manage Documents in the File Management module.
 - Manage Printers in Printer Management module.
 - Access to Documentation on how use the Report Designer.
 - Import/Export Reports among environments.

##  Pattern and Technologies
 - Net Core 3.1
 - DevExpress Report Designer
 - CUPS 
# How Setup a development environment
### Prerequisites OS Windows
- Net Core SDK [3.1](https://dotnet.microsoft.com/en-us/download/dotnet/3.1) and
- Net Core Runtime [3.1](https://dotnet.microsoft.com/en-us/download/dotnet/3.1)
- Visual Studio Code [Windows version](https://code.visualstudio.com/)

### Prerequisites OS Linux based (Ubuntu)
- Visual Studio Code for Linux
- Net Core SDK 3.1:
#### Linux commands for Net Core 3.1
    sudo apt-get update; \
    sudo apt-get install -y apt-transport-https && \
    sudo apt-get update && \
    sudo apt-get install -y dotnet-sdk-3.1
- Net Core Runtime 3.1:
#### Linux commands for Net Core Runtime 3.1
    sudo apt-get update; \
    sudo apt-get install -y apt-transport-https && \
    sudo apt-get update && \
    sudo apt-get install -y aspnetcore-runtime-3.1

You can check your recent installation typing in a terminal `dotnet sdk-list` you should see the sdk version.

### Installation

1. Clone [PrintOut repository](https://bitbucket.org/ebsproject/cs-printout-server/src/develop/) into a folder
2. In VSCode (or IDE) open the folder where the repository was cloned.
3. Use the terminal to install dependencies with dotnet command `dotnet restore`.
4. Install DevExpress dependency with dotnet command `dotnet restore "./cs-printoutserver.csproj" -s https://nuget.devexpress.com/WlU1U9rv9IXcH7SN9QFlfdDdVajSL2ZSyu5hKPEWice3iBTe2U/api -s https://api.nuget.org/v3/index.json`

# How to build or run the project
## Considerations
 - IMPORTANT: You should create a json launch file in Visual Studio Code, to do so:
  1. Click in Run and Debug.

![cs-po_run_and_debug](/cs-printout-server/Help/images/images_readme/run_and_debug.png)

  2. Click in `create launch.json file`.
  3. Select the SDK for Net Core.
   
![cs-po_run_and_debug](/cs-printout-server/Help/images/images_readme/select_environment.png)
## Edit the `launch.json` file

  1. Add/Edit the values of the recent created json file with the following in the `dev` tag:

                "ENDPOINT_CS_SG": "https://sg1.cimmyt.org:8243",
                "ENDPOINT_CS_API": "http://localhost:18080",
                "CLIENT_SECRET": "client_secret_value",
                "ENDPOINT_CUPS": "https://localhost:631",
                "CLIENT_ID": "client_id_value",
                "ASPNETCORE_ENVIRONMENT": "Development",
                "ASPNETCORE_URLS": "http://localhost:82"

  2. Replace the CS_API value with the proper from your local environment.


## Local Development

#### Run local environment

1. In Visual Studio Code click in Run and Debug button.

![cs-po_run_and_debug](/cs-printout-server/Help/images/images_readme/run_application.png)
- The server will be launch in your localhost in the port 82.

![cs-po_run_and_debug](/cs-printout-server/Help/images/images_readme/launch_browser.png)

## How Run PrintOut Server Tests
 You can run all tests or one at time, to do so:
 - Open a terminal.
 - Type `dotnet test` to run all tests.
 - Type `dotnet test ClassName.MethodName` to run an specific test.
 - Type `dotnet test --logger trx ` to get and output report file, it will be saved in TestResults folder in the test project.

