﻿using cs_sync_service.Models;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Helper
{
    public class Authentication : IAuthentication
    {
        readonly ILogger<Worker> _logger;
        public Authentication(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        public async Task<TokenResponse?> Value()
        {

            try
            {
                string sgEndPoint = Environment.GetEnvironmentVariable("EBS_USER_AUTH_URL");
                string sgClientID = Environment.GetEnvironmentVariable("CLIENT_ID");
                //string sgClientSecret = Environment.GetEnvironmentVariable("CLIENT_SECRET");
                string sgPwdUser = Environment.GetEnvironmentVariable("USER_PWD");
                string sgUser = Environment.GetEnvironmentVariable("USER_NAME");
                var options = new RestClientOptions(sgEndPoint);

                using var client = new RestClient(options);

                var request = new RestRequest( )
                    .AddHeader("Content-Type", "application/x-amz-json-1.1")
                    .AddHeader("X-Amz-Target", "AWSCognitoIdentityProviderService.InitiateAuth");
                request.Method = Method.Post;
                request.RequestFormat = DataFormat.Json;

                var body = new TokenBody
                {
                    AuthParameter = new AuthParameter
                    {
                        UserName = sgUser,
                        Password = sgPwdUser
                    },
                    ClientId = sgClientID
                };



                request.AddJsonBody(body);


                var response = await client.PostAsync(request).ConfigureAwait(false);

                dynamic resultJSON = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(response.Content);
                TokenResponse tokenResponse = ((Newtonsoft.Json.Linq.JObject)resultJSON.AuthenticationResult) .ToObject<TokenResponse>();
                return tokenResponse;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return new TokenResponse();
            }
        }


    }
}
