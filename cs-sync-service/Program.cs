using cs_sync_service.Models;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using cs_sync_service;
using cs_sync_service.Helper;
using cs_sync_service.Repository;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        Uri urlEndPoint = new Uri("http://localhost");

        urlEndPoint = new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CS_API"));


        services.AddScoped<IGraphQLClient>(s => new GraphQLHttpClient(new GraphQLHttpClientOptions
        {
            EndPoint = new Uri(urlEndPoint, "graphql")
            

        }, new NewtonsoftJsonSerializer()));

        //Repository Injection
        //services.AddScoped<ISourceOrganization,SourceOrganization>();
        //services.AddScoped<ITargetOrganization,TargetOrganization>();



        services.AddDbContext<EBSContext>();
        //services.AddScoped<EBSContext>();  
        
        //add dataflows     
        services.AddHostedService<Worker>();
        services.AddScoped<ISyncService, SyncService>();
        //services.AddScoped<IAuthentication<TokenResponse>, Authentication>();
    })
    .Build();

await host.RunAsync();
