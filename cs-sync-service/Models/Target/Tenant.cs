﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Target
{
     public class Tenant
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public bool Sync { get; set; }=false;
        public int CropId { get; set; }
        public string? CropName { get; set; }
        public int CustomerId { get; set; }
        public string? CustomerName { get; set; }



    }
}
