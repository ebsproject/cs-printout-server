﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Target
{
    public class HierarchyTree
    {
        public int Id { get; set; }
        public string? Value { get; set; }
        public int? RecordId { get; set; }
        public HierarchyTree Parent { get; set; }
        public int? ExternalRecordId { get; set; } = 0;

    }
}
