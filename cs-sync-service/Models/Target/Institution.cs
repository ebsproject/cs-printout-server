﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Target
{
     public class Institution
    {
        public int Id { get; set; }
        public string? LegalName { get; set; }
        public string? ExternalCode { get; set; } = "";
        public string? CommonName { get; set; }

    }
}
