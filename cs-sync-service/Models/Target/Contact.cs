﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Target
{
     public class Contact
    {
        [Key]
        public int Id { get; set; }
        public string? email { get; set; }
     //   public Category Category { get; set; }
        public Institution Institution { get; set; } = new Institution();
    }
}
