﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Target
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Phone { get; set; }
        public string? OfficialEmail { get; set; }
        public string? AlternateEmail { get; set; }
        public string? JobTitle { get; set; }
        public string? LanguagePreference { get; set; }
        public long? PhoneExtension { get; set; }
        public long OwningOrganizationId { get; set; }
        public bool Active { get; set; }
    }
}
