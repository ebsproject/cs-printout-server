﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Target
{
    public class User
    {
        public int Id { get; set; }
        public int? ExternalId { get; set; } = null;
        public string? UserName { get; set; }
        public bool IsActive { get; set; }

        
    }
}
