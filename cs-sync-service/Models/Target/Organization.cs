﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace cs_sync_service.Models.Target
{
    public class Organization
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
        public bool Active { get; set; }
        public string Code { get; set; }
    }
}
