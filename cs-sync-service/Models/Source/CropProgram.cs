﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
     public class CropProgram
    {
        [Key]
        public int Id { get; set; }
        public string? CropProgramCode { get; set; }
        public string? CropProgramName { get; set; }
        public Crop Crop { get; set; } = new Crop();
        public Organization Organization { get; set; } = new Organization();
        public List<Program> Programs { get; set; } = new List<Program>();
        public List<CropProgramTeam> CropProgramTeams { get; set; } = new List<CropProgramTeam>();
    }
}
