﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
    public class TeamMember
    {
        [Key]
        public int Id { get; set; }
        public Team Team { get; set; }= new Team();
        public Person Person { get; set; } = new Person();
    }
}
