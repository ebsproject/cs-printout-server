﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
    public class Organization
    {
        [Key]
        public int Id { get; set; }
        public string? OrganizationName { get; set; }
        public string? OrganizationCode { get; set; }
        public string? Description { get; set; }
        public bool? IsVoid { get; set; } =false;

        public List<CropProgram>? CropPrograms { get; set; }

    }
}
