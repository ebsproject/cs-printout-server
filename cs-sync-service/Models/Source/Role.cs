﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
    public class Role
    {
 
        public int Id { get; set; }
        public string? PersonRoleCode { get; set; }
        public string? PersonRoleName { get; set; }
     
    }
}
