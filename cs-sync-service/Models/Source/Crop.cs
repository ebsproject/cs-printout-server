﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
    public class Crop
    {
        [Key]
        public int Id { get; set; }
        public string CropName { get; set; }

        public List<CropProgram> CropPrograms { get; set; }
    }
}
