﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
    public class Program
    {
        [Key]
        public int Id { get; set; }
        public string? ProgramCode { get; set; }
        public string? ProgramName { get; set; }
        public string? ProgramType { get; set; }
        public string? ProgramStatus { get; set; }        
        public List<ProgramTeam> ProgramTeams { get; set; }
        public CropProgram CropProgram { get; set; } = new CropProgram();
    }
}
