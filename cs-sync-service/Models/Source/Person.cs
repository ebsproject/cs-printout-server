﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
     public class Person
    {
        [Key]
        public int Id { get; set; }
        public string? UserName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public bool IsActive { get; set; } = false;
        public string? Email { get; set; }
        public string? PersonName { get; set; }
        public string? PersonStatus { get; set; }
        public string? PersonType { get; set; }
        public int RoleId { get; set; }
        public string? RoleName { get; set; }
        public string? RoleCode { get; set; }
        public Role Role { get; set; } = new Role();
        public List<TeamMember> TeamMembers { get; set; } = new List<TeamMember>();

    }
}
