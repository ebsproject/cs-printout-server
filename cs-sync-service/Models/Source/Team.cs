﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Models.Source
{
     public class Team
    {
        [Key]
        public int Id { get; set; }
        public string? TeamCode { get; set; }
        public string? TeamName { get; set; }

        public List<CropProgramTeam> CropProgramTeams { get; set; } = new List<CropProgramTeam>();
        public List<TeamMember> TeamMembers { get; set; } = new List<TeamMember>();

    }
}
