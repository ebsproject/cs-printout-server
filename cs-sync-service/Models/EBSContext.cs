﻿using System; 
using Microsoft.EntityFrameworkCore;
using cs_sync_service.Models;

namespace cs_sync_service.Models
{
    public class EBSContext : DbContext
    {

        protected override void OnConfiguring
           (DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "EBSDatabase");
            optionsBuilder.EnableSensitiveDataLogging();
        }

        public virtual DbSet<Source.Organization> SourceOrganization { get; set; }
        public virtual DbSet<Source.Program> SourceProgram { get; set; }

        public virtual DbSet<Source.Person> SourcePerson { get; set; }
        public virtual DbSet<Source.Role> SourceRole { get; set; }
        public virtual DbSet<Source.TeamMember> SourceTeamMember { get; set; }
        public virtual DbSet<Source.Team> SourceTeam { get; set; }
        public virtual DbSet<Source.CropProgramTeam> SourceProgramTeam { get; set; }
        public virtual DbSet<Source.CropProgram> SourceCropProgram { get; set; }
        public virtual DbSet<Source.CropProgramTeam> SourceCropProgramTeam { get; set; }


        /// <summary>
        /// Targets
        /// </summary>
        public virtual DbSet<Target.Organization> TargetOrganization { get; set; }
        public virtual DbSet<Target.Customer> TargetCustomer { get; set; }
        public virtual DbSet<Target.Contact> TargetContact { get; set; }
        public virtual DbSet<Target.Category> TargetCategory{ get; set; }
        public virtual DbSet<Target.Institution> TargetInstitution { get; set; }
        public virtual DbSet<Target.Tenant> TargetTenant { get; set; }
        public virtual DbSet<Target.Program> TargetProgram { get; set; }
        public virtual DbSet<Target.User> TargetUser { get; set; }
        public virtual DbSet<Target.Role> TargetRole { get; set; }
    }
}
