using cs_sync_service.Models;
using cs_sync_service.Repository;
using GraphQL.Client.Abstractions;

namespace cs_sync_service;

/// <summary>
/// Background process
/// </summary>
public sealed class Worker : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<Worker> _logger;
    
    private int _executionCount;

    /// <summary>
    /// Sync Worker
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <param name="logger"></param>
    public Worker(IServiceProvider serviceProvider,
                    ILogger<Worker> logger)
    =>
    (_serviceProvider, _logger) = (serviceProvider, logger);
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation(
            $"{nameof(Worker)} is running.");

        await DoWorkAsync(stoppingToken);
    }

    /// <summary>
    /// Execute the Worker, group by each sync process
    /// </summary>
    /// <param name="stoppingToken"></param>
    /// <returns></returns>
    private async Task DoWorkAsync(CancellationToken stoppingToken)
    {
        // int sync_timeout = Environment.GetEnvironmentVariable("SYNC_TIMEOUT") != null ? int.Parse(Environment.GetEnvironmentVariable("SYNC_TIMEOUT")) : 60000;
        int sync_timeout = 10000;
        while (!stoppingToken.IsCancellationRequested)
        {
            ++_executionCount;

            _logger.LogInformation(
                "{ServiceName} working, executing Sync Process  count: {Count}",
                nameof(SyncService),
                _executionCount);
            using (IServiceScope scope = _serviceProvider.CreateScope())
            {


                ISyncService SyncService =
                    scope.ServiceProvider.GetRequiredService<ISyncService>();

                IGraphQLClient GraphqlClientService = scope.ServiceProvider.GetRequiredService<IGraphQLClient>();
             //   EBSContext EBSContextService = scope.ServiceProvider.GetRequiredService<EBSContext>();


                Helper.Authentication auth = new Helper.Authentication(_logger);

                var token = await auth.Value();

                EBSContext EBSContextService = new EBSContext();

                TargetTenant tenant = new TargetTenant(EBSContextService, GraphqlClientService, _logger, token.IdToken);

                bool isCleanDB = false;




                if (EBSContextService.TargetTenant != null)
                {
                    var defaultTenant = EBSContextService.TargetTenant.FirstOrDefault();
                    
                    if(defaultTenant.Sync == false && defaultTenant.CropId == 0 && isCleanDB==false)
                    {
                        //review if the db is clean
                        TargetUser user = new TargetUser(EBSContextService, GraphqlClientService, _logger, token.IdToken);
                        TargetRole role = new TargetRole(EBSContextService, GraphqlClientService, _logger, token.IdToken);

                        isCleanDB = EBSContextService.TargetUser.Count() <= 1;
                    }


                    if ((defaultTenant.Sync && defaultTenant.CropId !=0) || isCleanDB)
                    {
                        //initialize objects
                        SourceProgram sourceProgram = new SourceProgram(EBSContextService, GraphqlClientService, _logger, token.IdToken, defaultTenant);
                        SourceOrganization sourceOrganization = new SourceOrganization(EBSContextService, GraphqlClientService, _logger, token.IdToken);
                        SourcePerson sourcePerson = new SourcePerson(EBSContextService, GraphqlClientService, _logger, token.IdToken);


                        TargetCustomer customer = new TargetCustomer(EBSContextService, GraphqlClientService, _logger, token.IdToken);
                        TargetContact contact = new TargetContact(EBSContextService, GraphqlClientService, _logger, token: token.IdToken);
                        TargetProgram program = new TargetProgram(EBSContextService, GraphqlClientService, _logger, token.IdToken);
                        TargetUser user = new TargetUser(EBSContextService, GraphqlClientService, _logger, token.IdToken);
                        TargetRole role = new TargetRole(EBSContextService, GraphqlClientService, _logger, token.IdToken);

                        await SyncService.ExecuteTenantDataflowAsync(stoppingToken,customer, contact, program,user,tenant);
                    }
                    if (defaultTenant.Sync && defaultTenant.CropId == 0) //reset hierarchy tree
                    { 
                           await tenant.CleanHierarchAsync();
                    }
                 }

             

            }
            await Task.Delay(sync_timeout, stoppingToken);
        }
    }

    /// <summary>
    /// stop the service
    /// </summary>
    /// <param name="stoppingToken"></param>
    /// <returns></returns>
    public override async Task StopAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation(
            $"{nameof(Worker)} is stopping.");

        await base.StopAsync(stoppingToken);
    }
}