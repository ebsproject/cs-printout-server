﻿using Paillave.Etl.Core;
using Paillave.Etl.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs_sync_service.Models;
using cs_sync_service.Repository;

namespace cs_sync_service
{
    public class SyncService : ISyncService
    {

        readonly ILogger<SyncService> _logger;
        readonly EBSContext _context;

        internal ITargetCustomer _targetCustomer;
        internal ITargetContact _targetContact;
        internal ITargetTenant _targetTenant;
        internal ITargetProgram _targetProgram;
        internal ITargetUser _targetUser;


        public SyncService(EBSContext context,
                           ILogger<SyncService> logger)
        {
            _context = context;
            _logger = logger;
        }


        public async Task ExecuteTenantDataflowAsync(CancellationToken stoppingToken,
                                                     ITargetCustomer targetCustomer,
                                                     ITargetContact targetContact,
                                                     ITargetProgram targetProgram,
                                                     ITargetUser targetUser,
                                                     ITargetTenant targetTenant)
        {
            _targetCustomer = targetCustomer;
            _targetContact = targetContact;
            _targetProgram = targetProgram;
            _targetUser = targetUser;
            _targetTenant = targetTenant;

            var syncOrganizations = StreamProcessRunner.Create<string>(SyncOrganizations);
            var syncPrograms = StreamProcessRunner.Create<string>(SyncPrograms);
            var syncContacts = StreamProcessRunner.Create<string>(SyncContacts);
            var syncUsers = StreamProcessRunner.Create<string>(SyncUsers);
            var syncTenant = StreamProcessRunner.Create<string>(SyncTenant);

            //inject DBContext
            var executionOptions = new ExecutionOptions<string>
            {
                TraceProcessDefinition = (ts, cs) => ts.Do("Show trace on console", t => _logger.LogTrace("Show Trace {trx}", t.ToString())),
                Resolver = new SimpleDependencyResolver().Register<EBSContext>(_context)
            };


            var res = await syncOrganizations.ExecuteAsync("sycOrganzation", executionOptions);

            if (res.Failed)
                _logger.LogError("Error Executing Organization Sync Process: {err}", res.ErrorTraceEvent.Content);

            res = await syncContacts.ExecuteAsync("syncContacts", executionOptions);
            if (res.Failed)
                _logger.LogError("Error Executing Institution Contact Sync Process: {err}", res.ErrorTraceEvent.Content);
            res = await syncPrograms.ExecuteAsync("syncPrograms", executionOptions);
            if (res.Failed)
                _logger.LogError("Error Executing Program Sync Process: {err}", res.ErrorTraceEvent.Content);

            res = await syncUsers.ExecuteAsync("syncUsers", executionOptions);
            if (res.Failed)
                _logger.LogError("Error Executing Users Sync Process: {err}", res.ErrorTraceEvent.Content);

            res = await syncTenant.ExecuteAsync("syncTenant", executionOptions);
            if (res.Failed)
                _logger.LogError("Error Executing Users Sync Process: {err}", res.ErrorTraceEvent.Content);

        }

        private void SyncTenant(ISingleStream<string> contextStream)
        {

            var CBUsers = contextStream
                            .EfCoreSelect("get Tenant from CS", (o, row) => o
                            .Set<Models.Target.Tenant>())
                            .Do("Display records", i => _targetTenant.SyncHierarchyTree(i));
        }
        private void SyncUsers(ISingleStream<string> contextStream)
        {
            var CBUsers = contextStream
                            .EfCoreSelect("get users from Users from Core Breeding", (o, row) => o
                            .Set<Models.Source.Person>()
                            .Where(w => !String.IsNullOrEmpty(w.Email))
                            );





            var SyncNewUsers = CBUsers
                          .EfCoreLookup("Lookup Users", o =>
                          o.Set<Models.Target.User>()
                          .On(s => s.Email.Trim().ToLower(), t => t.UserName.Trim().ToLower())
                          .Select((s, t) => new { Source = s, Target = t })
                          )
                          .Where("filter the user missings", i => i.Target == null)
                          .Do("Show User Missing", i => _logger.LogInformation("User {item}", i.Source.PersonName))
                          .Select("Insert Information", async i => await _targetUser.CreateAsync(i.Source))
                          .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
                          {

                              if (t1.Status == TaskStatus.RanToCompletion)
                              {
                                  _logger.LogInformation("User Created {info}", t1.Result.UserName);

                              }
                              else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
                              else if (t1.IsFaulted)
                              {
                                  _logger.LogError("Error {info}", t1.Exception?.Message);
                              }
                          }));

            var SyncRolesUsers = CBUsers
                  .EfCoreLookup("Lookup Users", o =>
                  o.Set<Models.Target.User>()
                  .On(s => s.Email.Trim().ToLower(), t => t.UserName.Trim().ToLower())
                  .Select((s, t) => new { Source = s, Target = t })
                  )
                  .Where("filter the user missings", i => i.Target != null && i.Source.IsActive)
                  .Select("Inject Roles", async i => await _targetUser.CreateRoleAsync(i.Source, i.Target))
                  .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
                  {

                      if (t1.Status == TaskStatus.RanToCompletion)
                      {
                          _logger.LogInformation("Role Created {info}", t1.Result.UserName);

                      }
                      else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
                      else if (t1.IsFaulted)
                      {
                          _logger.LogError("Error {info}", t1.Exception?.Message);
                      }
                  }));

            //null external id
            //var SycCSUsersNonActive = contextStream
            //                        .EfCoreSelect("get user not active", (o, row) => o
            //                            .Set<Models.Target.User>()
            //                            .Where(i => !i.IsActive))
            //                        .Do("show value on console", i => Console.WriteLine(i.UserName))
            //                        .Select("Change External Id", async i => await _targetUser.UpdateNonActiveAsync(i))
            //                                 .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
            //                             {

            //                                 if (t1.Status == TaskStatus.RanToCompletion)
            //                                 {
            //                                     _logger.LogInformation("External Id Updated {info}", t1.Result.UserName);

            //                                 }
            //                                 else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
            //                                 else if (t1.IsFaulted)
            //                                 {
            //                                     _logger.LogError("Error {info}", t1.Exception?.Message);
            //                                 }
            //                             }));

            var SyncReviewStatus = CBUsers
                 .EfCoreLookup("Lookup Users", o =>
                 o.Set<Models.Target.User>()
                 .On(s => s.Email.Trim().ToLower(), t => t.UserName.Trim().ToLower())
                 .Select((s, t) => new { Source = s, Target = t })
                 )
                 .Where("filter the user missings", i => i.Target != null && (i.Target.ExternalId == null || i.Target.IsActive != i.Source.IsActive))
                 .Do("Show User Name", i => _logger.LogInformation("User {item}", i.Source.PersonName))
                 .Select("Review Status", async i => await _targetUser.UpdateAsync(i.Source, i.Target))
                 .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
                 {

                     if (t1.Status == TaskStatus.RanToCompletion)
                     {
                         _logger.LogInformation("Status and External Ids Created {info}", t1.Result.UserName);

                     }
                     else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
                     else if (t1.IsFaulted)
                     {
                         _logger.LogError("Error {info}", t1.Exception?.Message);
                     }
                 }));




        }
        private void SyncPrograms(ISingleStream<string> contextStream)
        {





            var CBPrograms = contextStream
                             .EfCoreSelect("get programs from CoreBreeding", (o, row) => o
                             .Set<Models.Source.Program>())
                             .Do("Display records", i => _logger.LogInformation(" Start to Update Program {program}", i.ProgramCode));
            ;


            var SyncNewPrograms = CBPrograms
                            .EfCoreLookup("Lookup Programs", o =>
                            o.Set<Models.Target.Program>()
                            .On(s => s.ProgramCode.Trim().ToLower(), t => t.Code.Trim().ToLower())
                            .Select((s, t) => new { Source = s, Target = t })
                            )
                            .Where("filter the program missings", i => i.Target == null)
                            .Do("Show Program Missing", i => _logger.LogInformation("Program {item}", i.Source.ProgramName))
                            .Select("Insert Information", async i => await _targetProgram.CreateAsync(i.Source))
                            .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
                            {

                                if (t1.Status == TaskStatus.RanToCompletion)
                                {
                                    _logger.LogInformation("Program Created {info}", t1.Result.Code);

                                }
                                else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
                                else if (t1.IsFaulted)
                                {
                                    _logger.LogError("Error {info}", t1.Exception?.Message);
                                }
                            }));


            var SyncUpdatePrograms = CBPrograms
                                     .EfCoreLookup("find CS Program",
                                                    o => o.Set<Models.Target.Program>()
                                                   .On(s => s.ProgramCode.Trim().ToLower(), t => t.Code.Trim().ToLower())
                                                    .Select((s, t) => new { Source = s, Target = t })
                                                    )
                                                    .Where("filter the program match", i => i.Target != null)
                                                    .Where("filter External Ids Null", i => i.Target.ExternalId == null)
                                                    .Do("Display records", i => _targetProgram.UpdateAsync(i.Target, i.Source));







        }
        private void SyncOrganizations(ISingleStream<string> contextStream)
        {


            var CBInstitutions = contextStream
            .EfCoreSelect("get organization from CoreBreeding", (o, row) => o
        .Set<Models.Source.Organization>());



            //Add the CB Organization to Tenant Customer
            var streamTenantCustomer = CBInstitutions
                            .EfCoreLookup("Lookup Organization", o =>
                            o.Set<Models.Target.Customer>()
                            .On(s => s.OrganizationCode.Trim().ToLower(), t => t.Name.Trim().ToLower())
                            .Select((s, t) => new { Source = s, Target = t })
                            )
                            .Where("filter the organization missings", i => i.Target == null)
                            .Do("Show Organization Missing", i => _logger.LogInformation("Organization {item}", i.Source.OrganizationName))
                            .Select("Insert Information", async i => await _targetCustomer.CreateAsync(i.Source))
                            .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
                            {

                                if (t1.Status == TaskStatus.RanToCompletion)
                                {
                                    _logger.LogInformation("Organization Created {info}", t1.Result.Name);

                                }
                                else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
                                else if (t1.IsFaulted)
                                {
                                    _logger.LogError("Error {info}", t1.Exception?.Message);
                                }
                            }));




        }
        private void SyncContacts(ISingleStream<string> contextStream)
        {


            var CBInstitutions = contextStream
            .EfCoreSelect("get organization from CoreBreeding", (o, row) => o
        .Set<Models.Source.Organization>());

            var SyncInstitutionContact = CBInstitutions
                            .EfCoreLookup("Lookup Organization", o =>
                            o.Set<Models.Target.Institution>()
                            .On(s => s.OrganizationCode.Trim().ToLower(), t => t.CommonName.Trim().ToLower())
                            .Select((s, t) => new { Source = s, Target = t })
                            )
                            .Where("filter the organization missings", i => i.Target == null)
                            .Do("Show Organization Missing", i => _logger.LogInformation("Organization {item}", i.Source.OrganizationName))
                            .Select("Insert Information", async i => await _targetContact.CreateAsync(i.Source))
                            .Do("Display Transaction Status", i => i.ContinueWith(t1 =>
                            {

                                if (t1.Status == TaskStatus.RanToCompletion)
                                {
                                    _logger.LogInformation("Organization Created {info}", t1.Result.email);

                                }
                                else if (t1.IsCanceled) _logger.LogError("Error {info}", t1.Exception?.Message);
                                else if (t1.IsFaulted)
                                {
                                    _logger.LogError("Error {info}", t1.Exception?.Message);
                                }
                            }));

        }
    }
}
