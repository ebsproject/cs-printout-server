﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Dataflows
{
    public interface IDataFlowTenant
    {
         Task SyncTenant();
    }
}
