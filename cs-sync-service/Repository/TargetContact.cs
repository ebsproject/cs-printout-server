﻿using cs_sync_service.Helper;
using cs_sync_service.Models;
using cs_sync_service.Models.Target;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class TargetContact : ITargetContact
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly IAuthentication _authentication;
        internal string _token;
        public TargetContact(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;
            Task.Run(() => this.RefreshAsync()).Wait();

        }
        public async Task RefreshAsync()
        {
            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findContactList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                                    filters: [
                                            {{col: ""category.id"",
                                              mod: EQ,
                                              val: ""2""
                                            }}
                                          ]
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                                  id
                                                  email
                                                  category {{
                                                      id
                                                      name
                                                  }}
                                                  institution{{id
                                                    legalName
                                                    externalCode
                                                    commonName
                                                  }}
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                List<Models.Target.Contact> contacts = response.Data[$"findContactList"]["content"].ToObject<List<Models.Target.Contact>>();

                _context.TargetContact.RemoveRange(_context.TargetContact.ToList());
                _context.TargetInstitution.RemoveRange(_context.TargetInstitution.ToList());


                _context.AddRange(contacts);
                _context.SaveChanges();
                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Contact from Core System, review the endpoint: {ERR}", ex.Message);
            }
        }
        public async Task<Contact> CreateAsync(Models.Source.Organization organization)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation CREATE_CONTACT($contact: ContactInput!) {
                       createContact(contact: $contact) {
                       id
                       email                      
                     }
               }",
                OperationName = "CREATE_CONTACT",
                Variables = new { contact = new { id = 0, category = new { id = 2 }, contactTypeIds = new int[] { 5 },institution= new { cgiar=true,legalName=organization.OrganizationName,commonName=organization.OrganizationCode }, tenantIds = new int[] {1 }, purposeIds = new int[] { 3 } } }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                Contact contact = response.Data["createContact"].ToObject<Contact>();
                _context.TargetContact.Add(contact);
                _context.SaveChanges();
                return contact;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<Contact>> GetListAsync()
        {
            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findContactList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                                    filters: [
                                            {{col: ""category.id"",
                                              mod: EQ,
                                              val: ""2""
                                            }}
                                          ]
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                                  id
                                                  email
                                                  category {{
                                                      id
                                                      name
                                                  }}
                                                  institution{{id
                                                    legalName
                                                    externalCode
                                                    commonName
                                                  }}
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                List<Models.Target.Contact> contacts = response.Data[$"findContactList"]["content"].ToObject<List<Models.Target.Contact>>();

                return contacts;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Contact from Core System, review the endpoint: {ERR}", ex.Message);
                return new List<Models.Target.Contact>();
            }
           
        }
    }
}
