﻿using cs_sync_service.Models;
using cs_sync_service.Models.Source;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class SourcePerson : ISourcePerson
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly string _token;
        public SourcePerson(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;
            try
            {

                dynamic response = GetDataPerPage(1).Result;

                int currentPage = (int)response.number;
                int totalCount = (int)response.totalElements;
                int totalPages = (int)response.totalPages;
                int pageSize = (int)response.size;

                List<dynamic> persons = response.content.ToObject<List<dynamic>>();

                if (totalPages > 1)
                {
                    for (int i = 2; i <= totalPages; i++)
                    {
                         response = GetDataPerPage(i).Result;
                         persons.AddRange(response.content.ToObject<List<dynamic>>());
                    }
                }



                        //removes


                        _context.SourcePerson.RemoveRange(_context.SourcePerson.ToList());
                foreach (var objPerson in persons)
                {
                    Models.Source.Person person = JsonConvert.DeserializeObject<Person>(objPerson.ToString());

                    person.RoleId = person.Role.Id;
                    person.RoleName = person.Role.PersonRoleName;
                    person.RoleCode = person.Role.PersonRoleCode;
                    var entry = _context.Entry(person);
                    entry.State = EntityState.Added;
                    _context.SaveChanges();
                }


                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("The connection with Core Breeding GraphQL API is not working: {ERR}", ex.Message);
            }
        }
        public async Task<IEnumerable<Person>> GetListAsync()
        {
            var result = _context.SourcePerson.ToList();
            return await Task.FromResult(result);
        }

        private async Task<dynamic> GetDataPerPage(int page){

            //implementation
            using var graphQLClient = new GraphQLHttpClient(new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CB_GRAPHQL")), relativeUri: "graphql"), new NewtonsoftJsonSerializer());


            graphQLClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            var request = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findPersonList(page: {{
                                        number: {page}
                                        size: 300
                                    }}
                                    
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                                  id
                                                  email
                                                  firstName
                                                  lastName
                                                  isActive
                                                  username
                                                  personName
                                                  personStatus
                                                  personType
                                                  role {{
                                                    id
                                                    personRoleName
                                                    personRoleCode
                                                    description
                                                  }}
                                                  teamMembers {{
                                                    id
                                                    team {{
                                                      id
                                                      teamCode
                                                      teamName
                                                      cropProgramTeams {{
                                                        id
                                                        cropProgram{{
                                                          id              
                                                          programs {{
                                                            id
                                                            programCode
                                                            programName
                                                          }}
                                                        }}
                                                      }}
                                                    }}
                                                  }}                                        
                                       }}
                                }}
                            }}"
            };
            var response = graphQLClient.SendQueryAsync<dynamic>(request).Result;

            var content = response.Data[$"findPersonList"].ToObject<dynamic>();
            return content;
        }

      
    }
}
