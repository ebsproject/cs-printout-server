﻿using cs_sync_service.Models;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class SourceProgram : ISourceProgram
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly string _token;

        public SourceProgram(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token,Models.Target.Tenant tenant)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;

            try
            {
                //implementation
                using var graphQLClient = new GraphQLHttpClient(new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CB_GRAPHQL")), relativeUri: "graphql"), new NewtonsoftJsonSerializer());


                graphQLClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findProgramList(
                                   page: {{
                                        number: 1
                                        size: 300
                                    }}                                                                 
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                            id
                                            programCode
                                            programName
                                            cropProgram {{id
                                                crop {{
                                                    id
                                                    cropCode
                                                    cropName
                                                }}
                                            }}
                                        
                                       }}
                                }}
                            }}"
                };
                var response = graphQLClient.SendQueryAsync<dynamic>(request).Result;

                List<Models.Source.Program> programs = response.Data[$"findProgramList"]["content"].ToObject<List<Models.Source.Program>>();

                var objPrograms = programs.Where(w=>w.CropProgram.Crop.CropName.Trim().ToLower().Equals(tenant.CropName.Trim().ToLower()));

                _context.SourceProgram.RemoveRange(_context.SourceProgram.ToList());
                foreach (var program in objPrograms)
                {
                    var entry = _context.Entry(program);
                    entry.State = EntityState.Added;
                    _context.SaveChanges();
                }
                   

         
                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("The connection with Core Breeding GraphQL API is not working: {ERR}", ex.Message);
            }
        }
        public async Task<IEnumerable<Models.Source.Program>> GetListAsync()
        {
            var result = _context.SourceProgram.ToList();
            return await Task.FromResult(result);
        }
    }
}
