﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ISourceProgram
    {
        Task<IEnumerable<Models.Source.Program>> GetListAsync();

    }
}
