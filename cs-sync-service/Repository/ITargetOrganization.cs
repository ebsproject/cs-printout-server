﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ITargetOrganization
    {
        Task<IEnumerable<Models.Target.Organization>> GetListAsync();
        Task<Models.Target.Organization> CreateAsync(Models.Source.Organization organization);
    
    }
}
