﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ITargetProgram
    {
        Task<IEnumerable<Models.Target.Program>> GetListAsync();
        Task<Models.Target.Program> CreateAsync(Models.Source.Program program);
        Task<Models.Target.Program> UpdateAsync(Models.Target.Program target, Models.Source.Program source);
    }
}
