﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ITargetCustomer
    {
        Task<IEnumerable<Models.Target.Customer>> GetListAsync();
        Task<Models.Target.Customer> CreateAsync(Models.Source.Organization organization);
    }
}
