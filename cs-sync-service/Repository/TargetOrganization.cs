﻿using cs_sync_service.Helper;
using cs_sync_service.Models;
using cs_sync_service.Models.Target;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class TargetOrganization : ITargetOrganization
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly IAuthentication _authentication;
        internal string _token;

        public TargetOrganization(EBSContext context, IGraphQLClient client, ILogger<Worker> logger,string token)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;
            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findOrganizationList(page: {{
                                        number: 1
                                        size: 300
                                    }}){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                            id
                                            name
                                            legalName
                                            code
                                            active
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                List<Models.Target.Organization> organizations = response.Data[$"findOrganizationList"]["content"].ToObject<List<Models.Target.Organization>>();
                _context.TargetOrganization.RemoveRange(_context.TargetOrganization.ToList());
                _context.AddRange(organizations);
                _context.SaveChanges();
                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Contact from Core System, review the endpoint {ERR}", ex.Message);
            }
            
        }

        public async Task<Organization> CreateAsync(Models.Source.Organization organization)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation CREATE_ORGANIZATION($organization: OrganizationInput!) {
                       createOrganization(organization: $organization) {
                       id
                       name
                       legalName
                       active
                       code
                     }
               }",
                OperationName = "CREATE_ORGANIZATION",
                Variables = new { organization = new {id=0, name=organization.OrganizationName, legalName=organization.OrganizationName, code= organization.OrganizationCode,active=true } }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",",response.Errors.Select(s=>s.Message)));


                Organization org = response.Data["createOrganization"].ToObject<Organization>();                
                _context.TargetOrganization.Add(org);
                _context.SaveChanges();
                return org;
            }
            catch(Exception ex)
            {                
                throw new Exception(ex.Message);
            }
         
        }

        public async Task<IEnumerable<Models.Target.Organization>> GetListAsync()
        {
            var result = _context.TargetOrganization.ToList();
            return await Task.FromResult(result);
        }

      
    }
}
