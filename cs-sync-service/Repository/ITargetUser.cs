﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ITargetUser
    {

        Task<IEnumerable<Models.Target.User>> GetListAsync();
        Task<Models.Target.User> CreateAsync(Models.Source.Person person);
        Task<Models.Target.User> CreateRoleAsync(Models.Source.Person person, Models.Target.User user);
        Task<Models.Target.User> UpdateAsync(Models.Source.Person person, Models.Target.User user);
        Task<Models.Target.User> UpdateNonActiveAsync( Models.Target.User user);
        
    }
}
