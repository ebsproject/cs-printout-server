﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs_sync_service.Models;
using cs_sync_service.Helper;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.EntityFrameworkCore;

namespace cs_sync_service.Repository
{
    public class SourceOrganization : ISourceOrganization
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly string _token;
        public SourceOrganization(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;
            try
            {
                //implementation
                using var graphQLClient = new GraphQLHttpClient(new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CB_GRAPHQL")), relativeUri: "graphql"), new NewtonsoftJsonSerializer());


                graphQLClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findOrganizationList(page: {{
                                        number: 1
                                        size: 300
                                    }}
                                    
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                             id
                                               organizationCode
                                               organizationName
                                               description
                                        
                                       }}
                                }}
                            }}"
                };
                var response = graphQLClient.SendQueryAsync<dynamic>(request).Result;
                List<Models.Source.Organization> organizations = response.Data[$"findOrganizationList"]["content"].ToObject<List<Models.Source.Organization>>();
                _context.SourceOrganization.RemoveRange(_context.SourceOrganization.ToList());
                _context.AddRange(organizations);
                _context.SaveChanges();
                foreach (var entity in _context.ChangeTracker.Entries())                
                    entity.State = EntityState.Detached;
                 
            }
            catch (Exception ex)
            {
                _logger.LogError("The connection with Core Breeding GraphQL API is not working: {ERR} for Organizations", ex.Message);
            }
        }


        public async Task<IEnumerable<Models.Source.Organization>> GetListAsync()
        {
                      
            var result = _context.SourceOrganization.ToList();
            return await Task.FromResult(result);
        }
    }
}
