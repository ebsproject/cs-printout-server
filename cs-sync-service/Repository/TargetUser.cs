﻿using cs_sync_service.Models;
using cs_sync_service.Models.Source;
using cs_sync_service.Models.Target;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class TargetUser : ITargetUser
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        internal string _token;

        public TargetUser(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _client = client;
            _logger = logger;
            _token = token;
            _context = context;

            try
            {
                //implementation               
                dynamic response = GetDataPerPage(1).Result;

                int currentPage = (int)response.number;
                int totalCount = (int)response.totalElements;
                int totalPages = (int)response.totalPages;
                int pageSize = (int)response.size;

                List<User> users = response.content.ToObject<List<User>>();

                if (totalPages > 1)
                {
                    for (int i = 2; i <= totalPages; i++)
                    {
                        response = GetDataPerPage(i).Result;
                        users.AddRange(response.content.ToObject<List<User>>());
                    }
                }

                var query = users.GroupBy(x => x).Where(g => g.Count() > 1).Select(y => y.Key).ToList();

            
                _context.TargetUser.RemoveRange(_context.TargetUser.ToList());
                _context.SaveChanges();
                var s = _context.TargetUser.Select(s => s);

                foreach(var user in users)
                {
                    try {
                        _context.Add(user);
                        _context.SaveChanges();
                    } catch { }
                }
                

                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Users from Core System, review the endpoint: {ERR}", ex.Message);
            }

        }
        private async Task<dynamic> GetDataPerPage(int page)
        {


            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            var request = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findUserList(
                                    page: {{
                                        number: {page}
                                        size: 300
                                    }}
                           
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                           id
                                          externalId
                                          userName
                                          isActive                                              
                                       }}
                                }}
                            }}"
            };
            var response = csClient.SendQueryAsync<dynamic>(request).Result;
            var content = response.Data[$"findUserList"].ToObject<dynamic>();
            return content;
        }
        private async Task<Models.Target.Contact> ContactByEmailAsync(string email)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;
            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            var request = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findContactList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                                    filters: [
                                            {{col: ""category.id"",
                                              mod: EQ,
                                              val: ""1""
                                            }},
                                            {{
                                              col: ""email"",
                                              mod: EQ,
                                              val: ""{email}""
                                            }}
                                          ]
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                              id
                                              email     
                                              person{{
                                                givenName
                                                familyName
                                                additionalName
                                                salutation
                                              }}
                                       }}
                                }}
                            }}"
            };

            var response = csClient.SendQueryAsync<dynamic>(request).Result;
            List<Models.Target.Contact> contacts = response.Data[$"findContactList"]["content"].ToObject<List<Models.Target.Contact>>();
            var contact = contacts.FirstOrDefault();
            return contact == null ? new Contact() : contact;

        }
        private async Task<Models.Target.Contact> ContactByGivenAndFamilyNameAsync(string givenName, string familyName)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;
            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            var request = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findContactList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                                    filters: [
                                            {{col: ""category.id"",
                                              mod: EQ,
                                              val: ""1""
                                            }},
                                             {{col: ""person.givenName""
                                                    mod: EQ
                                                    val: ""{givenName}""
                                                }},
                                                {{
                                                    col: ""person.familyName""
                                                    mod: EQ
                                                    val: ""{familyName}""
                                                }} 
                                          ]
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                              id
                                              email     
                                              person{{
                                                givenName
                                                familyName
                                                additionalName
                                                salutation
                                              }}
                                       }}
                                }}
                            }}"
            };

            var response = csClient.SendQueryAsync<dynamic>(request).Result;
            List<Models.Target.Contact> contacts = response.Data[$"findContactList"]["content"].ToObject<List<Models.Target.Contact>>();
            var contact = contacts.FirstOrDefault();
            return contact == null ? new Contact() : contact;

        }
     
        private async Task<Models.Target.Contact> CreateContactAsync(dynamic newContact)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);



            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation CREATE_CONTACT($contact: ContactInput!) {
                       createContact(contact: $contact) {
                       id
                       email                      
                     }
               }",
                OperationName = "CREATE_CONTACT",
                Variables = new { contact = newContact }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                Contact contact = response.Data["createContact"].ToObject<Contact>();
                _context.TargetContact.Add(contact);
                _context.SaveChanges();
                return contact;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<User> CreateAsync(Person person)
        {

            
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            int contactId = 0;

            //Get Contact Informaation

            var ContactInfo = await ContactByGivenAndFamilyNameAsync(person.FirstName,person.LastName);

           
            if (ContactInfo.Id == 0)
            {
                dynamic contact = new
                {

                    id = 0,
                    category = new { id = 1 },
                    contactTypeIds = new int[] { 9 },
                    email = person.Email,
                    person = new
                    {
                        familyName = person.LastName,
                        givenName = person.FirstName,
                        gender = "To be Defined"
                    },
                    tenantIds = new int[] { 1 },
                    purposeIds = new int[] { 2 }

                };
                ContactInfo = await CreateContactAsync(contact);
                contactId = ContactInfo.Id;
            }
            else
            {
                contactId = ContactInfo.Id;
            }




            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation CREATE_USER($user: UserInput!) {
                       createUser(user: $user) {
                         id
                                             
                     }
               }",
                OperationName = "CREATE_USER",
                Variables = new { user = new { id = 0, userName = person.Email, tenantIds = new int[] { 1 }, active = person.IsActive, contactId = contactId } }
            };


            try
            {
                if(contactId == 0)            
                    throw new Exception($"The CB User {person.PersonName} couldn't add in Core System");
            
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                User user = response.Data["createUser"].ToObject<User>();
                _context.TargetUser.Add(user);
                _context.SaveChanges();
                return user;
            }
            catch (Exception ex)
            {
              
                throw new Exception($"Error creating the CB User: {person.PersonName} Error: {ex.Message}");
            }
        }
        public async Task<List<Models.Target.Role>> RolesAsync(Person person)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            int contactId = 0;

            //Get Contact Informaation

            var request = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findUserList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}

                                    filters: [
                                          
                                            {{
                                              col: ""userName"",
                                              mod: EQ,
                                              val: ""{person.Email}""
                                            }}
                                          ]
                           
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{                                           
                                           roles {{
                                                id
                                                name
                                            }}                                              
                                       }}
                                }}
                            }}"
            };



            try
            {
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));

                List<Models.Target.Role> roles = response.Data[$"findUserList"]["content"][0]["roles"].ToObject<List<Models.Target.Role>>();
                return roles;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<User> CreateRoleAsync(Person person, User user)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);



            //Get Contact Informaation
            var ContactInfo = await ContactByEmailAsync(person.Email);
            var roles = await RolesAsync(person);


            //FIND ROLES BY NAME 

            var csRole = _context.TargetRole.Where(w => w.Name.ToLower().Equals(person.RoleName.ToLower())).FirstOrDefault();
            if (csRole == null) csRole = new Models.Target.Role
            {
                Id = person.RoleId == 1 ? 2 : 11,
                Name = person.RoleId == 1 ? "Admin" : "CB User"
            };


            //array1.Any(l2 => array2.Contains(l2))
            try
            {
                if (roles.Any() && csRole.Id == 2)
                    return user; //the user already has the admin role

                GraphQL.GraphQLRequest request = null;
                if (roles.Count == 0 || (roles.Any() && !roles.Exists(r => r.Id == csRole.Id)))
                {
                    //add relationshipt
                    request = new GraphQL.GraphQLRequest
                    {
                        Query = @"
                           mutation CREATE_USER_ROLES($userRole: UserRoleInput!) {
                           createUserRole(userRole: $userRole) {
                             role {
                                id
                            }

                         }
                   }",
                        OperationName = "CREATE_USER_ROLES",
                        Variables = new
                        {
                            userRole =
                                           new
                                           {
                                               role = new
                                               {
                                                   id = csRole.Id,
                                                   name = csRole.Name
                                               },
                                               user = new
                                               {
                                                   id = user.Id,
                                                   userName = user.UserName,
                                                   tenantIds = new int[] { 1 },
                                                   active = person.IsActive,
                                                   contactId = ContactInfo.Id
                                               }
                                           }
                        }
                    };

                    var response = await csClient.SendMutationAsync<dynamic>(request);
                    if (response.Errors?.Length > 0)
                        throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));
                }

                //remove CB User role because the User now it is Admin
                if (roles.Any() && csRole.Id == 2 && roles.Exists(r => r.Id == csRole.Id))
                {
                    request = new GraphQL.GraphQLRequest
                    {
                        Query = @"
                           mutation DELETE_USER_ROLES($userRole: UserRoleInput!) {
                                                deleteUserRole(userRole: $userRole)
                            }",
                        OperationName = "DELETE_USER_ROLES",
                        Variables = new
                        {
                            userRole =
                                          new
                                          {
                                              role = new
                                              {
                                                  id = csRole.Id,
                                                  name = csRole.Name
                                              },
                                              user = new
                                              {
                                                  id = user.Id,
                                                  userName = user.UserName,
                                                  tenantIds = new int[] { 1 },
                                                  active = person.IsActive,
                                                  contactId = ContactInfo.Id
                                              }
                                          }
                        }
                    };

                    var response = await csClient.SendMutationAsync<dynamic>(request);
                    if (response.Errors?.Length > 0)
                        throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));
                }

                return user;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<User>> GetListAsync()
        {
            var result = _context.TargetUser.ToList();
            return await Task.FromResult(result);
        }
        public async Task<User> UpdateNonActiveAsync(User user)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            int contactId = 0;
            var ContactInfo = await ContactByEmailAsync(user.UserName);
            contactId = ContactInfo.Id;

            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation  MODIFY_USER($user: UserInput!) {
                       modifyUser(user: $user) {
                         id
                                             
                     }
               }",
                OperationName = "MODIFY_USER",
                Variables = new { user = new { id = user.Id, userName = user.UserName, externalId = DBNull.Value, tenantIds = new int[] { 1 }, active = user.IsActive, contactId = contactId } }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                User userUpdate = response.Data["modifyUser"].ToObject<User>();
                _context.TargetUser.Update(userUpdate);
                _context.SaveChanges();
                return user;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<User> UpdateAsync(Person person, User user)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;
           
            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            int contactId = 0;

            //Get Contact Informaation

            var ContactInfo = await ContactByEmailAsync(person.Email);
            contactId = ContactInfo.Id;




            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation  MODIFY_USER($user: UserInput!) {
                       modifyUser(user: $user) {
                         id
                                             
                     }
               }",
                OperationName = "MODIFY_USER",
                Variables = new { user = new { id = user.Id, userName = user.UserName, externalId = person.Id, tenantIds = new int[] { 1 }, active = person.IsActive, contactId = contactId } }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                User userUpdate = response.Data["modifyUser"].ToObject<User>();
                _context.TargetUser.Update(userUpdate);
                _context.SaveChanges();
                return user;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
