﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ITargetRole
    {
        Task<IEnumerable<Models.Target.Role>> GetListAsync();
    }
}
