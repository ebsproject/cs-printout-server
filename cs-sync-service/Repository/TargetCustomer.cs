﻿using cs_sync_service.Helper;
using cs_sync_service.Models;
using cs_sync_service.Models.Target;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class TargetCustomer : ITargetCustomer
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly IAuthentication _authentication;
        internal string _token;
        public TargetCustomer(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;

            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findCustomerList(page: {{
                                        number: 1
                                        size: 300
                                    }}){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                              id
                                              name
                                              phone
                                              officialEmail
                                              alternateEmail
                                              jobTitle
                                              languagePreference
                                              phoneExtension
                                              owningOrganizationId     
                                              active
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                List<Models.Target.Customer> customers = response.Data[$"findCustomerList"]["content"].ToObject<List<Models.Target.Customer>>();
                _context.TargetCustomer.RemoveRange(_context.TargetCustomer.ToList());
                _context.AddRange(customers);
                _context.SaveChanges();
                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Contact from Core System, review the endpoint: {ERR}", ex.Message);
            }
        }
        public async Task<Customer> CreateAsync(Models.Source.Organization organization)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation CREATE_CUSTOMER($customer: CustomerInput!) {
                       createCustomer(customer: $customer) {
                       id
                       name
                      
                     }
               }",
                OperationName = "CREATE_CUSTOMER",
                Variables = new { customer = new { id = 0, name = organization.OrganizationCode, owningOrganizationId=2, phone="0000", officialEmail="info@ebsproject.org",active=true } }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                Customer customer = response.Data["createCustomer"].ToObject<Customer>();
                _context.TargetCustomer.Add(customer);
                _context.SaveChanges();
                return customer;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<Customer>> GetListAsync()
        {
            var result = _context.TargetCustomer.ToList();
            return await Task.FromResult(result);
        }
    }
}
