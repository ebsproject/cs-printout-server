﻿using cs_sync_service.Helper;
using cs_sync_service.Models;
using cs_sync_service.Models.Target;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class TargetTenant : ITargetTenant
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        readonly IAuthentication _authentication;
        private ITargetContact _targetContact;


        internal string _token;
        public TargetTenant(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _context = context;
            _client = client;
            _logger = logger;
            _token = token;


            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    //fixed the tenant
                    Query = @$"{{findTenant(id:1){{                              
                               id
                                name
                                sync
                                instances{{crop{{
                                    id
                                    name
                                  }}
                                }}
                                customer{{id
                                  name
                                    }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                dynamic CoreTenant = response.Data[$"findTenant"].ToObject<dynamic>();

                Models.Target.Tenant tenant = new Tenant
                {
                    Id = CoreTenant.id,
                    Name = CoreTenant.name,
                    Sync = CoreTenant.sync,
                    CropId = CoreTenant.instances[0].crop.id,
                    CropName = CoreTenant.instances[0].crop.name,
                    CustomerId = CoreTenant.customer.id,
                    CustomerName = CoreTenant.customer.name
                };

                //Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<Tenant>
                _context.TargetTenant.RemoveRange(_context.TargetTenant.ToList());
                _context.AddRange(tenant);
                _context.SaveChanges();
                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Tenant from Core System, review the endpoint: {ERR}", ex.Message);
            }
        }


        public async Task<List<Models.Target.Hierarchy>> HierarchyLevelsAsync()
        {

            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findHierarchyDesignList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                           filters: [
                                      {{col: ""hierarchy.id""
                                        mod: EQ
                                        val: ""1""

                                      }}
                                    ]
                                sort:
                                     {{
                                        col: ""level""
                                        mod: DES
                                }}
                                )
                        {{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                         id
                                          level
                                          name                                              
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));
                List<Models.Target.Hierarchy> hierarchies = response.Data[$"findHierarchyDesignList"]["content"].ToObject<List<Models.Target.Hierarchy>>();

                return hierarchies;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Programs from Core System, review the endpoint: {ERR}", ex.Message);
                throw new Exception(String.Join(",", ex.Message));
            }

        }
        public async Task<List<Models.Target.HierarchyTree>> HierarchyTreeDataAsync(string colName, object value)
        {

            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findHierarchyTreeList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                           filters: [
                                      {{
                                        col: ""{colName}""
                                        mod: EQ
                                        val: ""{value}""

                                      }}
                                    ]
                              
                                )
                        {{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                          id
                                          value      
                                          recordId
                                          parent{{
                                            id
                                            value
                                            recordId        
                                          }}
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));
                List<Models.Target.HierarchyTree> data = response.Data[$"findHierarchyTreeList"]["content"].ToObject<List<Models.Target.HierarchyTree>>();
                return data;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Programs from Core System, review the endpoint: {ERR}", ex.Message);
                throw new Exception(String.Join(",", ex.Message));
            }

        }


        public async Task CleanHierarchAsync()
        {
            try
            {
                var levels = await HierarchyLevelsAsync();

                foreach (var item in levels.Where(w => w.Level != 0))
                {
                    //get ids of the level
                    var values = await HierarchyTreeDataAsync(colName: "hierarchyDesign.id", value: item.Id);

                    int[] dataItems = values.Select(x => x.Id).ToArray();
                    await DeleteTreeNodeDataAsync(dataItems);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Programs from Core System, review the endpoint: {ERR}", ex.Message);
            }
        }
        public async Task DeleteTreeNodeDataAsync(int[] ids)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            //deleteAllHierarchyTree(hierarchyTreeIds: [Int!]!): String
            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                      mutation DELETE_TREEVALUES($hierarchyTreeIds: [Int!]!) {
                                                deleteAllHierarchyTree(hierarchyTreeIds: $hierarchyTreeIds)
                            }",
                OperationName = "DELETE_TREEVALUES",
                Variables = new
                {
                    hierarchyTreeIds = ids
                }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<Tenant>> GetListAsync()
        {
            var result = _context.TargetTenant.ToList();
            return await Task.FromResult(result);
        }

        public async Task<Models.Source.Crop> CoreBreedingTenant(string cropName)
        {

            try
            {
                //implementation               


                using var graphQLClient = new GraphQLHttpClient(new Uri(new Uri(Environment.GetEnvironmentVariable("ENDPOINT_CB_GRAPHQL")), relativeUri: "graphql"), new NewtonsoftJsonSerializer());

                graphQLClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{
                                findCropList(
                                            page: {{number: 1, size: 300}}
                                            filters: [
                                                        {{col: ""cropName"", mod: EQ, val: ""{cropName}""}}
                                                     ]
                                ) {{                     
                                    totalElements
                                    totalPages
                                    size
                                    numberOfElements
                                    number
                                        content {{
                                            id
                                            cropName
                                            cropCode
                                            cropPrograms {{
                                                id
                                                cropProgramCode
                                                cropProgramName
                                                organization {{
                                                    id
                                                    organizationCode
                                                    organizationName
                                                }}
                                                programs {{
                                                    id
                                                    programCode
                                                    programName
                                                    programTeams {{
                                                        id
                                                        team {{
                                                            id
                                                            teamName
                                                            teamCode
                                                            teamMembers {{
                                                                person {{
                                                                    id
                                                                    email
                                                                    personType
                                                                      role {{ id
                                                                       personRoleCode
                                                                        personRoleName
                                                                      }}    
                                                                }}
                                                            }}
                                                        }}
                                                    }}
                                                }}
                                            }}
                                        }}
                                    }}
                                }}"
                };
                var response = graphQLClient.SendQueryAsync<dynamic>(request).Result;
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));
                Models.Source.Crop tenant = response.Data[$"findCropList"]["content"][0].ToObject<Models.Source.Crop>();

                return tenant;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Programs from Core System, review the endpoint: {ERR}", ex.Message);
                throw new Exception(String.Join(",", ex.Message));
            }

        }


        public async Task SyncHierarchyTree(Models.Target.Tenant tenant)
        {
            var cbTenant = await CoreBreedingTenant(tenant.CropName);

            foreach (var t in cbTenant.CropPrograms)
            {
                await SyncGlobalProgramLevel(t);
                await SyncOrganization(t.Organization, t);
                foreach (var program in t.Programs)
                {
                    await SyncPrograms(program, t.Organization);
                    foreach (var teams in program.ProgramTeams)
                    {
                        await SyncTeams(teams.Team, program);
                        foreach (var member in teams.Team.TeamMembers)
                        {
                            await SyncMembers(member.Person, teams.Team);
                        }
                    }
                }
            }


        }

        public async Task SyncGlobalProgramLevel(Models.Source.CropProgram programLevel)
        {
            var treeParents = await HierarchyTreeDataAsync("hierarchyDesign.id", 0);
            var treeParent = treeParents.FirstOrDefault();
            var treeTenantLevel = await HierarchyTreeDataAsync("hierarchyDesign.id", 1);
            var treeData = treeTenantLevel.Where(w => w.RecordId == programLevel.Id && w.Parent.Id == treeParent.Id);

            //add global program
            if (!treeData.Any())
            {

                dynamic payload = new
                {
                    value = programLevel.CropProgramCode,
                    recordId = programLevel.Id,
                    hierarchyDesignId = 1,
                    parentId = treeParent.Id,
                    externalRecordId = programLevel.Id
                };

                await CreateTreeNode(payload);

            }
            else //update
            {
                var treeRecord = treeData.FirstOrDefault();
                dynamic payload = new
                {
                    id = treeRecord.Id,
                    value = treeRecord.Value,
                    recordId = treeRecord.RecordId,
                    hierarchyDesignId = 1,
                    parentId = treeRecord.Parent.Id,
                    externalRecordId = programLevel.Id
                };

                await ModifyTreeNode(payload);
            }
        }

        public async Task SyncOrganization(Models.Source.Organization organizationLevel, Models.Source.CropProgram parent)
        {


            //find parent
            var treeParents = await HierarchyTreeDataAsync("hierarchyDesign.id", 1);
            var treeParent = treeParents.Where(w => w.Value == parent.CropProgramCode).FirstOrDefault();


            if (treeParent != null)
            {



                _targetContact = new TargetContact(_context, _client, _logger, _token);
                var contacts = await _targetContact.GetListAsync();

                var findInstitution = contacts.Where(w => w.Institution.CommonName.Trim().ToLower().Equals(organizationLevel.OrganizationCode.Trim().ToLower())).FirstOrDefault();
                if (findInstitution == null)
                {
                    _logger.LogWarning("The proccess can not register organization to hierarchy, cannot find the code in the contact {org}", organizationLevel.OrganizationName);
                    return;
                }


                var treeInstitutionLevel = await HierarchyTreeDataAsync("hierarchyDesign.id", 2);
                var treeData = treeInstitutionLevel.Where(w => w.RecordId == findInstitution.Id && w.Parent.Id == treeParent.Id);

                //add global program
                if (!treeData.Any())
                {

                    dynamic payload = new
                    {
                        value = findInstitution.Institution.CommonName,
                        recordId = findInstitution.Id,
                        hierarchyDesignId = 2,
                        parentId = treeParent.Id,
                        externalRecordId = organizationLevel.Id
                    };

                    await CreateTreeNode(payload);

                }
                else //update
                {
                    var treeRecord = treeData.FirstOrDefault();
                    dynamic payload = new
                    {
                        id = treeRecord.Id,
                        value = treeRecord.Value,
                        recordId = treeRecord.RecordId,
                        hierarchyDesignId = 2,
                        parentId = treeRecord.Parent.Id,
                        externalRecordId = organizationLevel.Id
                    };

                    await ModifyTreeNode(payload);
                }
            }
        }
        public async Task SyncPrograms(Models.Source.Program programLevel, Models.Source.Organization parent)
        {

            var findProgram = _context.TargetProgram.Where(w => w.ExternalId == programLevel.Id).FirstOrDefault();
            if (findProgram == null)
            {
                _logger.LogWarning("The proccess can not register program to hierarchy, cannot find the code in the contact {org}", programLevel.ProgramName);
                return;
            }

            //find parent
            var treeParents = await HierarchyTreeDataAsync("hierarchyDesign.id", 2);
            var treeParent = treeParents.Where(w => w.Value == parent.OrganizationCode).FirstOrDefault();
            if (treeParent != null)
            {



                var treeProgramLevel = await HierarchyTreeDataAsync("hierarchyDesign.id", 3);
                var treeData = treeProgramLevel.Where(w => w.RecordId == findProgram.Id && w.Parent.Id == treeParent.Id);

                //add global program
                if (!treeData.Any())
                {

                    dynamic payload = new
                    {
                        value = findProgram.Code,
                        recordId = findProgram.Id,
                        hierarchyDesignId = 3,
                        parentId = treeParent == null ? -1 : treeParent.Id,
                        externalRecordId = programLevel.Id
                    };
                    await CreateTreeNode(payload);
                }
                else //update
                {
                    var treeRecord = treeData.FirstOrDefault();
                    dynamic payload = new
                    {
                        id = treeRecord.Id,
                        value = treeRecord.Value,
                        recordId = treeRecord.RecordId,
                        hierarchyDesignId = 3,
                        parentId = treeRecord.Parent.Id,
                        externalRecordId = programLevel.Id
                    };

                    await ModifyTreeNode(payload);
                }
            }
        }

        public async Task SyncTeams(Models.Source.Team teamLevel, Models.Source.Program parent)
        {

            //find parent
            var treeParents = await HierarchyTreeDataAsync("hierarchyDesign.id", 3);
            var treeParent = treeParents.Where(w => w.Value == parent.ProgramCode).FirstOrDefault();

            if (treeParent != null)
            {


                //verify if exist the team by value
                var treeTeamLevel = await HierarchyTreeDataAsync("hierarchyDesign.id", 5);
                var treeTeamData = treeTeamLevel.Where(w => (w.RecordId == teamLevel.Id ) && w.Parent.Id == treeParent.Id);
                //add global program
                if (!treeTeamData.Any())
                {

                    var duplicatedRecords = await HierarchyTreeDataAsync("value", teamLevel.TeamCode);

                    string teamValue = teamLevel.TeamCode;
                    string prefix = "TEAM";
                    if (duplicatedRecords.Any())
                    {
                        teamValue = $"{prefix}-{teamValue}";
                        if (teamValue.Contains(prefix))
                            teamValue = $"{teamValue}-{teamLevel.Id.ToString()}";
                    }
                                    


           
                        dynamic payload = new
                        {
                            value = teamValue,
                            recordId = teamLevel.Id,
                            hierarchyDesignId = 5,
                            parentId = treeParent == null ? -1 : treeParent.Id,
                            externalRecordId = teamLevel.Id
                        };

                        await CreateTreeNode(payload);
                }
                else //update
                {
                    var treeRecord = treeTeamData.FirstOrDefault();
                    dynamic payload = new
                    {
                        id = treeRecord.Id,
                        value = treeRecord.Value,
                        recordId = treeRecord.RecordId,
                        hierarchyDesignId = 5,
                        parentId = treeRecord.Parent.Id,
                        externalRecordId = teamLevel.Id
                    };

                    await ModifyTreeNode(payload);
                }


            }

        }
        public async Task SyncMembers(Models.Source.Person memberLevel, Models.Source.Team parent)
        {


            //FIND ROLES BY NAME 

            var csRole = _context.TargetRole.Where(w => w.Name.ToLower().Equals(memberLevel.Role.PersonRoleName)).FirstOrDefault();
            if (csRole == null) csRole = new Models.Target.Role
            {
                Id = memberLevel.Role.Id== 1 ? 2 : 11,
                Name = memberLevel.Role.Id == 1 ? "Admin" : "CB User"
            };



            //insert role level in the team
            //string csRoleDesc = memberLevel.PersonType.Equals("admin") ? "Admin" : "CB User";
            //int csRoleId = memberLevel.PersonType.Equals("admin") ? 2 : 11;
            
            var treeParents = await HierarchyTreeDataAsync("hierarchyDesign.id", 5);


            //var duplicatedRecords = await HierarchyTreeDataAsync("value", parent.TeamCode);
            ////improve it
            //string teamValue = parent.TeamCode;
            //string prefix = "TEAM";
            //if (duplicatedRecords.Any())
            //{
            //    teamValue = $"{prefix}-{teamValue}";
            //    if (teamValue.Contains(prefix))
            //        teamValue = $"{teamValue}-{parent.Id.ToString()}";
            //}

            var treeParent = treeParents.Where(w => w.RecordId == parent.Id).FirstOrDefault();

            /////

            if (treeParent != null)
            {


                var treeRoleLevel = await HierarchyTreeDataAsync("hierarchyDesign.id", 6);
                var treeRole = treeRoleLevel.Where(w => w.RecordId == csRole.Id && w.Parent.Id == treeParent.Id);


                //add global program
                if (!treeRole.Any())
                {


                    dynamic payload = new
                    {
                        value = csRole.Name,
                        recordId = csRole.Id,
                        hierarchyDesignId = 6,
                        parentId = treeParent == null ? -1 : treeParent.Id,
                        externalRecordId = memberLevel.Role.Id
                    };

                    await CreateTreeNode(payload);

                }
                else //update
                {
                    var treeRecord = treeRole.FirstOrDefault();
                    dynamic payload = new
                    {
                        id = treeRecord.Id,
                        value = treeRecord.Value,
                        recordId = treeRecord.RecordId,
                        hierarchyDesignId = 6,
                        parentId = treeRecord.Parent.Id,
                        externalRecordId = memberLevel.Role.Id
                    };

                    await ModifyTreeNode(payload);
                }

                await AddMember(memberLevel, treeParent, csRole.Id);
            }

        }

        public async Task AddMember(Models.Source.Person memberLevel, HierarchyTree teamParent, int RoleId)
        {

            //insert user
            var findUser = _context.TargetUser.Where(w => w.UserName.Trim().ToLower().Equals(memberLevel.Email.Trim().ToLower())).FirstOrDefault();
            if (findUser == null)
            {
                _logger.LogWarning("The proccess can not register user to hierarchy, cannot find the code in the contact {user}", memberLevel.PersonName);
                return;
            }

            var treeParents = await HierarchyTreeDataAsync("hierarchyDesign.id", 6);
            var treeParent = treeParents.Where(w => w.RecordId == RoleId && w.Parent.Id == teamParent.Id).FirstOrDefault();


            if (treeParent != null)
            {


                var treeMemberLevel = await HierarchyTreeDataAsync("hierarchyDesign.id", 7);
                var treeMember = treeMemberLevel.Where(w => w.RecordId == findUser.Id && w.Parent.Id == treeParent.Id);


                //add global program
                if (!treeMember.Any())
                {

                    string strVal = ($"U:{memberLevel.Email}");
                    dynamic payload = new
                    {
                        value = strVal,
                        recordId = findUser.Id,
                        hierarchyDesignId = 7,
                        parentId = treeParent == null ? -1 : treeParent.Id,
                        externalRecordId = memberLevel.Id
                    };

                    await CreateTreeNode(payload);

                }
                else //update
                {
                    var treeRecord = treeMember.FirstOrDefault();
                    dynamic payload = new
                    {
                        id = treeRecord.Id,
                        value = treeRecord.Value,
                        recordId = treeRecord.RecordId,
                        hierarchyDesignId = 7,
                        parentId = treeRecord.Parent.Id,
                        externalRecordId = memberLevel.Id
                    };

                    await ModifyTreeNode(payload);
                }
            }

        }
      
        /// <summary>
        /// Create Node in Hierarchy Tree
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task CreateTreeNode(dynamic payload)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation  CREATE_NODE($hierarchyTree: HierarchyTreeInput!) {
                       createHierarchyTree(hierarchyTree: $hierarchyTree) {
                         id
                                             
                     }
               }",
                OperationName = "CREATE_NODE",
                Variables = new { hierarchyTree = new { id = 0, value = payload.value, recordId = payload.recordId, hierarchyDesignId = payload.hierarchyDesignId, parentId = payload.parentId, externalRecordId= payload.externalRecordId } }
            };
            var response = csClient.SendMutationAsync<dynamic>(request).Result;
            if (response.Errors?.Length > 0)
                throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));

            //try
            //{
               

            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
        }

        /// <summary>
        /// Modify the externals ids
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task ModifyTreeNode(dynamic payload)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation  MODIFY_NODE($hierarchyTree: HierarchyTreeInput!) {
                       modifyHierarchyTree(hierarchyTree: $hierarchyTree) {
                         id                                             
                     }
               }",
                OperationName = "MODIFY_NODE",
                Variables = new { hierarchyTree = new { id = payload.id, value = payload.value, recordId = payload.recordId, hierarchyDesignId = payload.hierarchyDesignId, parentId = payload.parentId, externalRecordId = payload.externalRecordId } }
            };


            try
            {
                var response = csClient.SendMutationAsync<dynamic>(request).Result;
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
