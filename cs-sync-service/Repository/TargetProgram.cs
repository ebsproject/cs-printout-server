﻿using cs_sync_service.Models;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public class TargetProgram : ITargetProgram
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        
        internal string _token;
        public TargetProgram(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _client = client;
            _logger = logger;
            _token = token;
            _context = context;
            try
            {
                //implementation               


                GraphQLHttpClient csClient = _client as GraphQLHttpClient;

                csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                var request = new GraphQL.GraphQLRequest
                {
                    Query = @$"{{findProgramList(
                                    page: {{
                                        number: 1
                                        size: 300
                                    }}
                           
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                          id
                                          name
                                          code
                                          externalId
                                              
                                       }}
                                }}
                            }}"
                };
                var response = csClient.SendQueryAsync<dynamic>(request).Result;
                List<Models.Target.Program> programs = response.Data[$"findProgramList"]["content"].ToObject<List<Models.Target.Program>>();

                _context.TargetProgram.RemoveRange(_context.TargetProgram.ToList());
              

                _context.AddRange(programs);
                _context.SaveChanges();

                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Programs from Core System, review the endpoint: {ERR}", ex.Message);
            }
        }
        public async Task<Models.Target.Program> CreateAsync(Models.Source.Program program)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation CREATE_PROGRAM($program: ProgramInput!) {
                       createProgram(program: $program) {
                                   id
                                   code
                                   name
                     }
               }",
                OperationName = "CREATE_PROGRAM",
                Variables = new { program = new { id=0,code=program.ProgramCode.Trim(),name= program.ProgramName.Trim(),
                                                   type="breeding", status="active", description= program.ProgramName.Trim(),
                                                    notes=String.Empty, externalId= program.Id,cropProgramId=0, tenantId=1  } }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                Models.Target.Program objProgram = response.Data["createProgram"].ToObject<Models.Target.Program>();
                _context.TargetProgram.Add(objProgram);
                _context.SaveChanges();
                return objProgram;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<Models.Target.Program>> GetListAsync()
        {
            var result = _context.TargetProgram.ToList();
            return await Task.FromResult(result);
        }

        public async Task<Models.Target.Program> UpdateAsync(Models.Target.Program target,Models.Source.Program source)
        {
            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);


            var request = new GraphQL.GraphQLRequest
            {
                Query = @"
                       mutation MODIFY_PROGRAM($program: ProgramInput!) {
                       modifyProgram(program: $program) {
                                   id
                                   code
                                   name
                     }
               }",
                OperationName = "MODIFY_PROGRAM",
                Variables = new
                {
                    program = new
                    {
                        id = target.Id,                       
                        externalId = source.Id,
                        cropProgramId = 0,
                        tenantId = 1
                    }
                }
            };


            try
            {
                var response = await csClient.SendMutationAsync<dynamic>(request);
                if (response.Errors?.Length > 0)
                    throw new Exception(String.Join(",", response.Errors.Select(s => s.Message)));


                Models.Target.Program objProgram = response.Data["modifyProgram"].ToObject<Models.Target.Program>();
                _context.TargetProgram.Update(objProgram);
                _context.SaveChanges();
                return objProgram;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
