﻿using cs_sync_service.Models;
using cs_sync_service.Models.Target;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    internal class TargetRole : ITargetRole
    {
        readonly EBSContext _context;
        readonly IGraphQLClient _client;
        readonly ILogger<Worker> _logger;
        internal string _token;

        public TargetRole(EBSContext context, IGraphQLClient client, ILogger<Worker> logger, string token)
        {
            _client = client;
            _logger = logger;
            _token = token;
            _context = context;

            try
            {
                //implementation               
                dynamic response = GetDataPerPage(1).Result;

                int currentPage = (int)response.number;
                int totalCount = (int)response.totalElements;
                int totalPages = (int)response.totalPages;
                int pageSize = (int)response.size;

                List<Role> roles = response.content.ToObject<List<Role>>();

                if (totalPages > 1)
                {
                    for (int i = 2; i <= totalPages; i++)
                    {
                        response = GetDataPerPage(i).Result;
                        roles.AddRange(response.content.ToObject<List<Role>>());
                    }
                }

                //var query = roles.GroupBy(x => x).Where(g => g.Count() > 1).Select(y => y.Key).ToList();


                _context.TargetRole.RemoveRange(_context.TargetRole.ToList());
                _context.SaveChanges();
     

                foreach (var role in roles)
                {
                    try
                    {
                        _context.Add(role);
                        _context.SaveChanges();
                    }
                    catch { }
                }


                foreach (var entity in _context.ChangeTracker.Entries())
                    entity.State = EntityState.Detached;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error populating Users from Core System, review the endpoint: {ERR}", ex.Message);
            }

        }

        private async Task<dynamic> GetDataPerPage(int page)
        {


            GraphQLHttpClient csClient = _client as GraphQLHttpClient;

            csClient.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

            var request = new GraphQL.GraphQLRequest
            {
                Query = @$"{{findRoleList(
                                    page: {{
                                        number: {page}
                                        size: 300
                                    }}
                           
                                ){{
                                totalElements
                                totalPages
                                size
                                numberOfElements
                                number
                                content {{
                                           id
                                          name
                                          description
                                       }}
                                }}
                            }}"
            };
            var response = csClient.SendQueryAsync<dynamic>(request).Result;
            var content = response.Data[$"findRoleList"].ToObject<dynamic>();
            return content;
        }
        public async Task<IEnumerable<Role>> GetListAsync()
        {


            var result = _context.TargetRole.ToList();
            return await Task.FromResult(result);
        }
    }
}
