﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs_sync_service.Models.Source;

namespace cs_sync_service.Repository
{
    public interface ISourceOrganization
    {
        Task<IEnumerable<Models.Source.Organization>> GetListAsync();
 
    }
}
