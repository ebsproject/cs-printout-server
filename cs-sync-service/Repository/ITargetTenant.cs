﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
    public interface ITargetTenant
    {
        Task<IEnumerable<Models.Target.Tenant>> GetListAsync();
        Task CleanHierarchAsync();
        Task SyncHierarchyTree(Models.Target.Tenant tenant);
    }
}
