﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service.Repository
{
     public interface ITargetContact
    {
        Task<IEnumerable<Models.Target.Contact>> GetListAsync();
        Task<Models.Target.Contact> CreateAsync(Models.Source.Organization organization);
        Task RefreshAsync();
    }
}
