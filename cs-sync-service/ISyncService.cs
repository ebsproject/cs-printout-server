﻿using cs_sync_service.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_sync_service
{
    interface ISyncService
    {
        Task ExecuteTenantDataflowAsync(CancellationToken stoppingToken,                                      
                                        ITargetCustomer targetCustomer,
                                        ITargetContact targetContact,
                                        ITargetProgram targetProgram,
                                        ITargetUser targetUser,
                                        ITargetTenant targetTenant);
    }
}
